/**************************************************
 * this is for weighted graph in ChemNetwork      *
 * Tiecheng Zhou                                  *
 * A. Clark Research Lab, Department of Chemistry *
 * Washington State University, Pullman, WA 99164 *
 **************************************************/

#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "chemnetworks_orig.h"

using namespace CN_NS;

ChemNetworkOrig::ChemNetwork_Weighted_Graph::
    ChemNetwork_Weighted_Graph() { // constructor for weighted graph
  index_weighted_graph = 0;        // setting the default values here
  index_wg_st1_cluster = index_wg_st2_cluster = 0;
  wg_solute_type = 0;
  wg_solvent_type = 0;
  num_wg_select_type_st = num_wg_select_type_sv = 0;

  for (wgi = 0; wgi < NUM_INTER; wgi++) {
    wg_select_type_sv[wgi] = wg_select_type_sv[wgi] = 0;

    cluster_st1_sv_type[wgi] = cluster_st1_atom[wgi] =
        cluster_st1_sv_atom[wgi] = 0;
    cluster_st1_sv_dist_min[wgi] = 0.0;
    cluster_st1_sv_dist_max[wgi] = 1000.0;
    atom1_wg_st1_sv1[wgi] = atom2_wg_st1_sv1[wgi] = atom1_wg_st1_sv2[wgi] =
        atom2_wg_st1_sv2[wgi] = 0;

    atom1_wg_sv1_sv1[wgi] = atom2_wg_sv1_sv1[wgi] = atom1_wg_sv2_sv2[wgi] =
        atom2_wg_sv2_sv2[wgi] = atom1_wg_sv1_sv2[wgi] = atom2_wg_sv1_sv2[wgi] =
            0;

    cluster_st2_sv_type[wgi] = cluster_st2_atom[wgi] =
        cluster_st2_sv_atom[wgi] = 0;
    cluster_st2_sv_dist_min[wgi] = 0.0;
    cluster_st2_sv_dist_max[wgi] = 1000.0;
    atom1_wg_st2_sv1[wgi] = atom2_wg_st2_sv1[wgi] = atom1_wg_st2_sv2[wgi] =
        atom2_wg_st2_sv2[wgi] = 0;

    atom1_wg_st1_st2[wgi] = atom2_wg_st1_st2[wgi] = 0;
  }

  index_wg_st1_sv1 = index_wg_st1_sv2 = 0;
  num_wg_st1_sv1_dist = num_wg_st1_sv2_dist = 0;
  funct_type_wg_st1_sv1 = funct_type_wg_st1_sv2 = 0;
  funct_par1_wg_st1_sv1 = funct_par2_wg_st1_sv1 = funct_par1_wg_st1_sv2 =
      funct_par2_wg_st1_sv2 = 0.0; // parameters in funct, currently 1.0 / (1.0
                                   // + exp(n0 * (r - r0)/r0))

  index_wg_st2_sv1 = index_wg_st2_sv2 = 0;
  num_wg_st2_sv1_dist = num_wg_st2_sv2_dist = 0;
  funct_type_wg_st2_sv1 = funct_type_wg_st2_sv2 = 0;
  funct_par1_wg_st1_sv2 = funct_par2_wg_st2_sv1 = funct_par1_wg_st2_sv2 =
      funct_par2_wg_st2_sv2 = 0.0;

  index_wg_sv1_sv1 = index_wg_sv2_sv2 = index_wg_sv1_sv2 = 0;
  num_wg_sv1_sv1_dist = num_wg_sv2_sv2_dist = num_wg_sv1_sv2_dist = 0;
  funct_type_wg_sv1_sv1 = funct_type_wg_sv2_sv2 = funct_type_wg_sv1_sv2;
  funct_par1_wg_sv1_sv1 = funct_par2_wg_sv1_sv1 = funct_par1_wg_sv2_sv2 =
      funct_par2_wg_sv2_sv2 = funct_par1_wg_sv1_sv2 = funct_par2_wg_sv1_sv2 =
          0.0;

  index_wg_st1_st2 = 0;
  num_wg_st1_st2_dist = 0;
  funct_type_wg_st1_st2 = 0;
  funct_par1_wg_st1_st2 = funct_par2_wg_st1_st2 = 0.0;

  num_mol_cluster_st1 = num_mol_cluster_st2 = num_mol_cluster_sv1 =
      num_mol_cluster_sv2 = 0;
};

ChemNetworkOrig::ChemNetwork_Weighted_Graph::
    ~ChemNetwork_Weighted_Graph(){}; // destructor for weighted graph

/* this is a function to calculate the pagerank force  */
int ChemNetworkOrig::ChemNetwork_Weighted_Graph::calculate_PR_force_from_PR(
    FILE *output_PR_force_on_weighted_graph, double PR_damping_factor,
    double **PR_inverse_IdS, double **WG_Adj, double **PR_Smatrix,
    double ***dWij_dxk, double ***dWij_dyk, double ***dWij_dzk,
    int num_mol_cluster_st1, int nsolute1, int num_mol_cluster_st2,
    int nsolute2, int num_mol_cluster_sv1, int nsolvent1,
    int num_mol_cluster_sv2, int nsolvent2, double *PR_vector,
    struct Mol_identity *WG_Mol_id, int num_PR_wg_force_bias,
    int PR_wg_bias_target_id[NUM_INTER],
    double PR_wg_bias_target_value[NUM_INTER],
    double PR_wg_bias_constant[NUM_INTER]) {

  int num_node, num_atom;
  int ni, nj, nk, ak, site, ai;
  double *totalWj;
  double ***dSij_dxk, ***dSij_dyk, ***dSij_dzk;
  double **dPRi_dxk, **dPRi_dyk, **dPRi_dzk;
  double *Fx, *Fy, *Fz;

  num_node = num_mol_cluster_st1 + num_mol_cluster_st2 + num_mol_cluster_sv1 +
             num_mol_cluster_sv2;
  num_atom = num_mol_cluster_st1 * nsolute1 + num_mol_cluster_st2 * nsolute2 +
             num_mol_cluster_sv1 * nsolvent1 + num_mol_cluster_sv2 * nsolvent2;

  totalWj = (double *)calloc(num_node, sizeof(double));

  dSij_dxk = (double ***)malloc(num_node * sizeof(double **));
  dSij_dyk = (double ***)malloc(num_node * sizeof(double **));
  dSij_dzk = (double ***)malloc(num_node * sizeof(double **));
  for (ni = 0; ni < num_node; ni++) {
    dSij_dxk[ni] = (double **)malloc(num_node * sizeof(double *));
    dSij_dyk[ni] = (double **)malloc(num_node * sizeof(double *));
    dSij_dzk[ni] = (double **)malloc(num_node * sizeof(double *));

    for (nj = 0; nj < num_node; nj++) {
      dSij_dxk[ni][nj] = (double *)calloc(num_atom, sizeof(double));
      dSij_dyk[ni][nj] = (double *)calloc(num_atom, sizeof(double));
      dSij_dzk[ni][nj] = (double *)calloc(num_atom, sizeof(double));
    }
  }

  dPRi_dxk = (double **)malloc(num_node * sizeof(double *));
  dPRi_dyk = (double **)malloc(num_node * sizeof(double *));
  dPRi_dzk = (double **)malloc(num_node * sizeof(double *));
  for (ni = 0; ni < num_node; ni++) {
    dPRi_dxk[ni] = (double *)calloc(num_atom, sizeof(double));
    dPRi_dyk[ni] = (double *)calloc(num_atom, sizeof(double));
    dPRi_dzk[ni] = (double *)calloc(num_atom, sizeof(double));
  }

  Fx = (double *)calloc(num_atom, sizeof(double));
  Fy = (double *)calloc(num_atom, sizeof(double));
  Fz = (double *)calloc(num_atom, sizeof(double));

  for (nj = 0; nj < num_node; nj++) {
    for (ni = 0; ni < num_node; ni++)
      totalWj[nj] += WG_Adj[ni][nj];
  }

  /*
    for(ni=0; ni < num_node; ni++){
      for(nj=0; nj < num_node; nj++){
        printf("%d %d %e %e %e\n",ni,nj, WG_Adj[ni][nj], totalWj[nj],
    PR_Smatrix[ni][nj]);
      }
    } */

  /* calculate dSij_dxk, dSij_dyk, dSij_dzk */
  for (ak = 0; ak < num_atom; ak++) {
    for (ni = 0; ni < num_node; ni++) {
      for (nj = 0; nj < num_node; nj++) {
        if (totalWj[nj] == 0) {
          dSij_dxk[ni][nj][ak] =
              0.0; // normalization of Sij = 1.0/N when totalWj = 0.0, therefore
                   // its derivative is zero
          dSij_dyk[ni][nj][ak] = 0.0;
          dSij_dzk[ni][nj][ak] = 0.0;
        } else { // Sij = wij / totalWj, when totalWj != 0.0
          dSij_dxk[ni][nj][ak] = dWij_dxk[ni][nj][ak] / totalWj[nj];
          dSij_dyk[ni][nj][ak] = dWij_dyk[ni][nj][ak] / totalWj[nj];
          dSij_dzk[ni][nj][ak] = dWij_dzk[ni][nj][ak] / totalWj[nj];
          for (nk = 0; nk < num_node; nk++) {
            dSij_dxk[ni][nj][ak] += -1.0 * WG_Adj[ni][nj] / totalWj[nj] /
                                    totalWj[nj] * dWij_dxk[nk][nj][ak];
            dSij_dyk[ni][nj][ak] += -1.0 * WG_Adj[ni][nj] / totalWj[nj] /
                                    totalWj[nj] * dWij_dyk[nk][nj][ak];
            dSij_dzk[ni][nj][ak] += -1.0 * WG_Adj[ni][nj] / totalWj[nj] /
                                    totalWj[nj] * dWij_dzk[nk][nj][ak];
          }
        }
      }
    }
  }

  /* calculate dPRi_dxk .. */
  for (ni = 0; ni < num_node; ni++) {
    for (ak = 0; ak < num_atom; ak++) {
      dPRi_dxk[ni][ak] = 0.0;
      dPRi_dyk[ni][ak] = 0.0;
      dPRi_dzk[ni][ak] = 0.0;

      for (nk = 0; nk < num_node; nk++) {
        for (nj = 0; nj < num_node; nj++) {
          dPRi_dxk[ni][ak] += PR_damping_factor * PR_inverse_IdS[ni][nk] *
                              dSij_dxk[nk][nj][ak] * PR_vector[nj];
          dPRi_dyk[ni][ak] += PR_damping_factor * PR_inverse_IdS[ni][nk] *
                              dSij_dyk[nk][nj][ak] * PR_vector[nj];
          dPRi_dzk[ni][ak] += PR_damping_factor * PR_inverse_IdS[ni][nk] *
                              dSij_dzk[nk][nj][ak] * PR_vector[nj];
        }
      }
    }
  }

  /* calculate the force on each individual atom k, only the atoms involved in
   * the edge-weight have corresponding forces */
  for (site = 0; site < num_PR_wg_force_bias; site++) {
    ni = PR_wg_bias_target_id[site] -
         1; // the target id is read from ChemNetwork Input-file that is labeled
            // starting from 1 .., while in the Array, Matrix, the molecule id
            // starts from 0

    for (ak = 0; ak < num_atom;
         ak++) { // here I assume the bias is U = U_bias_constant * (PR_i -
                 // PR_i_target_value)^2
      Fx[ak] += -2.0 * PR_wg_bias_constant[site] *
                (PR_vector[ni] - PR_wg_bias_target_value[site]) *
                dPRi_dxk[ni][ak];
      Fy[ak] += -2.0 * PR_wg_bias_constant[site] *
                (PR_vector[ni] - PR_wg_bias_target_value[site]) *
                dPRi_dyk[ni][ak];
      Fz[ak] += -2.0 * PR_wg_bias_constant[site] *
                (PR_vector[ni] - PR_wg_bias_target_value[site]) *
                dPRi_dzk[ni][ak];
    }
  }

  /* print the force information to a file */
  for (ni = 0; ni < num_node; ni++) {
    if (WG_Mol_id[ni].solute_type != 0 &&
        WG_Mol_id[ni].solvent_type == 0) { // this is a solute
      nj = WG_Mol_id[ni].id;

      if (WG_Mol_id[ni].solute_type == 1) { // this is solute1
        for (ai = 1; ai <= nsolute1; ai++) {
          ak = ni * nsolute1 + ai - 1;
          fprintf(output_PR_force_on_weighted_graph,
                  "%d ( st1 %d atom %d ) force: %e %e %e \n", ni + 1, nj, ai,
                  Fx[ak], Fy[ak], Fz[ak]);
        }
      } else if (WG_Mol_id[ni].solute_type == 2) { // this is solute2
        for (ai = 1; ai <= nsolute2; ai++) {
          ak = num_mol_cluster_st1 * nsolute1 +
               (ni - num_mol_cluster_st1) * nsolute2 + ai - 1;
          fprintf(output_PR_force_on_weighted_graph,
                  "%d ( st2 %d atom %d ) force: %e %e %e \n", ni + 1, nj, ai,
                  Fx[ak], Fy[ak], Fz[ak]);
        }
      }
    } else { // this is a solvent
      if (WG_Mol_id[ni].solvent_type == 1) {
        nj = WG_Mol_id[ni].id;

        for (ai = 1; ai <= nsolvent1; ai++) {
          ak = num_mol_cluster_st1 * nsolute1 + num_mol_cluster_st2 * nsolute2 +
               (ni - num_mol_cluster_st1 - num_mol_cluster_st2) * nsolvent1 +
               ai - 1;
          fprintf(output_PR_force_on_weighted_graph,
                  "%d ( sv1 %d atom %d ) force: %e %e %e \n", ni + 1, nj, ai,
                  Fx[ak], Fy[ak], Fz[ak]);
        }
      } else if (WG_Mol_id[ni].solvent_type == 2) {
        nj = WG_Mol_id[ni].id;

        for (ai = 1; ai <= nsolvent2; ai++) {
          ak = num_mol_cluster_st1 * nsolute1 + num_mol_cluster_st2 * nsolute2 +
               num_mol_cluster_sv1 * nsolvent1 +
               (ni - num_mol_cluster_st1 - num_mol_cluster_st2 -
                num_mol_cluster_sv1) *
                   nsolvent2 +
               ai - 1;
          fprintf(output_PR_force_on_weighted_graph,
                  "%d ( sv2 %d atom %d ) force: %e %e %e \n", ni + 1, nj, ai,
                  Fx[ak], Fy[ak], Fz[ak]);
        }
      }
    }
  }

  /* free memory allocated here */
  free(totalWj);

  for (ni = 0; ni < num_node; ni++) {
    for (nj = 0; nj < num_node; nj++) {
      free(dSij_dxk[ni][nj]);
      free(dSij_dyk[ni][nj]);
      free(dSij_dzk[ni][nj]);
    }
    free(dSij_dxk[ni]);
    free(dSij_dyk[ni]);
    free(dSij_dzk[ni]);
  }
  free(dSij_dxk);
  free(dSij_dyk);
  free(dSij_dzk);

  for (ni = 0; ni < num_node; ni++) {
    free(dPRi_dxk[ni]);
    free(dPRi_dyk[ni]);
    free(dPRi_dzk[ni]);
  }
  free(dPRi_dxk);
  free(dPRi_dyk);
  free(dPRi_dzk);

  free(Fx);
  free(Fy);
  free(Fz);

  return (0);
};

/* this is a function to create the Adjacency matrix of weighted graph based on
 * the coordinates from selected cluster */
int ChemNetworkOrig::ChemNetwork_Weighted_Graph::create_WG_Adj_from_cluster(
    FILE *output_weighted_graph, double **WG_Adj,
    struct Mol_identity *WG_Mol_id, double ***dWij_dxk, double ***dWij_dyk,
    double ***dWij_dzk, double *atom_cluster_st1, int num_mol_cluster_st1,
    int nsolute1, double *atom_cluster_st2, int num_mol_cluster_st2,
    int nsolute2, double *atom_cluster_sv1, int num_mol_cluster_sv1,
    int nsolvent1, double *atom_cluster_sv2, int num_mol_cluster_sv2,
    int nsolvent2, int index_wg_st1_sv1, int num_wg_st1_sv1_dist,
    int atom1_wg_st1_sv1[NUM_INTER], int atom2_wg_st1_sv1[NUM_INTER],
    int funct_type_wg_st1_sv1, double funct_par1_wg_st1_sv1,
    double funct_par2_wg_st1_sv1, int index_wg_st1_sv2, int num_wg_st1_sv2_dist,
    int atom1_wg_st1_sv2[NUM_INTER], int atom2_wg_st1_sv2[NUM_INTER],
    int funct_type_wg_st1_sv2, double funct_par1_wg_st1_sv2,
    double funct_par2_wg_st1_sv2, int index_wg_st2_sv1, int num_wg_st2_sv1_dist,
    int atom1_wg_st2_sv1[NUM_INTER], int atom2_wg_st2_sv1[NUM_INTER],
    int funct_type_wg_st2_sv1, double funct_par1_wg_st2_sv1,
    double funct_par2_wg_st2_sv1, int index_wg_st2_sv2, int num_wg_st2_sv2_dist,
    int atom1_wg_st2_sv2[NUM_INTER], int atom2_wg_st2_sv2[NUM_INTER],
    int funct_type_wg_st2_sv2, double funct_par1_wg_st2_sv2,
    double funct_par2_wg_st2_sv2, int index_wg_sv1_sv1, int num_wg_sv1_sv1_dist,
    int atom1_wg_sv1_sv1[NUM_INTER], int atom2_wg_sv1_sv1[NUM_INTER],
    int funct_type_wg_sv1_sv1, double funct_par1_wg_sv1_sv1,
    double funct_par2_wg_sv1_sv1, int index_wg_sv2_sv2, int num_wg_sv2_sv2_dist,
    int atom1_wg_sv2_sv2[NUM_INTER], int atom2_wg_sv2_sv2[NUM_INTER],
    int funct_type_wg_sv2_sv2, double funct_par1_wg_sv2_sv2,
    double funct_par2_wg_sv2_sv2, int index_wg_sv1_sv2, int num_wg_sv1_sv2_dist,
    int atom1_wg_sv1_sv2[NUM_INTER], int atom2_wg_sv1_sv2[NUM_INTER],
    int funct_type_wg_sv1_sv2, double funct_par1_wg_sv1_sv2,
    double funct_par2_wg_sv1_sv2, int index_wg_st1_st2, int num_wg_st1_st2_dist,
    int atom1_wg_st1_st2[NUM_INTER], int atom2_wg_st1_st2[NUM_INTER],
    int funct_type_wg_st1_st2, double funct_par1_wg_st1_st2,
    double funct_par2_wg_st1_st2, double xside, double yside, double zside) {

  int nodei, nodej, atomi, atomj, typei, typej, indexi, indexj, totalnum, iter,
      atomk;
  double sitei_x, sitei_y, sitei_z, sitej_x, sitej_y, sitej_z, temp;
  double dist_ij, weight_ij, dist_ij_x, dist_ij_y, dist_ij_z;

  totalnum =
      num_mol_cluster_st1 + num_mol_cluster_st2 + num_mol_cluster_sv1 +
      num_mol_cluster_sv2; // currently 2 solute and 2 solvents are implemented

  for (nodei = 0; nodei < totalnum; nodei++) {
    if (WG_Mol_id[nodei].solute_type == 1 && WG_Mol_id[nodei].solvent_type == 0)
      typei = 1; // solute1

    if (WG_Mol_id[nodei].solute_type == 2 && WG_Mol_id[nodei].solvent_type == 0)
      typei = 2; // solute2

    if (WG_Mol_id[nodei].solute_type == 0 && WG_Mol_id[nodei].solvent_type == 1)
      typei = 3; // solvent1

    if (WG_Mol_id[nodei].solute_type == 0 && WG_Mol_id[nodei].solvent_type == 2)
      typei = 4; // solvent2

    for (nodej = 0; nodej < totalnum; nodej++) {
      if (nodej == nodei)
        WG_Adj[nodei][nodej] = 0.0;
      else {
        if (WG_Mol_id[nodej].solute_type == 1 &&
            WG_Mol_id[nodej].solvent_type == 0)
          typej = 1;

        if (WG_Mol_id[nodej].solute_type == 2 &&
            WG_Mol_id[nodej].solvent_type == 0)
          typej = 2;

        if (WG_Mol_id[nodej].solute_type == 0 &&
            WG_Mol_id[nodej].solvent_type == 1)
          typej = 3;

        if (WG_Mol_id[nodej].solute_type == 0 &&
            WG_Mol_id[nodej].solvent_type == 2)
          typej = 4;

        // printf("   Adj %d %d %d %d\n",nodei,nodej,typei,typej);

        /* get the weights for the requested edges*/
        if (typei == 1) {
          if (typej == 1)
            WG_Adj[nodei][nodej] =
                0.0;           // no solute1 - solute1 interaction is included
          else if (typej == 2) // solute1 - solute2 interaction
          {
            if (index_wg_st1_st2 ==
                1) // edge weight if solute1-solute2 is requested
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_st2_dist; iter++) {
                indexi = nodei;
                indexj = nodej - num_mol_cluster_st1;

                dist_ij = wg_site_distance(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_st2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom2_wg_st1_st2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_st2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom2_wg_st1_st2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_st2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom2_wg_st1_st2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_st2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom2_wg_st1_st2[iter],
                    2, zside);

                if (funct_type_wg_st1_st2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st1_st2 *
                                       (dist_ij - funct_par2_wg_st1_st2) /
                                       funct_par2_wg_st1_st2));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_st2 *
                                          (dist_ij - funct_par2_wg_st1_st2) /
                                          funct_par2_wg_st1_st2));
                  atomk = indexi * nsolute1 + atom1_wg_st1_st2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 + indexj * nsolute2 +
                          atom2_wg_st1_st2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute1-solute2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_st2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 3) {
            if (index_wg_st1_sv1 == 1) // get edge weights when solute1 -
                                       // solvent1 interaction is requested
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_sv1_dist;
                   iter++) // the weight between nodei and nodej is additive
                           // over all the possible interaction sites
              {
                indexi = nodei;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2;

                dist_ij = wg_site_distance(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st1_sv1[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st1_sv1[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st1_sv1[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st1_sv1[iter],
                    2, zside);

                if (funct_type_wg_st1_sv1 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 /
                          (1.0 + exp(funct_par1_wg_st1_sv1 *
                                     (dist_ij - funct_par2_wg_st1_sv1) /
                                     funct_par2_wg_st1_sv1)); // fermi-function,
                                                              // sigmoid-type

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_sv1 *
                                          (dist_ij - funct_par2_wg_st1_sv1) /
                                          funct_par2_wg_st1_sv1));
                  atomk = indexi * nsolute1 + atom1_wg_st1_sv1[iter] -
                          1; // -1 because atom index in atom1_wg_st1_sv1[] is
                             // labeled starting from 1
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexj * nsolvent1 +
                          atom2_wg_st1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else { /* add other functional formulisms here */
                  printf("warning in solute1-solvent1 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_sv1);
                  weight_ij = 0.0;
                }
              }

              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;

          } else if (typej == 4) {
            if (index_wg_st1_sv2 == 1) // edge of solute1 - solvent2
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_sv2_dist; iter++) {
                indexi = nodei;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;

                dist_ij = wg_site_distance(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st1_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st1_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st1_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st1, indexi, nsolute1, atom1_wg_st1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st1_sv2[iter],
                    2, zside);

                if (funct_type_wg_st1_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st1_sv2 *
                                       (dist_ij - funct_par2_wg_st1_sv2) /
                                       funct_par2_wg_st1_sv2));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_sv2 *
                                          (dist_ij - funct_par2_wg_st1_sv2) /
                                          funct_par2_wg_st1_sv2));
                  atomk = indexi * nsolute1 + atom1_wg_st1_sv2[iter] -
                          1; // -1 because atom index in atom1_wg_st1_sv1[] is
                             // labeled starting from 1
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexj * nsolvent2 +
                          atom2_wg_st1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute1-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          }
        } else if (typei == 2) // solute2 - xx edges
        {
          if (typej == 1) {
            if (index_wg_st1_st2 == 1) // solute2-solute1 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_st2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1;
                indexj = nodej;

                dist_ij = wg_site_distance(
                    atom_cluster_st2, indexi, nsolute2, atom2_wg_st1_st2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_st2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom2_wg_st1_st2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_st2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom2_wg_st1_st2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_st2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom2_wg_st1_st2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_st2[iter],
                    2, zside);

                if (funct_type_wg_st1_st2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st1_st2 *
                                       (dist_ij - funct_par2_wg_st1_st2) /
                                       funct_par2_wg_st1_st2));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_st2 *
                                          (dist_ij - funct_par2_wg_st1_st2) /
                                          funct_par2_wg_st1_st2));
                  atomk = num_mol_cluster_st1 * nsolute1 + indexi * nsolute2 +
                          atom2_wg_st1_st2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      dist_ij_z / dist_ij;
                  atomk = indexj * nsolute1 + atom1_wg_st1_st2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_st2 / funct_par2_wg_st1_st2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute1-solute2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_st2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;

          } else if (typej == 2)
            WG_Adj[nodei][nodej] =
                0.0; // by default, there is no solute2-solute2 interaction
          else if (typej == 3) {
            if (index_wg_st2_sv1 == 1) // solute2-solvent1 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st2_sv1_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2;

                dist_ij = wg_site_distance(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st2_sv1[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st2_sv1[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st2_sv1[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_st2_sv1[iter],
                    2, zside);

                if (funct_type_wg_st2_sv1 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st2_sv1 *
                                       (dist_ij - funct_par2_wg_st2_sv1) /
                                       funct_par2_wg_st2_sv1));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st2_sv1 *
                                          (dist_ij - funct_par2_wg_st2_sv1) /
                                          funct_par2_wg_st2_sv1));
                  atomk = num_mol_cluster_st1 * nsolute1 + indexi * nsolute2 +
                          atom1_wg_st2_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexj * nsolvent1 +
                          atom2_wg_st2_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute2-solvent1 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st2_sv1);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;

          } else if (typej == 4) // solute2-solvent2 edges
          {
            if (index_wg_st2_sv2 == 1) {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st2_sv2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;

                dist_ij = wg_site_distance(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st2_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st2_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st2_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_st2, indexi, nsolute2, atom1_wg_st2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_st2_sv2[iter],
                    2, zside);

                if (funct_type_wg_st2_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st2_sv2 *
                                       (dist_ij - funct_par2_wg_st2_sv2) /
                                       funct_par2_wg_st2_sv2));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st2_sv2 *
                                          (dist_ij - funct_par2_wg_st2_sv2) /
                                          funct_par2_wg_st2_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 + indexi * nsolute2 +
                          atom1_wg_st2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexj * nsolvent2 +
                          atom2_wg_st2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute2-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st2_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          }
        } else if (typei == 3) // solvent1 - xx edges
        {
          if (typej == 1) {
            if (index_wg_st1_sv1 == 1) // solvent1-solute1 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_sv1_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2;
                indexj = nodej;

                dist_ij = wg_site_distance(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st1_sv1[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv1[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st1_sv1[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv1[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st1_sv1[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv1[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st1_sv1[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv1[iter],
                    2, zside);

                if (funct_type_wg_st1_sv1 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st1_sv1 *
                                       (dist_ij - funct_par2_wg_st1_sv1) /
                                       funct_par2_wg_st1_sv1));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_sv1 *
                                          (dist_ij - funct_par2_wg_st1_sv1) /
                                          funct_par2_wg_st1_sv1));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexi * nsolvent1 +
                          atom2_wg_st1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      dist_ij_z / dist_ij;
                  atomk = indexj * nsolute1 + atom1_wg_st1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv1 / funct_par2_wg_st1_sv1 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute1-solvent1 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_sv1);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 2) // solvent1 - solute2 edges
          {
            if (index_wg_st2_sv1 == 1) {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st2_sv1_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2;
                indexj = nodej - num_mol_cluster_st1;

                dist_ij = wg_site_distance(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st2_sv1[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv1[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st2_sv1[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv1[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st2_sv1[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv1[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom2_wg_st2_sv1[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv1[iter],
                    2, zside);

                if (funct_type_wg_st2_sv1 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st2_sv1 *
                                       (dist_ij - funct_par2_wg_st2_sv1) /
                                       funct_par2_wg_st2_sv1));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st2_sv1 *
                                          (dist_ij - funct_par2_wg_st2_sv1) /
                                          funct_par2_wg_st2_sv1));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexi * nsolvent1 +
                          atom2_wg_st2_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 + indexj * nsolute2 +
                          atom1_wg_st2_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv1 / funct_par2_wg_st2_sv1 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute2-solvent1 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st2_sv1);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 3) // this is solvent1 - solvent1 interaction
          {
            if (index_wg_sv1_sv1 == 1) {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_sv1_sv1_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2;

                dist_ij = wg_site_distance(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_sv1_sv1[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_sv1_sv1[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_sv1_sv1[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv1[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom2_wg_sv1_sv1[iter],
                    2, zside);

                if (funct_type_wg_sv1_sv1 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_sv1_sv1 *
                                       (dist_ij - funct_par2_wg_sv1_sv1) /
                                       funct_par2_wg_sv1_sv1));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_sv1_sv1 *
                                          (dist_ij - funct_par2_wg_sv1_sv1) /
                                          funct_par2_wg_sv1_sv1));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexi * nsolvent1 +
                          atom1_wg_sv1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexj * nsolvent1 +
                          atom2_wg_sv1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv1 / funct_par2_wg_sv1_sv1 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solvent1-solvent1 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_sv1_sv1);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 4) {
            if (index_wg_sv1_sv2 ==
                1) // this is solvent1 - solvent2 interaction
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_sv1_sv1_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;

                dist_ij = wg_site_distance(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv1_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv1_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv1_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv1, indexi, nsolvent1, atom1_wg_sv1_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv1_sv2[iter],
                    2, zside);

                if (funct_type_wg_sv1_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_sv1_sv2 *
                                       (dist_ij - funct_par2_wg_sv1_sv2) /
                                       funct_par2_wg_sv1_sv2));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_sv1_sv2 *
                                          (dist_ij - funct_par2_wg_sv1_sv2) /
                                          funct_par2_wg_sv1_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexi * nsolvent1 +
                          atom1_wg_sv1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexj * nsolvent2 +
                          atom2_wg_sv1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solvent1-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_sv1_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          }

        } else if (typei == 4) // solvent2 - xx edges
        {
          if (typej == 1) {
            if (index_wg_st1_sv2 == 1) // solvent2 - solute1 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st1_sv2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;
                indexj = nodej;

                dist_ij = wg_site_distance(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st1_sv2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st1_sv2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st1_sv2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st1_sv2[iter],
                    atom_cluster_st1, indexj, nsolute1, atom1_wg_st1_sv2[iter],
                    2, zside);

                if (funct_type_wg_st1_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st1_sv2 *
                                       (dist_ij - funct_par2_wg_st1_sv2) /
                                       funct_par2_wg_st1_sv2));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st1_sv2 *
                                          (dist_ij - funct_par2_wg_st1_sv2) /
                                          funct_par2_wg_st1_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexi * nsolvent2 +
                          atom2_wg_st1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = indexj * nsolute1 + atom1_wg_st1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st1_sv2 / funct_par2_wg_st1_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute1-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st1_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 2) {
            if (index_wg_st2_sv2 == 1) // solvent2 - solute2 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_st2_sv2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;
                indexj = nodej - num_mol_cluster_st1;

                dist_ij = wg_site_distance(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st2_sv2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st2_sv2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st2_sv2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_st2_sv2[iter],
                    atom_cluster_st2, indexj, nsolute2, atom1_wg_st2_sv2[iter],
                    2, zside);

                if (funct_type_wg_st2_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_st2_sv2 *
                                       (dist_ij - funct_par2_wg_st2_sv2) /
                                       funct_par2_wg_st2_sv2));

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_st2_sv2 *
                                          (dist_ij - funct_par2_wg_st2_sv2) /
                                          funct_par2_wg_st2_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexi * nsolvent2 +
                          atom2_wg_st2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 + indexj * nsolute2 +
                          atom1_wg_st2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_st2_sv2 / funct_par2_wg_st2_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solute2-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_st2_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;

          } else if (typej == 3) {
            if (index_wg_sv1_sv2 == 1) // solvent2 - solvent1 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_sv1_sv2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2;

                dist_ij = wg_site_distance(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_sv1_sv2[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom1_wg_sv1_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_sv1_sv2[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom1_wg_sv1_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_sv1_sv2[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom1_wg_sv1_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom2_wg_sv1_sv2[iter],
                    atom_cluster_sv1, indexj, nsolvent1, atom1_wg_sv1_sv2[iter],
                    2, zside);

                if (funct_type_wg_sv1_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_sv1_sv2 *
                                       (dist_ij - funct_par2_wg_sv1_sv2) /
                                       funct_par2_wg_sv1_sv2));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_sv1_sv2 *
                                          (dist_ij - funct_par2_wg_sv1_sv2) /
                                          funct_par2_wg_sv1_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexi * nsolvent2 +
                          atom2_wg_sv1_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 + indexj * nsolvent1 +
                          atom1_wg_st1_sv1[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv1_sv2 / funct_par2_wg_sv1_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solvent1-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_sv1_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          } else if (typej == 4) {
            if (index_wg_sv2_sv2 == 1) // solvent2 - solvent2 edges
            {
              weight_ij = 0.0;
              for (iter = 0; iter < num_wg_sv2_sv2_dist; iter++) {
                indexi = nodei - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;
                indexj = nodej - num_mol_cluster_st1 - num_mol_cluster_st2 -
                         num_mol_cluster_sv1;

                dist_ij = wg_site_distance(
                    atom_cluster_sv2, indexi, nsolvent2, atom1_wg_sv2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv2_sv2[iter],
                    xside, yside, zside);
                dist_ij_x = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom1_wg_sv2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv2_sv2[iter],
                    0, xside);
                dist_ij_y = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom1_wg_sv2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv2_sv2[iter],
                    1, yside);
                dist_ij_z = wg_site_separation(
                    atom_cluster_sv2, indexi, nsolvent2, atom1_wg_sv2_sv2[iter],
                    atom_cluster_sv2, indexj, nsolvent2, atom2_wg_sv2_sv2[iter],
                    2, zside);

                if (funct_type_wg_sv2_sv2 == 1) {
                  weight_ij =
                      weight_ij +
                      1.0 / (1.0 + exp(funct_par1_wg_sv2_sv2 *
                                       (dist_ij - funct_par2_wg_sv2_sv2) /
                                       funct_par2_wg_sv2_sv2));

                  // printf("   %f : %f %f %f : %f \n",dist_ij, dist_ij_x,
                  // dist_ij_y, dist_ij_z, weight_ij);

                  temp = 1.0 / (1.0 + exp(funct_par1_wg_sv2_sv2 *
                                          (dist_ij - funct_par2_wg_sv2_sv2) /
                                          funct_par2_wg_sv2_sv2));
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexi * nsolvent2 +
                          atom1_wg_sv2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      dist_ij_x / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      dist_ij_y / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      dist_ij_z / dist_ij;
                  atomk = num_mol_cluster_st1 * nsolute1 +
                          num_mol_cluster_st2 * nsolute2 +
                          num_mol_cluster_sv1 * nsolvent1 + indexj * nsolvent2 +
                          atom2_wg_sv2_sv2[iter] - 1;
                  dWij_dxk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      (-1.0 * dist_ij_x) / dist_ij;
                  dWij_dyk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      (-1.0 * dist_ij_y) / dist_ij;
                  dWij_dzk[nodei][nodej][atomk] +=
                      -1.0 * temp * temp * (1.0 / temp - 1.0) *
                      funct_par1_wg_sv2_sv2 / funct_par2_wg_sv2_sv2 *
                      (-1.0 * dist_ij_z) / dist_ij;
                } else {
                  printf("warning in solvent2-solvent2 edge: "
                         "weight-function-type %d is not implemented\n",
                         funct_type_wg_sv2_sv2);
                  weight_ij = 0.0;
                }
              }
              WG_Adj[nodei][nodej] = weight_ij;
            } else
              WG_Adj[nodei][nodej] = 0.0;
          }

        } // end of ' if(typei == 1) else if == 2 else if == 3  == 4'

      } // end of ' if(nodej == nodei) else '

    } // end of ' for(nodej = 0; nodej < totalnum; nodej++) '
  }   // end of ' for(nodei = 0; nodei < totalnum; nodei++) '

  /* print out the Adjacency matrix to an output-file */
  for (nodei = 0; nodei < totalnum; nodei++) {
    for (nodej = 0; nodej < totalnum; nodej++) {
      if (WG_Adj[nodei][nodej] > 0.0) // only print the non-zero weights
      {
        if (WG_Mol_id[nodei].solute_type != 0) {
          if (WG_Mol_id[nodej].solute_type != 0)
            fprintf(output_weighted_graph,
                    "%d ( st%d %d ) - %d ( st%d %d ) edge-weight: %lf \n",
                    nodei + 1, WG_Mol_id[nodei].solute_type,
                    WG_Mol_id[nodei].id, nodej + 1,
                    WG_Mol_id[nodej].solute_type, WG_Mol_id[nodej].id,
                    WG_Adj[nodei][nodej]);
          else if (WG_Mol_id[nodej].solute_type == 0)
            fprintf(output_weighted_graph,
                    "%d ( st%d %d ) - %d ( sv%d %d ) edge-weight: %lf \n",
                    nodei + 1, WG_Mol_id[nodei].solute_type,
                    WG_Mol_id[nodei].id, nodej + 1,
                    WG_Mol_id[nodej].solvent_type, WG_Mol_id[nodej].id,
                    WG_Adj[nodei][nodej]);

        } else if (WG_Mol_id[nodei].solute_type == 0) {
          if (WG_Mol_id[nodej].solute_type != 0)
            fprintf(output_weighted_graph,
                    "%d ( sv%d %d ) - %d ( st%d %d ) edge-weight: %lf \n",
                    nodei + 1, WG_Mol_id[nodei].solvent_type,
                    WG_Mol_id[nodei].id, nodej + 1,
                    WG_Mol_id[nodej].solute_type, WG_Mol_id[nodej].id,
                    WG_Adj[nodei][nodej]);
          else if (WG_Mol_id[nodej].solute_type == 0)
            fprintf(output_weighted_graph,
                    "%d ( sv%d %d ) - %d ( sv%d %d ) edge-weight: %lf \n",
                    nodei + 1, WG_Mol_id[nodei].solvent_type,
                    WG_Mol_id[nodei].id, nodej + 1,
                    WG_Mol_id[nodej].solvent_type, WG_Mol_id[nodej].id,
                    WG_Adj[nodei][nodej]);
        }
      }
    }
  }

  return (0);
};

/* PageRank calculation based on an Adjacency matrix for weighted graph */
int ChemNetworkOrig::ChemNetwork_Weighted_Graph::calculate_PR_from_cluster_Adj(
    FILE *output_PR_on_weighted_graph, double *PR_vector, int total_num_nodes,
    double **WG_Adj, double **PR_Smatrix, double **PR_inverse_IdS,
    struct Mol_identity *WG_Mol_id, double damping_factor) {
  int pi, pj;
  double *W_column, *PR_initial;
  double **I_dS;
  int tag;

  /*
    printf("\n");
    for(pi=0; pi < total_num_nodes; pi++){
      for(pj=0; pj < total_num_nodes; pj++)
        printf("%e ", WG_Adj[pi][pj]);
      printf("\n");
    }
    printf("\n");
  */

  PR_initial = (double *)malloc(total_num_nodes * sizeof(double));
  for (pi = 0; pi < total_num_nodes; pi++) {
    PR_initial[pi] = 1.0 / total_num_nodes;
  }

  W_column = (double *)malloc(total_num_nodes * sizeof(double));
  for (pj = 0; pj < total_num_nodes; pj++) {
    W_column[pj] = 0.0;
    for (pi = 0; pi < total_num_nodes; pi++)
      W_column[pj] += WG_Adj[pi][pj];
  }

  for (pi = 0; pi < total_num_nodes; pi++) {
    for (pj = 0; pj < total_num_nodes; pj++) {
      if (W_column[pj] == 0.0)
        PR_Smatrix[pi][pj] = 1.0 / total_num_nodes;
      else
        PR_Smatrix[pi][pj] = WG_Adj[pi][pj] / W_column[pj];
    }
  }

  /*
    printf("\n");
    for(pi=0; pi < total_num_nodes; pi++)
      printf("%e ",W_column[pi]);
    printf("\n");

    printf("\n");
    for(pi=0; pi < total_num_nodes; pi++){
      for(pj=0; pj < total_num_nodes; pj++)
        printf("%e ", PR_Smatrix[pi][pj]);
      printf("\n");
    }
    printf("\n");
  */

  I_dS = (double **)malloc(total_num_nodes * sizeof(double));
  for (pi = 0; pi < total_num_nodes; pi++)
    I_dS[pi] = (double *)calloc(total_num_nodes, sizeof(double));

  for (pi = 0; pi < total_num_nodes; pi++) {
    for (pj = 0; pj < total_num_nodes; pj++) {
      if (pj == pi)
        I_dS[pi][pj] = 1.0 - damping_factor * PR_Smatrix[pi][pj];
      else
        I_dS[pi][pj] = 0.0 - damping_factor * PR_Smatrix[pi][pj];
    }
  }

  /*
    printf("\n");
    for(pi=0; pi < total_num_nodes; pi++){
      for(pj=0; pj < total_num_nodes; pj++)
        printf("%e ", I_dS[pi][pj]);
      printf("\n");
    }
    printf("\n");
  */

  tag = get_inverse(
      total_num_nodes, PR_inverse_IdS,
      I_dS); /* get the inverse of a matrix using Gauss-Jordan method */
  if (tag != 0) {
    printf("Error: cannot get the matrix inverse\n");
    return (-1);
  }

  /*
    printf("\n");
    for(pi=0; pi < total_num_nodes; pi++) {
      for(pj=0; pj < total_num_nodes; pj++)
        printf("%e ",PR_inverse_IdS[pi][pj]);
      printf("\n");
    }
    printf("\n");
  */

  for (pi = 0; pi < total_num_nodes; pi++) {
    PR_vector[pi] = 0.0;
    for (pj = 0; pj < total_num_nodes; pj++)
      PR_vector[pi] +=
          PR_inverse_IdS[pi][pj] * (1.0 - damping_factor) * PR_initial[pj];
  }

  /* print the PageRank vector into the output file */
  for (pi = 0; pi < total_num_nodes; pi++) {
    if (WG_Mol_id[pi].solute_type != 0)
      fprintf(output_PR_on_weighted_graph, "%d ( st%d %d ) pagerank: %e \n",
              pi + 1, WG_Mol_id[pi].solute_type, WG_Mol_id[pi].id,
              PR_vector[pi]);
    else
      fprintf(output_PR_on_weighted_graph, "%d ( sv%d %d ) pagerank: %e \n",
              pi + 1, WG_Mol_id[pi].solvent_type, WG_Mol_id[pi].id,
              PR_vector[pi]);
  }

  /* free memory */
  free(W_column);
  free(PR_initial);
  for (pi = 0; pi < total_num_nodes; pi++) {
    free(I_dS[pi]);
  }
  free(I_dS);

  return (0);
};

/* get the inverse of a matrix using Gauss-Jordan method, in which GJ_matrix (
 * Matrix | I ) is reduced to ( I | Inverse_Matrix ) using row operations */
int ChemNetworkOrig::ChemNetwork_Weighted_Graph::get_inverse(
    int size_of_matrix, double **Inverse_Matrix, double **Matrix) {
  int ii, jj, kk;
  double **GJ_matrix, temp;
  int result = 0;

  GJ_matrix = (double **)malloc(size_of_matrix * sizeof(double *));
  for (ii = 0; ii < size_of_matrix; ii++)
    GJ_matrix[ii] = (double *)calloc(size_of_matrix * 2, sizeof(double));

  for (ii = 0; ii < size_of_matrix; ii++) {
    for (jj = 0; jj < size_of_matrix * 2; jj++) {
      if (jj < size_of_matrix)
        GJ_matrix[ii][jj] = Matrix[ii][jj];
      else {
        if (jj == ii + size_of_matrix)
          GJ_matrix[ii][jj] = 1.0;
        else
          GJ_matrix[ii][jj] = 0.0;
      }
    }
  }

  /*  this is used in debug
    for(ii=0; ii < size_of_matrix; ii++)
    {
      for(jj=0; jj < size_of_matrix * 2; jj++)
        printf("%e ", GJ_matrix[ii][jj]);
      printf("\n");
    }
  */

  for (ii = 0; ii < size_of_matrix; ii++) {
    if (GJ_matrix[ii][ii] == 0.0) // if this row, ii, starts with 0, find
                                  // another row_j, and swap it with another row
    {
      for (jj = ii + 1; jj < size_of_matrix; jj++)
        if (GJ_matrix[jj][ii] != 0.0)
          break;

      if (jj >= size_of_matrix) {
        printf("  Error: matrix is not invertible %d\n", ii);
        result = -1;
        break;
      } else {
        for (kk = 0; kk < size_of_matrix * 2; kk++) {
          temp = GJ_matrix[ii][kk];
          GJ_matrix[ii][kk] = GJ_matrix[jj][kk];
          GJ_matrix[jj][kk] = temp;
        }
      }

    } // after this if statement, the first element in row ii is non-zero

    temp = GJ_matrix[ii][ii];
    for (kk = 0; kk < size_of_matrix * 2; kk++)
      GJ_matrix[ii][kk] = GJ_matrix[ii][kk] / temp;

    for (jj = 0; jj < size_of_matrix; jj++) {
      if (jj != ii) {
        temp = GJ_matrix[jj][ii];
        for (kk = 0; kk < size_of_matrix * 2; kk++)
          GJ_matrix[jj][kk] = GJ_matrix[jj][kk] - GJ_matrix[ii][kk] * temp;
      }
    }

  } // this is the end of ' for(ii=0; ii < size_of_matrix; ii++) ', so that
    // GJ_matrix is now ( I | Inverse_Matrix )

  /* this is used in debug
    for(ii=0; ii < size_of_matrix; ii++)
    {
      for(jj=0; jj < size_of_matrix * 2; jj++)
        printf("%e ", GJ_matrix[ii][jj]);
      printf("\n");
    }
  */

  for (ii = 0; ii < size_of_matrix; ii++)
    for (jj = 0; jj < size_of_matrix; jj++)
      Inverse_Matrix[ii][jj] = GJ_matrix[ii][jj + size_of_matrix];

  for (ii = 0; ii < size_of_matrix; ii++)
    free(GJ_matrix[ii]);
  free(GJ_matrix);

  return (result);
};

double ChemNetworkOrig::ChemNetwork_Weighted_Graph::wg_site_separation(
    double *atomM1, int idmolM1, int natmM1, int idatmM1, double *atomM2,
    int idmolM2, int natmM2, int idatmM2, int lab_xyz, double boxsize) {
  double result;
  double xi, xj;

  /* lab_xyz = 0 for x, 1 for y, 2 for z */
  xi = atomM1[(idmolM1 * natmM1 + (idatmM1 - 1)) * 3 + lab_xyz];
  xj = atomM2[(idmolM2 * natmM2 + (idatmM2 - 1)) * 3 + lab_xyz];

  result = xi - xj;

  if (result > boxsize * 0.5) {
    while (result > boxsize * 0.5) {
      result = result - boxsize;
    }
  } else if (result < -boxsize * 0.5) {
    while (result < -boxsize * 0.5) {
      result = result + boxsize;
    }
  }

  return (result);
}

double ChemNetworkOrig::ChemNetwork_Weighted_Graph::wg_site_distance(
    double *atomM1, int idmolM1, int natmM1, int idatmM1, double *atomM2,
    int idmolM2, int natmM2, int idatmM2, double xside, double yside,
    double zside) {
  double wg_distance = 0.0;
  double site1x, site1y, site1z, site2x, site2y, site2z;
  double minx, miny, minz;

  site1x = site1y = site1z = site2x = site2y = site2z = 0.0;

  site1x = atomM1[(idmolM1 * natmM1 + (idatmM1 - 1)) * 3];
  site1y = atomM1[(idmolM1 * natmM1 + (idatmM1 - 1)) * 3 + 1];
  site1z = atomM1[(idmolM1 * natmM1 + (idatmM1 - 1)) * 3 + 2];

  site2x = atomM2[(idmolM2 * natmM2 + (idatmM2 - 1)) * 3];
  site2y = atomM2[(idmolM2 * natmM2 + (idatmM2 - 1)) * 3 + 1];
  site2z = atomM2[(idmolM2 * natmM2 + (idatmM2 - 1)) * 3 + 2];

  minx = site1x - site2x;
  miny = site1y - site2y;
  minz = site1z - site2z;

  if (minx > xside * 0.5) { // minx is the shortest distance in x-direction,
                            // considering pbc
    while (minx >
           xside * 0.5) { // if the atom sites are separated by more than 1
                          // pbc-unit, this may happen in CP2K output-file
      minx = minx - xside;
    }
  } else if (minx < -xside * 0.5) {
    while (minx < -xside * 0.5) {
      minx = minx + xside;
    }
  }

  if (miny > yside * 0.5) {
    while (miny > yside * 0.5) {
      miny = miny - yside;
    }
  } else if (miny < -yside * 0.5) {
    while (miny < -yside * 0.5) {
      miny = miny + yside;
    }
  }

  if (minz > zside * 0.5) {
    while (minz > zside * 0.5) {
      minz = minz - zside;
    }
  } else if (minz < -zside * 0.5) {
    while (minz < -zside * 0.5) {
      minz = minz + zside;
    }
  }

  wg_distance = sqrt(minx * minx + miny * miny + minz * minz);

  return (wg_distance);
}

int ChemNetworkOrig::ChemNetwork_Weighted_Graph::findsolvent(
    int id_solvent_type, int number,
    int array[NUM_INTER]) { // check an solvent-id from an array
  int result = 0;
  int i;

  if (number > NUM_INTER) {
    printf("Error: size-overflow in function 'findsolvent'\n");
    result = -1;
  } else {
    for (i = 0; i < number; i++) {
      if (id_solvent_type == array[i]) {
        result = 1;
        break;
      }
    }
  }

  return (result);
}
