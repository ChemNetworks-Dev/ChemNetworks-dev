/*! \file common.h
    \brief common.h documentation

    This is to test the documentation of constant macros in common.h
*/

#ifndef _COMMON_H
#define _COMMON_H

/**this is a valid comment*/
#define MAX_CHAR_LENGTH 256

#define FLN 256

#define LARGE 100000 //!< and another test with large
#define MAX 100000   ///< Brief description after MAX test

/*!
 even another test with NUM_INTER
*/
#define NUM_INTER 50

#define PI 3.14159265

#endif
