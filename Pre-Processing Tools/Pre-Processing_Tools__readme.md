This folder contains several preprocessing scripts that can read the output files of different MD codes, wrap the coordinates appropriately, and then separate out the molecule or atom types into the different .xyz files that are analyzed by the ChemNetworks.exe

**note that ChemNetworks accounts for edges that cross periodic boundaries (cubic and rectangular boxes only), and as such it .xyz files must be wrapped.

