      PROGRAM waterwrap
!*********************************************************************
!     
!     program to convert HISTORY, .arc, .mov (from Ilja) 
!     and lammps trajectory files into individual snapshots based upon the solvent type,
!      wrap the box (if needed), and put in input file structure for SolutionNetworks
!     -will not wrap lammps trajectories, only put in right input file structure
!     
!     copyright daresbury laboratory 1997
!     author  w.smith june 1997
!     MODIFIED by auclark june 2012
!
!     aec
!     2012/06/21
!     2012/12/27
!     
!*********************************************************************
      integer, parameter :: mxatms = 200000
      character*40 :: fname, solnam,solnam2, solnam3, solnam4, solnam5
      character*80 :: title
      character*9, DIMENSION(mxatms) :: newnam,atmtyp, atmnam
      character*8, DIMENSION(5,100) :: sol
      character*8 :: dummy, cnum
      real*8, DIMENSION(mxatms) :: xxx,yyy,zzz
      real*8, DIMENSION(mxatms) :: vxx,vyy,vzz
      real*8, DIMENSION(mxatms) :: weight
      real*8, DIMENSION(9) :: cell
      integer, DIMENSION(mxatms) :: atlbl, soltyp
      integer, dimension(5) :: sol2atms
      real*8 :: rdum,cellxmin,cellxmax,cellymin
      real*8 :: cellymax,cellzmin,cellzmax
      integer :: levcfg,imcon,natms,nstep,matms,dummy2,wrap
      integer ::  type, solno, wattyp, molno, molsol1, molsol2
      integer ::  molsol3, molsol4
      real*8, parameter :: delr = 1.0d0
      integer :: iconf, iter, ftype
      integer :: typnu, k,i,j,l
      integer, dimension(5,100,1,2) :: bond  ! 2017.09.06, by Tiecheng
         ! size of bonds needs to be changed for big molecule like TBP
      integer, dimension(5) :: ibond, natmtot
      
!*********************************************************************
! Zero out key matrices
       do i=1,5
          sol2atms(i)=0
          sol(i,1)='XX'    
       enddo
          
!*********************************************************************
       cell(1)=0.0d0
       write(*,'(a)')'Enter name of file to convert'
       read(5,*)fname
       write(*, '(a)') 'Is this a HISTORY file (1), .arc file (2), xyz &
          &(3), mov file from Ilja (4), or lammps trajectory (5)?'
       read(5,*)ftype
     write(*,'(a)')'Do the coordinates need to be wrapped? Y(1) N(2)'
       read(5,*)wrap
      if(ftype.eq.1) then    !history file
        if(wrap.eq.2) goto 50
         write(*, '(a)') 'Please enter cubic box-side length'
         read(5,*) cell(1)
         cell(2)=0.0d0
         cell(3)=0.0d0
         cell(4)=0.0d0
         cell(5)=cell(1)
         cell(6)=0.0d0
         cell(7)=0.0d0
         cell(8)=0.0d0
         cell(9)=cell(1)
      endif
      if(ftype.eq.2) then    !arc file
         write(*, '(a)') 'Please enter total number of atoms' 
         read(5,*) natms
         if(wrap.eq.2) goto 50
         write(*, '(a)') 'Please enter cubic box-side length'
         read(5,*) cell(1)
      endif
      if(ftype.eq.3) then    !simple xyz file
         if(wrap.eq.2) goto 50 
         write(*, '(a)') 'Please enter x-side length'
         read(5,*) cell(1)
         write(*, '(a)') 'Please enter y-side length'
         read(5,*) cell(5)
         write(*, '(a)') 'Please enter z-side length'
         read(5,*) cell(9)
      endif
      if(ftype.eq.4) then    !mov file from Ilja MC code
        if(wrap.eq.2) goto 50
        write(*, '(a)') 'Please enter x box length'
        read(5,*) cell(1)
        write(*, '(a)') 'Please enter y box length'
        read(5,*) cell(5)
        write(*, '(a)') 'Please enter z box length'
        read(5,*) cell(9)
      endif
       if(ftype.eq.5) then   !lammps trajectory
         write(*, '(a)') 'How many atom types are there?'
         read(5,*) typnu
         write(*, '(a)') 'Please enter atom type lookup &
           &table for lammps trajectory (e.g., 1=C, 2=C)' 
         do i=1, typnu
            write(*, *) 'Atom Type', i, '='
            read(5,*) atmtyp(i) 
         enddo
       endif
50     continue

       write(*,'(a)')'Is the water model a 3-pt(1) or 4-pt(2)?'
       read(5,*) wattyp
       write(*, '(a)') 'Enter atom labels of water in order' 
         if(wattyp.eq.1) k=3
         if(wattyp.eq.2) k=4
         do i=1, k
            write(*, *) 'Atom ', i
            read(5,*) sol(1,i)
         enddo
      write(*,'(a)')'How many other solvent/solute types are there? '
       read(5,*) solno
!*********************************************************************
       if(solno.gt.0) then
         sol2atms(1)=3
         do i=2,solno+1
            write(*,*)'Number of atoms in species', i
            read(5,*) sol2atms(i)
           write(*,*)'Enter atom labels in order for species', i 
            do j=1, sol2atms(i)
               write(*, *) 'Atom ', j
               read(5,*) sol(i,j)
            enddo
!*********************************************************************
            write(*,'(a)') 'What is the total number of bonded pairs  &
               (e.d. 4 CT-HC bonds) in species', i 
            read(5,*) ibond(i)
            if(ibond(i).eq.0) go to 100
           write(*, '(a)')'Write bonded pairs of atoms in species',i
              do j=1, ibond(i)
                 write(*, *)'Atom i, Atom j (put in atom number,e.g. 1 &
                   &2)'
                 read(5,*)  bond(i,j,1,1), bond(i,j,1,2)
              enddo
100    continue
          enddo
         endif

!*********************************************************************
! Open file and read necessary headers outside the configuration loop

      open(7,file=fname)

      if(ftype.eq.1) then    !History file 
         read(7,'(a80)')title
         read(7,'(3i10)')levcfg,imcon,natms
      endif

!*********************************************************************
! the following is for every interval recorded
! YOU SHOULD CHECK THE HEADERS AND COLUMN WIDTHS OF THE HISTORY FILE, LOTS OF VARIATIONS
! for dl_poly2 the format statement is (a8,4i10,f12.6) and (3g20.10)
! for dl_poly3 the format statement is (a8,4i10,f12.6) and (3g12.6)

         iconf = 0
           do 
              iconf = iconf + 1
              do i=1,5   !zero out the natmtot directory for each snapshot
                  natmtot(i)=0
              enddo
              if(ftype.eq.1) then      !History file
                 read(7,*)
                 if(imcon.gt.0)then
!*********************************************************************
                   read(7,*)
                   read(7,*)
                   read(7,*)
                 else 
                    write(6,*)'IMCON & LEVCFG', imcon, levcfg
                 endif
               endif
               if(ftype.eq.5) then  ! this is determining the cell dimensions from the lmptraj 
                 read(7,*,err=501,end=505)
                 read(7,*,err=501,end=505)
                 read(7,*,err=501,end=505)
                 read(7,*,err=501,end=505) natms
                 write(6,*)'natms=', natms
                 read(7,*,err=501,end=505)
                 read(7,*,err=501,end=505) cellxmin, cellxmax
                 read(7,*,err=501,end=505) cellymin, cellymax
                 read(7,*,err=501,end=505) cellzmin, cellzmax
                 cell(1)=cellxmax-cellxmin  ! 2015.12.14, changed, Tiecheng
                 cell(5)=cellymax-cellymin  ! should it be max-min here
                 cell(9)=cellzmax-cellzmin
                 write(6,*)'total x dist=', cell(1)
                 write(6,*)'total y dist=', cell(5)
                 write(6,*)'total z dist=', cell(9)
                 read(7,*)
               endif
!*********************************************************************
               if(ftype.eq.2) then   !arc file
                   read(7,*) 
                    cell(2)=0.0d0
                    cell(3)=0.0d0
                    cell(4)=0.0d0
                    cell(5)=cell(1)
                    cell(6)=0.0d0
                    cell(7)=0.0d0
                    cell(8)=0.0d0
                    cell(9)=cell(1)
               endif 
               if(ftype.eq.3) then  !xyz file
                 read(7,*,err=501,end=505) natms
                 cell(2)=0.0d0
                 cell(3)=0.0d0
                 cell(4)=0.0d0
                 cell(6)=0.0d0
                 cell(7)=0.0d0
                 cell(8)=0.0d0 
               endif
               if(ftype.eq.4) then   !mov file from ilja
                 read(7,*,err=501,end=505) natms
                 read(7,*,err=501,end=505)
               endif
!*********************************************************************
! BEGIN LOOP OVER ATOMS IN A SINGLE CONFIGURATION
! for dl_poly2 the format statement is (a8,i10,2f12.6) and (3e19.10)
! for dl_poly3 the format statement is (a8,i10,2f12.6) and (3e12.4)
            i = 0
             do while (i < natms)
                i = i + 1
                if(ftype.eq.1) then !diff hist file versions
                  read(7,'(a8,i10,2f12.6)',err=501,end=505)atmnam(i),  &
                      atlbl(i),weight(i),rdum
                  read(7,*,err=501,end=505)xxx(i),yyy(i),zzz(i)
                   if(levcfg.gt.0)read(7,*)
                   if(levcfg.gt.1)read(7,*)
               endif
!                 write(6,*)'iconf, i, atmnam, xxx, yyy, zzz',         &
!                  iconf,i, atmnam(i), xxx(i), yyy(i), zzz(i)    
               if(ftype.eq.2) then   !arc file
               read(7, '(i6,3a2,3f12.6)',err=501,end=505) atlbl(i),    &
                     dummy, atmnam(i),dummy, xxx(i), yyy(i), zzz(i)
!                 write(6,*)'atmname(i)=', atmnam(i)
               end if
               if(ftype.eq.3) then    !xyz file
                 read(7,*) atmnam(i), xxx(i), yyy(i),zzz(i)
!                 write(6,*)'iconf, i, atmnam, xxx, yyy, zzz',         &
!                  iconf,i, atmnam(i), xxx(i), yyy(i), zzz(i)    
               endif
               if(ftype.eq.4) then  !ilja mov file
          read(7,*,err=501,end=505) atmnam(i), &
               xxx(i),yyy(i),zzz(i)     ! 2017.08.01, change format
!                 write(6,*) 'atmnam, x, y, z', atmnam(i),              &
!                    xxx(i), yyy(i), zzz(i)
               endif
               if(ftype.eq.5) then   !lammps traj file
               read(7,*,err=501,end=505) dummy, dummy, type, dummy,    &
                    xxx(i), yyy(i), zzz(i)
!                write(6,*) 'x,y,z', xxx(i), yyy(i), zzz(i)
                 atmnam(i)=atmtyp(type)
!                write(6,*)'atmnam=', atmnam(i)
               endif
             enddo
!***************************************************************
!Count the number of solvent molecules in the snapshot 
!Note there is a potential for a bug here if the 2nd solvent also begins
!with O
        
          if(solno.gt.0) then
             molno=0  !molecule # index
             molsol1=0
             molsol2=0
             molsol3=0
             molsol4=0
             molsol5=0
             k=1  !atom index
             do while (k < natms+1)
!                 write(6,*)'k, atmnam(k)', k, atmnam(k)
                 if(atmnam(k).eq.sol(1,1))then
!                  write(6,*)'k in h2o, atmnam(k)', k, atmnam(k)
                   molno=molno+1
                   molsol1=molsol1+1
                   if(wattyp.eq.1) k=k+3
                   if(wattyp.eq.2) k=k+4
!                  write(6,*)'molno=', molno
                else if(atmnam(k).eq.sol(2,1))then
!                 write(6,*)'k in sol2, atmnam(k)', k, atmnam(k)
!                 write(6,*)'sol2atms(2)=', sol2atms(2)
                  molno=molno+1
                  molsol2=molsol2+1
!                 write(6,*) 'molsol2=', molsol2
                  k=k+sol2atms(2)
!                 write(6,*)'molno=', molno
                else if(atmnam(k).eq.sol(3,1))then
!                 write(6,*)'k in sol3, atmnam(k)', k, atmnam(k)
                  molno=molno+1
                  molsol3=molsol3+1
                  k=k+sol2atms(3)
!                 write(6,*)'molno=', molno
                else if(atmnam(k).eq.sol(4,1))then
!                 write(6,*)'k in sol4, atmnam(k)', k, atmnam(k)
                  molno=molno+1
                  molsol4=molsol4+1
                  k=k+sol2atms(4)
!                 write(6,*)'molno=', molno
                else if(atmnam(k).eq.sol(5,1))then
!                 write(6,*)'k in sol5, atmnam(k)', k, atmnam(k)
                  molno=molno+1
                  molsol5=molsol5+1
                  k=k+sol2atms(5)
!                 write(6,*)'molno=', molno
!                 write(6,*)'sol2atms(5)=', sol2atms(5)
!***************************************************************
                else 
          write(6,*)'Cannot match atom labels to those provided'
                endif
            enddo
           else
             if(wattyp.eq.1) molno=natms/3
             if(wattyp.eq.2) molno=natms/4
           endif
!          write(6,*)'molno=', molno
!          write(6,*)'molsol1=', molsol1
!          write(6,*)'molsol2=', molsol2
!          write(6,*)'molsol3=', molsol3
!          write(6,*)'molsol4=', molsol4
!          write(6,*)'molsol5=', molsol5
!***************************************************************
!Separate the coordinates based on solvent type for a given configuration
!soltyp(i)=1 is always for water

             k=0
             do i=1, molno
                k=k+1
                if(atmnam(k).eq.sol(1,1)) then 
                  if(atmnam(k+1).eq.sol(1,2)) then
                    if(wattyp.eq.1) then
                     k=k+2
                     do j= k-2, k
                      soltyp(j)=1
                     enddo
                    else
                     k=k+3
                     do j=k-3, k
                      soltyp(j)=1
                     enddo
                    endif
                  endif
                endif
               if(atmnam(k).eq.sol(2,1)) then
                  k=k+(sol2atms(2)-1)
                  do j=k-(sol2atms(2)-1),k
                     soltyp(j)=2
                  enddo
                endif
              if(atmnam(k).eq.sol(3,1)) then
                  k=k+(sol2atms(3)-1)
                  do j=k-(sol2atms(3)-1),k
                     soltyp(j)=3
                  enddo
                endif
              if(atmnam(k).eq.sol(4,1)) then
                  k=k+(sol2atms(4)-1)
                  do j=k-(sol2atms(4)-1),k
                     soltyp(j)=4
                  enddo
                endif
              if(atmnam(k).eq.sol(5,1)) then
                  k=k+(sol2atms(5)-1)
                  do j=k-(sol2atms(5)-1),k
                     soltyp(j)=5
                  enddo
                endif
             enddo
!            do i=1,natms
!               write(6,*)'soltyp=', soltyp(i),i
!            enddo

!***************************************************************
!Change the name of the atoms to be consistent for all output files

! commented 2015.12.15, Tiecheng, 
! 2016.July, by comment this section, the main part is updated
!        do j=1, natms
!          if(atmnam(j) == 'H1')atmnam(j)='H '
!          if(atmnam(j) == 'H2')atmnam(j)='H '
!          if(atmnam(j) == '  H   ')atmnam(j)='H '
!          if(atmnam(j) == 'H ')atmnam(j)='H '
!          if(atmnam(j) == 'HW')atmnam(j)='H '
!          if(atmnam(j) == 'HC')atmnam(j)='H '
!          if(atmnam(j) == 'Hm')atmnam(j)='H '
!          if(atmnam(j) == '  O   ')atmnam(j)='O '
!          if(atmnam(j) == 'O ')atmnam(j)='O '
!          if(atmnam(j) == 'O')atmnam(j)='O '
!          if(atmnam(j) == 'Om')atmnam(j)='O '
!          if(atmnam(j) == 'OW')atmnam(j)='O '
!          if(atmnam(j) == 'C')atmnam(j)='C '
!          if(atmnam(j) == 'CT')atmnam(j)='C '
!          if(atmnam(j) == 'Cm')atmnam(j)='C '
!        enddo
!***************************************************************
!Wrap each molecule based upon distance to bonded atom --- main part

        if(wrap.eq.2) go to 600
        i=0   !atom index
        do j=1, molno
             i = i + 1
             if(soltyp(i).eq.1) then  ! for water molecule
             if(atmnam(i)=='O '.or.atmnam(i)=='OW'.or.atmnam(i)=='Ow')then
!         write(6,*)'atmnam(i) inside h2o loop = ',i, atmnam(i)
!          write(6,*)'j, i, atmnam, xxx, yyy, zzz',j,i,  &
!            atmnam(i), xxx(i), yyy(i), zzz(i) 
                 i = i + 1
             if(atmnam(i)=='H '.or.atmnam(i)=='HW'.or.atmnam(i)=='Hw')then
!         write(6,*)'atmnam(i) inside h2o loop = ',i, atmnam(i)
!          write(6,*)'j, i, atmnam, xxx, yyy, zzz',j,i,  &
!            atmnam(i), xxx(i), yyy(i), zzz(i) 
                   rx = xxx(i-1) - xxx(i)
                   ry = yyy(i-1) - yyy(i)
                   rz = zzz(i-1) - zzz(i)
                   rij = sqrt(rx**2 + ry**2 + rz**2)
                   if(rij .gt. 1.5)then
!                  write(6,*)'rij before', rij
                     if(rx .gt. 1.5)xxx(i) = xxx(i) + cell(1)
                     if(rx .lt. -1.5)xxx(i) = xxx(i) - cell(1)
                     if(ry .gt. 1.5)yyy(i) = yyy(i) + cell(5)
                     if(ry .lt. -1.5)yyy(i) = yyy(i) - cell(5)
                     if(rz .gt. 1.5)zzz(i) = zzz(i) + cell(9)
                     if(rz .lt. -1.5)zzz(i) = zzz(i) - cell(9)
                   rx = xxx(i-1) - xxx(i)
                   ry = yyy(i-1) - yyy(i)
                   rz = zzz(i-1) - zzz(i)
                   rij = sqrt(rx**2 + ry**2 + rz**2)
!                  write(6,*)'rij after', rij
                   if(rij .gt. 2.0) then
                     write(6,*)'potential error in wrapping&
                       large HB distances found'
                   endif
                   end if
                 else
                   write(6,*)'ERROR at water H-atom'
                   stop
                 end if
                 i = i + 1
        if(atmnam(i)=='H '.or.atmnam(i)=='HW'.or.atmnam(i)=='Hw')then ! the second H atom 
!***************************************************************
!         write(6,*)'atmnam(i) inside h2o loop = ',i, atmnam(i)
!          write(6,*)'j, i, atmnam, xxx, yyy, zzz',j,i,  &
!            atmnam(i), xxx(i), yyy(i), zzz(i) 
                   rx = xxx(i-2) - xxx(i)
                   ry = yyy(i-2) - yyy(i)
                   rz = zzz(i-2) - zzz(i)
                   rij = sqrt(rx**2 + ry**2 + rz**2)
!                  write(6,*)'rij before', rij
                   if(rij .gt. 1.5)then
                     if(rx .gt. 1.5)xxx(i) = xxx(i) + cell(1)
                     if(rx .lt. -1.5)xxx(i) = xxx(i) - cell(1)
                     if(ry .gt. 1.5)yyy(i) = yyy(i) + cell(5)
                     if(ry .lt. -1.5)yyy(i) = yyy(i) - cell(5)
                     if(rz .gt. 1.5)zzz(i) = zzz(i) + cell(9)
                     if(rz .lt. -1.5)zzz(i) = zzz(i) - cell(9)
                   rx = xxx(i-2) - xxx(i) ! changed i-1 to i-2
                   ry = yyy(i-2) - yyy(i)
                   rz = zzz(i-2) - zzz(i)
                   rij = sqrt(rx**2 + ry**2 + rz**2)
!                  write(6,*)'rij after', rij
                   end if
                   else
                     write(6,*)'ERROR at water H-atom'
                   stop
                 end if
                 if(wattyp.eq.2) then  !skips the 4th atom in 4pt water models
                   i=i+1
                 endif
               endif
             endif
!***************************************************************
             if(soltyp(i).gt.1) then              !looping over other solvent molecules
!        write(6,*)'atmnam(i) inside sol2 loop = ', i, atmnam(i)
               if(ibond(soltyp(i)).eq.0) go to 300
               do k=1,ibond(soltyp(i))
                    l=soltyp(i)
                         rx = xxx(i-1+bond(l,k,1,2)) -            &
                               xxx(i-1+bond(l,k,1,1))
                         ry = yyy(i-1+bond(l,k,1,2)) -            &
                               yyy(i-1+bond(l,k,1,1))
                         rz = zzz(i-1+bond(l,k,1,2)) -            &
                               zzz(i-1+bond(l,k,1,1))
                    rij = sqrt(rx**2 + ry**2 + rz**2)
!     write(6,*)'   molecule/bond',soltyp(i),bond(l,k,1,1), bond(l,k,1,2)
!                  write(6,*)'rx before shift=',  rx
!                  write(6,*)'ry before shift =', ry
!                  write(6,*)'rz before shift =', rz
!***************************************************************
               if(rij .gt. 3.5)then
!                  write(6,*)'rij before sol2=', rij
                    l=soltyp(i)
!                          if(rx .gt. 3.5)xxx(i+bond(l,k,1,2))  &
!                             = xxx(i+bond(l,k,1,2)) + cell(1)
                           if(rx .gt. 3.5)xxx(i-1+bond(l,k,1,2))  &
                              = xxx(i-1+bond(l,k,1,2)) - cell(1)
!                          if(rx .lt. -3.5)xxx(i+bond(l,k,1,2)) &
!                             = xxx(i+bond(l,k,1,2)) - cell(1)
                           if(rx .lt. -3.5)xxx(i-1+bond(l,k,1,2)) &
                              = xxx(i-1+bond(l,k,1,2)) + cell(1)
!                   write(6,*)'ry before shift =', ry
!               write(6,*)'yyy 2 before shift=', yyy(i-1+bond(l,k,1,2))
!                          if(ry .gt. 3.5)yyy(i+bond(l,k,1,2))  &
!                             = yyy(i+bond(l,k,1,2)) + cell(5)
                           if(ry .gt. 3.5)yyy(i-1+bond(l,k,1,2))  &
                              = yyy(i-1+bond(l,k,1,2)) - cell(5)
!                  write(6,*)'yyy 2 after shift=', yyy(i+bond(l,k,1,2))
!                          if(ry .lt. -3.5)yyy(i+bond(l,k,1,2)) & 
!                 write(6,*)'yyy 2 after shift=', yyy(i-1+bond(l,k,1,2))
                           if(ry .lt. -3.5)yyy(i-1+bond(l,k,1,2)) & 
                              = yyy(i-1+bond(l,k,1,2)) + cell(5)
!                   write(6,*)'rz before shift =', rz
!                          if(rz .gt. 3.5)zzz(i+bond(l,k,1,2))  &
!                             = zzz(i+bond(l,k,1,2)) + cell(9)
                           if(rz .gt. 3.5)zzz(i-1+bond(l,k,1,2))  &
                              = zzz(i-1+bond(l,k,1,2)) - cell(9)
!                          if(rz .lt. -3.5)zzz(i+bond(l,k,1,2)) & 
!                             = zzz(i+bond(l,k,1,2)) - cell(9)
                           if(rz .lt. -3.5)zzz(i-1+bond(l,k,1,2)) & 
                              = zzz(i-1+bond(l,k,1,2)) + cell(9)
                         rx = xxx(i-1+bond(l,k,1,2)) -            &
                               xxx(i-1+bond(l,k,1,1))
                         ry = yyy(i-1+bond(l,k,1,2)) -            &
                               yyy(i-1+bond(l,k,1,1))
                         rz = zzz(i-1+bond(l,k,1,2)) -            &
                               zzz(i-1+bond(l,k,1,1))
!                  write(6,*)'rx after shift=',  rx
!                  write(6,*)'ry after shift =', ry
!                  write(6,*)'rz after shift =', rz
                   rij = sqrt(rx**2 + ry**2 + rz**2)
                   if(rij .gt. 3.5) then
                     write(6,*)'potential error in wrapping&
                       large HB distances found'
                    write(6,*)'rij after shift=', rij
                    stop
                   endif
               end if
               enddo  ! end of loop from line 477
                 i=i+sol2atms(l)-1
300    continue
              endif   ! end if from line 403
           enddo  ! end of loop from line 401
600    continue
!
!***************************************************************
! Open relevant files for particular snapshot

400    continue

       if(iconf.lt.10) then
         write(cnum,'(I1.1)') iconf
       endif
       if((iconf.ge.10).and.(iconf.lt.100)) then
         write(cnum,'(I2.2)') iconf
       endif
       if((iconf.ge.100).and.(iconf.lt.1000)) then
         write(cnum,'(I3.3)') iconf
       endif
       if((iconf.ge.1000).and.(iconf.lt.10000)) then
         write(cnum,'(I4.4)') iconf
       endif
       if((iconf.ge.10000).and.(iconf.lt.100000)) then
         write(cnum,'(I5.5)') iconf
       endif
       if((iconf.ge.100000).and.(iconf.lt.1000000)) then ! added 12.03.16
         write(cnum,'(I6.6)') iconf
       endif
       write(6,*) 'cnum=', cnum
       solnam='water'//trim(cnum)//'.xyz'
       open(1, file=solnam)
       if(solno.eq.1) then
         solnam2='solB'//trim(cnum)//'.xyz'
         open(2, file=solnam2)
       endif
       if(solno.eq.2) then
         solnam2='solB'//trim(cnum)//'.xyz'
         open(2, file=solnam2)
         solnam3='solC'//trim(cnum)//'.xyz'
         open(3, file=solnam3)
       endif
       if(solno.eq.3) then
         solnam2='solB'//trim(cnum)//'.xyz'
         open(2, file=solnam2)
         solnam3='solC'//trim(cnum)//'.xyz'
         open(3, file=solnam3)
         solnam4='solD'//trim(cnum)//'.xyz'
         open(4, file=solnam4)
       endif
       if(solno.eq.4) then
         solnam2='solB'//trim(cnum)//'.xyz'
         open(2, file=solnam2)
         solnam3='solC'//trim(cnum)//'.xyz'
         open(3, file=solnam3)
         solnam4='solD'//trim(cnum)//'.xyz'
         open(4, file=solnam4)
         solnam5='solE'//trim(cnum)//'.xyz'
         open(5, file=solnam5)
       endif

!***************************************************************
! Now count how many atoms are in each snapshot for each species

      do i=1,natms
          if(soltyp(i).eq.1) then
            if(atmnam(i).eq.'M') goto 150
            natmtot(1)=natmtot(1)+1
150  continue
          endif
          if(soltyp(i).eq.2) then
            natmtot(2)=natmtot(2)+1
          endif
          if(soltyp(i).eq.3) then
            natmtot(3)=natmtot(3)+1
          endif
          if(soltyp(i).eq.4) then
            natmtot(4)=natmtot(4)+1
          endif
          if(soltyp(i).eq.5) then
            natmtot(5)=natmtot(5)+1
          endif
      enddo
!***************************************************************
! Now right header for every snapshot

        write(1,*)natmtot(1)
        write(1,*)
        if(solno.eq.1)  then
           write(2,*)natmtot(2) 
           write(2,*)
        endif
        if(solno.eq.2) then 
           write(2,*)natmtot(2) 
           write(2,*)
           write(3,*)natmtot(3) 
           write(3,*)
        endif
        if(solno.eq.3) then
           write(2,*)natmtot(2) 
           write(2,*)
           write(3,*)natmtot(3) 
           write(3,*)
           write(4,*)natmtot(4) 
           write(4,*)
        endif
        if(solno.eq.5) then
           write(2,*)natmtot(2) 
           write(2,*)
           write(3,*)natmtot(3) 
           write(3,*)
           write(4,*)natmtot(4) 
           write(4,*)
           write(5,*)natmtot(5) 
           write(5,*)
        endif
!***************************************************************
! Now print xyz coordinates for each snapshot and each species
       do i=1,natms
          if(soltyp(i).eq.1) then
            if(atmnam(i).eq.'M') go to 500
             write(1,'(a2,2x,3e14.6)')atmnam(i),      &
               xxx(i),yyy(i),zzz(i)
          endif
          if(soltyp(i).eq.2) then
             write(2,'(a2,2x,3e14.6)')atmnam(i),      &
               xxx(i),yyy(i),zzz(i) 
          endif
          if(soltyp(i).eq.3) then
             write(3,'(a2,2x,3e14.6)')atmnam(i),      &
               xxx(i),yyy(i),zzz(i) 
          endif
          if(soltyp(i).eq.4) then
             write(4,'(a2,2x,3e14.6)')atmnam(i),      &
               xxx(i),yyy(i),zzz(i) 
          endif
          if(soltyp(i).eq.5) then
             write(5,'(a2,2x,3e14.6)')atmnam(i),      &
               xxx(i),yyy(i),zzz(i) 
          endif
500       continue
       enddo

!***************************************************************
! Now close all files for each snapshot

       close(1)
       if(solno.eq.1) then
         close(2)
       endif
       if(solno.eq.2) then
         close(2)
         close(3)
       endif
       if(solno.eq.3) then
         close(2)
         close(3)
         close(4)
       endif
       if(solno.eq.4) then
         close(2)
         close(3)
         close(4)
         close(5)
       endif


! end do loop over records
      enddo

501    write(6,*)'ERROR in reading file, check formatting'
       stop

505    write(6,*)'End of File found'
       write(6,*)'Number of records', (iconf-1)

1000   close (7)

      end PROGRAM waterwrap
