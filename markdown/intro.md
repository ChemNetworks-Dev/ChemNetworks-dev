 Introduction to ChemNetworks V3.0 {#intro} 
============================

ChemNetworks is used to create networks of interactions from simulation or experimental data. It has been employed to analyze a large range of chemical systems that include complex solutions, liquid interfaces, self-assemblies, and pure liquids undergoing phase changes. This software converts .xyz coordinates to a graph/network based upon user-defined rules for intra- or intermolecular interactions "edges" between atom or molecular "vertices". The ChemNetworks package includes the base code that creates the networks, and within that code are analysis algorithms that include geodesics and PageRank. In addition, we provide a suite of post-processing analysis scripts that help support user capabilities and provide a platform for additions and future development.

<b> ** Citation ** </b>
Any results obtained with ChemNetworks should refer to the following publication:
["Ozkanlar, A.; Clark, A. E. "ChemNetworks: A Complex Network Analysis Tool for Chemical Systems" J. Comp. Chem. 2014, 35, 495-505."](https://onlinelibrary.wiley.com/doi/abs/10.1002/jcc.23506 )


 <b> ** Complilation **</b> 

1. Download ChemNetworks V3.0 repo 

2. in the src directory, make -f MakeFile 

3. This will generate an executable named ChemNetworks.exe


<b> ** Running ChemNetworks ** </b>

Running ChemNetworks mandates that you have:
1) An input file that provides the definition of edges and the chemical system.
2) xyz files of each of the individual species that are used as "nodes" in the network. For example, if you have run a simulation of binary water and methanol, and are interested in creating a network of water...methanol hydrogen bonds, then you would have the xyz files of water separate from those of methanol molecules.

Use the following syntax to run ChemNetworks in interactive mode:

./ChemNetworks.exe {Inputfile.input} {Solvent.xyz files} {Solute.xyz files} 

For example, for a chemical system of a ternary solution phase mixture, use following;

./ChemNetworks.exe Inputfile.input Solvent1.xyz Solvent2.xyz Solvent3.xyz

Submission scripts for embarrasingly parallelized submission of many different xyz files (using Slurm) is found in the "submission-scripts" directory of this distribution.


<b> ** Below is a lists of publications that illustrate different analyses that have been performed using ChemNetworks  ** </b>

1) General overviews of different network analysis algorithms

- Clark, A. E. Intermolecular Network Theory: A General Approach for Understanding the Structural and Dynamic Properties of Liquids and Solutions. In Annual Reports in Computational Chemistry; Dixon, D. A., Ed.; 2015; pp 313–359 
- Servis, M. J.; Martinez-Baez, E.; Clark, A. E. Hierarchical Phenomena in Multicomponent Liquids: Methods, Analysis, Chemistry. Physical Chemistry Chemical Physics. 2020, invited Perspective article,  In Press, https://doi.org/10.1039/D0CP00164C. 

2) Using PageRank as a way to identify geometries of solutes (ions) in complex solutions

- Hudelson, M.; Mooney, B. L.; Clark, A. E.  Determining Polyhedral Arrangements of Atoms Using PageRank, Journal of Mathematical Chemistry, 2012, 50, 2342. DOI: 10.1007/s10910-012-0033-7
- Mooney, B. L.#; Corrales, L. R.; Clark, A. E. Novel Analysis of Cation Solvation Using Graph Theoretic Approaches, Journal of Physical Chemistry B, 2012, 116, 4263. DOI: 10.1021/jp300193j 
- Kelley, M. P.; Yang, P.; Clark, S. B.; Clark, A. E. Competitive Interactions Within Cm(III) Solvation in Binary Water/Methanol Solutions, Inorganic Chemistry, 2018, 57, 10050-10058. DOI: 10.1021/acs.inorgchem.8b01214 

3) Analyzing changes to solvation structure in complex solutions

- Semrouni, D.; Wang, H.-W.; Clark, S. B.; Pearce, C.; Page, K.; Wesolowski, D. J.; Stack, A. G.; Clark, A. E. Resolving Local Configurational Contributions to X-ray and Neutron Scattering Radial Distribution Functions Within Solutions of Concentrated Electrolytes – A Case Study of Concentrated NaOH, Phys. Chem. Chem. Phys., 2019,  21, 6828 - 6838
DOI: 10.1039/C8CP06802J

- Rock, W.; Qiao, B.; Zhou, T.; Clark, A. E.; Uysal, A. Heavy Anion Complex Creates a Unique Water Structure at a Soft Charged Interface, J. Phys. Chem. C, 2018, 122, 29228-29236 DOI: 10.1021/acs.jpcc.8b08419

4) Analyses of liquid interfaces

- Zhou, T.; McCue, A.; Ghadar, Y.; Bako, I.; Clark, A. E. Structural and Dynamic Heterogeneity of Capillary Wave Fronts at Aqueous Interfaces, Journal of Physical Chemistry B, 2017, 121, 9052-9062.DOI: 10.1021/acs.jpcb.7b07406
- Ghadar Y.; Clark A.E. Intermolecular Network Analysis of the Liquid and Vapor Interfaces of Pentane and Water: Microsolvation Does Not Trend with Interfacial Properties,  Physical Chemistry Chemical Physics, 2014, 16, 12475 – 12487.
- Servis, M. ; Clark, A. E. Interfacial heterogeneity is essential to water extraction into organic solvents, Phys. Chem. Chem. Phys., 2019, 21, 2866 – 2874. DOI: 10.1039/C8CP06450D. Front Cover art, listed as 2018 Hot Article by PCCP


5) Analysing compositions and temporal changes to aggregation

- Graham, T. R.; Pope, D. J.; Ghadar, Y.; Clark, S. B.; Clark, A. E.; Saunders, S. R. Alcohol Clustering Mechanisms in Supercritical Carbon Dioxide using Diffusion NMR and Network Analysis – Feedback on Step-Wise Self-Association Models, Journal Physical Chemistry B, 2019, 123, 5316-5323. DOI:  10.1021/acs.jpcb.9b02305

6) Using PageRank as a collective variable along solution-phase reaction coordinates

- Zhou, T., Martinez-Baez, E., Schenter, G., & Clark, A. E. (2019). PageRank as a collective variable to study complex chemical transformations and their energy landscapes. The Journal of chemical physics, 150(13), 134102.

7) Removing artifacts of edge definitions in time correlation functions

- Ozkanlar, A.; Zhou, T.; Clark, A. E. Towards a Unified Descriptions of Hydrogen Bond Networks of Liquid Water: A Dynamics Based Approach Journal of Chemical Physics, 2014, 141, 214107.


8) Solvent response to confinement (including the first definition of geodesic index as a way to measure interconnectivity in networks)

- Wang, C. -H. W.; Bai, P.;  Siepmann, J. I.; Clark, A. E. Deconstructing hydrogen bond networks of solvents confined in nanoporous materials: Implications for alcohol-water separation Journal of Physical Chemistry C, 2014, 118, 19723-19832.

9) analyzing global behavior of complex solutions (in this case a non-Newtonian fluid)

- Edens, L. E.; Pednekar, S.; Morris, J. F.; Schenter, G. K.; Clark, A. E.; Chun, J. Network Theory Analyses Reveal Hierarchical Structure Relationships to the Viscosity of Athermal Suspensions from Contact Force Networks, Phys. Rev. E, 2019, 99, 012607. DOI:10.1103/PhysRevE.99.012607






