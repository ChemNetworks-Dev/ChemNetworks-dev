Optional Input file for PageRank Calculation{#input_pagerank}
====================================

PageRank can be calculated for several different purposes. In general, PR (like other eigenvalues of the adjacency matrix and its relatives) is a unique identifier of the connectivity patterns about a node in the network. The dampening factor, d, controls the sensitivity of the PR of the node to the connectivities of other nodes at different distances from the node of interest. 

<b>1) POLYHEDRAL SEARCH USING PAGERANK</b> Because the adjacency matrices of regular polygons (up to 10) have unique adjacency matrices, the PR of a node with less than 10 edges can be directly studied in terms of the 3-dimensional polyhedral arrangement about the node. To study the polyhedral geometries of the connectivity patterns about nodes with less than 10 edges, the input file structure described in the example input files can be employed using unweighted graphs. PageRank Analysis can be performed (optional) on the weighted graph to describe the importance of each node within the graph. The PageRank Analysis has been utilized in Polyhedral Structure section for unweighted graph. For detailed PageRank calculations, refer to [M. Hudelson, B.L. Mooney, and A.E. Clark, J. Math. Chem. 50, 2342 (2012)] and [B.L. Mooney, L.R. Corrales, and A.E. Clark, J. Phys. Chem. B 116, 4263 (2012)]. 

Input and output files will be discussed for the setup and running of a search for polyhedral structure of hydrogen atoms around several chloride ions in solution. In this example, only the solute-solvent network is constructed, and polyhedral structure is determined through a graph-theoretic approach. To run this analysis, good input parameters are imperative, so the first run will be done to determine appropriate input parameters for the second run.  

This algorithm for finding polyhedral arrangements works by considering the distances between specific atoms in the solvation shell (in this case, the hydrogen atoms within a specific distance of a chloride ion). It creates graphs with the selected atoms as vertices, and subsequently assigns edges between those vertices in hopes of matching a polyhedral graph. The algorithm will automatically create edges if the distance between two atoms is beneath a specified threshold. It then considers all other edges beneath a second threshold to be “possible” edges, and iteratively adds all combinations of these edges. When a (PageRank™) match is found between the created graph and a known polyhedral (or pseudo-polyhedral) graph, the graph is selected and kept until another graph with higher connectivity (more edges) or improved uniformity in edge length (lower standard deviation) is found. Empirically, increasing the connectivity of these graphs tends to universally worsen uniformity in edge length, so higher connectivity is prioritized over improved uniformity. 

Since the algorithm’s run time is O(2n) for n the number of “possible” edges, it is important to determine approximate parameters before running the polyhedral search. One way of doing this before running polyhedral search is using the distribution of paired distances between polyhedra-eligible solvation shell atoms.




<b> Input File (watercl1.input) </b>
The blue numbers on the left of this sample input file are line numbers for reference in this manual, and are not used in practice.

<b> [NUMBER OF SOLVENT TYPES] 1

[NUMBER OF SOLUTE TYPES] 1

[NUMBER OF ATOMS IN SOLVENT1] 3

 O 1
 
 H 2
 
 H 3
 
[NUMBER OF ATOMS IN SOLUTE1] 1

 Cl 1
 
[PERIODIC BOUNDARY CONDITIONS] 1

[BOX XSIDE] 39.45 

[BOX YSIDE] 39.45

[BOX ZSIDE] 39.45

[GRAPH SOLVENT1 SOLUTE1] 1

[SOLVENT1 SOLUTE1 CUTOFF] 2

 2 1 2.85
 
 3 1 2.85   
 
[PAIRED SHELL DISTANCES] 1

[MAX SHELL SIZE] 15

[POLYHEDRA] 0 </b>

Line 1:  Required keyword for the number of major species of interest. Integer value is 1, here, as we have one solvent type. 

Line 2: One solute species is present. Therefore, value is set to 1.

Line 3: Number of atoms in a water molecule (“solvent 1”) is 3.

Lines 4 to 6: Atom labels used in the .xyz file for the water, and their position/order numbers next to labels. Here, the water molecule is described as OHH; oxygen is the first, with label “O” and then the hydrogens with the label “H” for both.

Line 7: Number of atoms in a chloride molecule (“solute 1”) is 1.

Line 8: Atom label used in the .xyz file for the chloride, and its position/order number next.

Line 9: Periodic boundary conditions are requested to be taken into account across the boundaries of the rectangular box of which has dimensions must be provided.

Lines 10 to 12: X-, Y-, and Z-dimensions of the box, respectively. In this example, we have a cubic box with side length 39.45 Angstroms. 

Line 13: Request creation of graph between Water (Solvent 1) and Chloride (Solute 1). This line is necessary for any polyhedra-related task. 

Line 14: The number of distance criteria to be considered for an edge to be formed between water and chloride (Solvent 1 and Solute 1, respectively). 

Lines 15 to 16: The distance criteria for edge formation between water’s 2nd atom (H) and chloride is 2.85 Angstroms. The distance criteria for edge formation between water’s 3rd atom (H) and chloride is also 2.85 Angstroms. 

Line 17: Indicates you a file output giving distribution of paired distances between polyhedra-eligible solvation shell atoms. 

Line 18: Indicate largest solvation shell size you wish to consider. 

Lines 19 to 37: All other keywords (for POLYHEDRA, GRAPHs, etc) are set to 0.  



<b> Output File (./ChemNetworks.exe watercl1.input water.xyz chloride.xyz) </b>
The only new output file is from the Paired Shell Distances keyword. 
[Paired Shell Distances] 1
watercl.input.pdfs.csv


It is now possible to use this information to approximate some bounds on edge lengths in the polyhedra search. This is one of many ways to set polyhedral edge length bounds. For each shell size, it is important to ensure that edges corresponding to distances within the first peak are forced to exist; make sure that the “lower edge uncertainty threshold” is larger than the majority of this peak. Next, avoid too much of the second peak being considered. Set the “upper edge uncertainty threshold” somewhere before the local maximum, probably at or before the point of inflection. Adequate edge uncertainty thresholds for the above data are used in the following input file. 

<b> Second Input File (watercl2.input) </b>

<b> [NUMBER OF SOLVENT TYPES] 1

[NUMBER OF SOLUTE TYPES] 1

[NUMBER OF ATOMS IN SOLVENT1] 3

O 1

H 2

H 3

[NUMBER OF ATOMS IN SOLUTE1] 1

Cl 1

[PERIODIC BOUNDARY CONDITIONS] 1

[BOX XSIDE] 39.45

[BOX YSIDE] 39.45

[BOX ZSIDE] 39.45

[GRAPH SOLVENT1 SOLUTE1] 1

[SOLVENT1 SOLUTE1 CUTOFF] 2 

2 1 2.85

3 1 2.85 

[MAX SHELL SIZE] 15

[PAIRED SHELL DISTANCES] 0

[POLYHEDRA] 1

[POLYHEDRA EDGE CERTAINTY BOUNDS] 8

4 3.85 4.65

5 3.2 4.6

6 3.4 4.45

7 3.4 4.2

8 3.4 4.1

9 3.2 4.0

10 3.2 3.9

11 3.2 3.8
</b>


The only difference from the previous input file is the addition of lines 20 to 28. These lines describe the “Polyhedra Edge Certainty Bounds” which are required for polyhedra search. The first number, 8, refers to how many lines follow. Each following line is in the format “Shell-size lower-threshold upper-threshold.” For instance, for a solvation shell with 4 polyhedra-eligible atoms, all edges below 3.85 Angstroms are forced to be included in the graph, and all edges between 3.85 and 4.65 Angstroms are considered as “possible” edges, and are iterated through in search of polyhedral matches. Any edges above 4.65 Angstroms are discarded as unfeasible. 

The minimum input is a single line; in that case, the given bounds will be used for all shell sizes. Any additional lines given will be linearly extrapolated between, and it is important to list bounds in order of increasing shell size. The maximum and minimum shell size bounds will be used for shell sizes beyond the range which is specified. (That is, if the lowest shell size bounds you specify is shell size of 5 with bounds 4.2 to 5.2, then for the shell size of 4, the bounds will also be 4.2 to 5.2.)


<b> Output File (./ChemNetworks.exe watercl2.input water.xyz chloride.xyz) </b>

[POLYHEDRA] 1
watercl2.input.Polys 

Note that the name of the .xyz files is not included in the file name. For subsequent snapshots new lines will be appended to this file, allowing for straightforward analysis. Be attentive not to use the same input file for different runs, or to rename the .Polys file before doing so.

Output Format

The output is a CSV formatted file with each line containing the following values (numbered by column):

1.	Solvent XYZ file name (for keeping track of snapshots)
2.	Solute Molecule Index (for files with multiple solute molecules)
3.	Solute Atom Number (only useful in case of polyatomic solute)
4.	Number of polyhedra-eligible atoms in solvation shell
5.	Name of polyhedron
6.	Polyhedron index number (its index in the C code, useful for analysis)
7.	Processor time

For the files used in the example, the output should appear something like this:




<b> Troubleshooting: </b>

If the algorithm takes a long time, it may be because of too small of a lower threshold or too large of an upper threshold, which both increase the amount of time taken exponentially. 



If a very high proportion of your output is “NA”, it may be caused by too large of a lower threshold (which can force existence of non-planar graphs) or too small of an upper threshold (which can fail to consider important edges). Additionally, consider viewing unmatched snapshots in a program like VMD for clues; it is possible an arrangement formed which is not included as a possibility in the program.

What is a good proportion of matched polyhedra? With the current set of polyhedra, aim for about 70-80% match. Too high of match indicates too high of an upper threshold and will tend towards including too many edges (the algorithm will always prefer a maximal planar graph). 

Adding new polyhedra or removing polyhedra involves slight modification of the C code (polyhedra.c). To add a single polyhedron:

1.	Increase the value of the variable numofpolies  at the beginning of the function polyhedra_st by 1. 

2.	Navigate to the function makeallpolys. There write two new lines before memory is freed, one creating an edge list and one calling the poly_create function, as follows, replacing all blue terms with your own:

int newpolyel[2 * num_of_edges] = {ordered_pairs_of_vertices};

polylist[next_index] = poly_create(“name_of_polyhedron”, newpolyel, num_of_vertices, num_of_edges);

Numerous examples already exist in the code itself. 

3.	Recompile and run. If errors occur, ensure that the number of edges and vertices are correct and consistent with the edge list provided. 



<b> UTILITIES FOR POST-PROCESSING </b>

Some utility programs will be introduced in this section. A script, written in R language, and a C code will be used to perform degree analysis, network neighborhood analysis, and additional geodesic path analysis. 

<b> R-SCRIPT FOR DEGREE AND NETWORK NEIGHBORHOOD </b> 
Once the edge list for the requested graphs/networks are generated using the ChemNetworks, the “degree” distributions and histograms can be obtained using our post-processing script written in R language, named chemical-networks.R, which reads the ChemNetworks output files with the extension .Graph. This script also enables to perform the “network neighborhood” analysis. The portions of the script (R language commands) for degree and network neighborhood analyses are shown below:

no.net.degs<-degree(grafik)
write(no.net.degs,file="all.degrees",ncolumns=(numbervertices),append=TRUE)
.
.
.
nghbr4<-neighborhood.size(grafik,4,1:(numbervertices))
write(nghbr4,file="neighborhoodsize.order.4",ncolumns=(numbervertices+1))

Here, grafik represents the graph object, which is essentially the edge-list for the network of interest. The degree (number of edges a vertex is making) of all vertices will be printed in an output file, named all.degrees. The fourth order neighborhood size (the number of all the vertices within 4 order neighborhood) of all vertices will be printed in an output file, named neighborhoodsize.order.4. 

The script chemical-networks.R is provided along with the ChemNetworks package. To run this script, the igraph R library should also be installed from the CRAN website: http://cran.r-project.org. 



<b>2) PR OF WEIGHTED GRAPHS </b> PR can also be performed using weighted graphs after utilizing an optional section in ChemNetworks for creating the weighted graph (where the edge is weighted based upon the interaction distance). In this instance, we have shown that the Fourier Transform of the PR autocorrelation function is the weighted average of the vibrational modes within a molecules, and thus it can be a straightforward fingerprint of molecular fluctuations.  Furthermore, PageRank Forces can be calculated by performing a bias on the PageRank value for targeted nodes. These PageRank Forces can be used in combination with a harmonic bias to drive the graph towards a specific pattern considering the whole graph connectivity within a biased MD simulation. [see Zhou, T., Martinez-Baez, E., Schenter, G., & Clark, A. E. (2019). J. Chem. Phys. 150(13), 134102.].
 
The Weighted Graph Analysis is performed as an additional section in ChemNetworks along with the existing unweighted graph analysis. Generally, the construction of Weighted Graph is a two-step process: 1) select the nodes, 2) determine the edge weights between the selected nodes. For the first step, there are two ways to select the nodes, one is by distance to solute (i.e. select a local cluster to solute) and the other is by solvent/solute type (i.e. select all solvent/solute molecules from input xyz-files). For the second step, the edge weights are parameterized on interaction distances using a smooth function (see release note Mar-2018).


<b> Input File for PageRank Calculation </b>

<b>[WEIGHTED GRAPH]</b> 1 (Yes, request weighted graph calculation); 0 (No)

<b>[WEIGHTED GRAPH BY SOLUTE1 CLUSTER]</b> N (number of ways to select solvent molecules within the solute1 cluster). If N > 0, this line should be followed directly by N lines of parameter list for each way of selecting cluster. As an example, “1 1 1 3 0.0 3.2” means selecting all the solvent 1 (the 1st solvent) that are 0.0 ~3.2 Å to solute 1 (the 1st solute), the distance is calculated from atom 1 in solute 1 to atom 3 in solvent 1.

<b>[WEIGHTED GRAPH BY SOLUTE2 CLUSTER]</b> N (number of ways to select the solvent molecules within solute2 cluster). This is the same for solute2 as it is for solute1.

<b>[WEIGHTED GRAPH BY SOLUTE TYPE]</b> N (number of solute types to be included in the nodes of weighted graph). If N > 0, this line should be followed directly by N lines of solute-type id (only 1 for solute1 and/or 2 for solute2 is accepted).

<b>[WEIGHTED GRAPH BY SOLVENT TYPE]</b> N (number of solvent types to be included in the nodes of weighted graph). If N > 0, this line should be followed directly by N lines of solvent-type id (only 1 for solvent1 and/or 2 for solvent2 is accepted).

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> 1 (Yes, request weighted-edges between solute1 and solvent1) or 0 (No).

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solvent1, this is to allow multiple interactions). Must be specified if keyword [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] is set to 1. If N > 0, this line should be followed directly by N lines of atom-index pairs. As an example, a pair “2 3” means the edge-weight is parameterized on the distance between atom 2 in solute1 and atom 3 in solvent1. 

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). Must be specified if keyword [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] is set to 1. This function-type = 1 is corresponding to a fermi function. This keyword should be followed directly by 1 line with the associated function parameters n0 and r0. As an example, “10.0 2.8” directly following this keyword means n0 = 10.0 and r0 = 2.8.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute2 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute2 and solvent2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solute2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solvent1 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solvent2 and solvent2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solute2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent1-solvent2 edges.

<b>[PAGERANK ON WEIGHTED GRAPH]</b> 1 (Yes, request the PageRank calculation on weighted graph) or 0 (No). If it is set to 1, there must have defined the weighted graph section. 

<b>[PAGERANK DAMPING FACTOR]</b> d (the numerical damping factor in PageRank calculation, 0<d<1, e.g. it can be 0.85). Must be specified if [PAGERANK ON WEIGHTED GRAPH] is set to 1. This damping factor describes how important the adjacency matrix is considered in PageRank calculation, the larger it is, the higher importance the adjacency matrix is.

<b>[PAGERANK FORCE CALCULATION]</b> 1 (Yes, request the PageRank force calculation) or 0 (No). Must be specified if [PAGERANK ON WEIGHTED GRAPH] is set to 1.

<b>[PAGERANK FORCE CALCULATION BIAS]</b> N (N ≧ 0, number of harmonic biases put on the nodes of the weighted graph based on the associated PageRank values). Must be specified if [PAGERANK FORCE CALCULATION] is set to 1.  If N > 0, this keyword should be followed directly by N lines specifying all the harmonic biases. Each line corresponds to one harmonic bias , which is based on the PageRank value on node k. As an example, “1 0.1976 1000.0” means a bias on node k = 1, the targeted PR(1)0 = 0.1976, with U0 = 1000.0 (in energy-unit, e.g. kcal/mol). Then the resulting forces on every node will be calculated in unit of “energy-unit per Å”, e.g. kcal/mol/Å. 
