Optional Search for Geometric Cycles in a Water Network {#cyclicity}
====================================


Direct structural search of specific water oligomers is invoked using the [WATER STRUCTURES] keyword as introduced in previous section. There has been historically a strong interest in the presence of polygons and cyclic oligomers of the form (H2O)3-5, and water hexamers, (H2O)6 (the bag, boat, book, cage, chair, prism, prismbook, and ring configurations). ChemNetworks incorporates geometric criteria for vertex angles and dihedrals for these structures, allowing for thermally induced angular deformations. The details are given below


<b>[WATER STRUCTURES]</b> 1 N  1: (Yes, search for water oligomers. Then, all keywords below starting with WATER are required), 0 (No); N: Water is the Nth “solvent” defined in the input file.

<b>[WATER HEXAMER RING]</b> 1 (Yes, search for cyclic ring hexamers); 0 (No)

<b>[WATER HEXAMER BOOK]</b> 1 (Yes, search for book hexamers); 0 (No)

<b>[WATER HEXAMER PRISM]</b> 1 (Yes, search for prism hexamers); 0 (No)

<b>[WATER HEXAMER CAGE]</b> 1 (Yes, search for cage hexamers); 0 (No)

<b>[WATER HEXAMER BAG]</b> 1 (Yes, search for bag hexamers); 0 (No)

<b>[WATER HEXAMER BOAT]</b> 1 (Yes, search for boat hexamers); 0 (No)

<b>[WATER HEXAMER CHAIR]</b> 1 (Yes, search for chair hexamers); 0 (No)

<b>[WATER HEXAMER PRISMBOOK]</b> 1 (Yes, search for prismbook hexamers); 0 (No)

<b>[WATER PENTAMER SEARCH]</b> 1 (Yes, search for cyclic pentamers); 0 (No)

<b>[WATER TETRAMER SEARCH]</b> 1 (Yes, search for cyclic tetramers); 0 (No)

<b>[WATER TRIMER SEARCH]</b> 1 (Yes, search for cyclic trimers); 0 (No)

<b>[WATER ISOLATED STRUCTURES]</b> 1 (Yes, search for all isolated oligomers requested above);        0 (No)



