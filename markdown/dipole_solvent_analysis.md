Dipole Solvent Analaysis {#dipole_solvent_analysis} 
===================================================
<!-- The #dipole_solvent_analysis if the end point from intro.md @ref -->
This is how you do solvent stuff


```cpp
//solvent analysis
int dipole=something();
cout<<dipole<<endl;
```
<!-- now trying a function call to return link to documentation for input_read -->

- \ref CN_NS::ChemNetworkNew::input_read() "input_read()"
- \ref CN_NS::ChemNetworkNew::read_xyz_all() "read_xyz_all()"

