LAMMPS and ChemNetworks {#LAMMPS_chemNetwork_compilation}
====================================

To run ChemNetworks analysis during a LAMMPS simulation, it is advised to put the ChemNetwork command after any of NVE/NVT/NPT commands. In this implementation we have taken advantage of fix templates within LAMMPS. The following is an example input file for LAMMPS. Please refer to LAMMPS documentation for LAMMPS commands. ## 


**Patching ChemNetworks**

In order to compile the code, after downloading it from gitlab repository please follow the instructions below:
 
cd src  
vi Makefile ( make sure the compiler is compatible with your system)  
make  
make lib

grab the path for the compiled library and shared library, modify the make file in lammps as follow:  
cd /lammps/src  
mkdir CHEMNET  
cp /chemnetwork/lammps/fix_chemnetwork.cpp CHEMNET/  
cp /chemnetwork/lammps/fix_chemnetwork.h CHEMNET  
make yes-CHEMNET  

vi /lammps/src/MAKE/Makefile.mpi  
add the following commands to your makefile  
CCFLAGS +=      -I/path/to/chemnetwork/src  
LIB =-L/path/to/chemnetwork/folder/src -lChemNetworks-2.2  
 
Save and close the file and compile lammps as usuall, for example make mpi -j 4  
Your executable should run with chemnetwork input file.

**Running ChemNetowrks (LAMMPS Input)**

fix ID group-ID style_name keyword iteration value.
ID = user-assigned name for the fix
group-ID = ID of the group of atoms to apply the fix to
style = chemnetwork
keyword= ChemNetwork input file name
iteration= number of steps to be collected for each output file
value= atom type name using ChemNetwork input file convention
 
for example:
fix     CHEM all chemnetwork cn.inp 10 O H

Example input/output files for running the ChemNetworks fix with LAMMPS can be found in the tests directory, and also in the source directory that has the LAMMPS patch.



--------------------------------------------------------------------------------------------------
