Pre-Processing for ChemNetworks {#pre-processing}
====================================

As input, ChemNetworks requires .xyz files of the species that comprise each node in the network that will be created. In the case of a molecular dynamics trajectory or Monte Carlo ensemble of frames of data, this would be the .xyz file of each frame of data (snapshot) separated into the different nodes of the network. 

Example: The first snapshot of a simulation of water and methanol where the hydrogen bond network of water-water, water-methanol, and methanol-methanol is desired would consist of a water1.xyz file and a methanol1.xyz  where water1.xyz contained all xyz coordinates of all water molecules, and methanol1.xyz contained all xyz coordinates of all methanol molecules

Further, the xyz files must be wrapped across the periodic boundaries of the simulation. Note that ChemNetworks will identify edges that cross PBC in cubic or rectangular boxes only. If box angles are not equal to 90 degrees, we suggest that you run ChemNetworks in a mode that ignores the PBC. 

Wrapping scripts have been provided in the ChemNetworks Pre-Processing directory, however we note that:

- Wrapping can be done in VMD using "pbc warp [options] -centersel sel -all "
- In GROMACS, wrapping can be done using command " -pbc mol -ur compact" .








