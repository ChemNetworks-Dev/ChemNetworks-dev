Example 1 : Bulk Water {#example1}
====================================

<b> BULK WATER </b>
A single snapshot from the molecular dynamics simulation of bulk water at room temperature will be used for the purpose of illustrating the ChemNetworks input file preparation. A detailed explanation of the input and output files will be provided. 
Input File (Water.input)


<b> [NUMBER OF SOLVENT TYPES] </b> 1

<b> [NUMBER OF SOLUTE TYPES] </b> 0

<b> [NUMBER OF ATOMS IN SOLVENT1]</b> 3

<b> O</b> 1

<b> H</b> 2

<b> H </b>3

<b> [PERIODIC BOUNDARY CONDITIONS]</b> 1

<b> [BOX XSIDE]</b> 18.643

<b> [BOX YSIDE]</b> 18.643

<b> BOX ZSIDE]</b> 18.643

<b> [GRAPH SOLVENT1 SOLVENT1]</b> 1

<b> [SOLVENT1 SOLVENT1 HBOND DISTANCE]</b> 4

<b> 1 2 </b>0.00 2.50

<b> 1 3 </b>0.00 2.50

<b> 2 1 </b>0.00 2.50

<b> 3 1 </b>0.00 2.50

<b> [SOLVENT1 SOLVENT1 HBOND ANGLE] </b>4

<b> 1 2</b>

<b> 1 2 1 </b>150.0 180.0

<b> 1 2</b>

<b> 1 3 1 </b>150.0 180.0

<b> 2 1</b>

<b> 1 2 1 </b>150.0 180.0   

<b> 2 1 </b>

<b> 1 3 1 </b>150.0 180.0

<b> [SOLVENT1 SOLVENT1 ENERGY]</b> 4

<b> 1 1 </b>−20.0 0.0

<b> 1 1</b> −20.0 0.0

<b> 1 1</b> −20.0 0.0

<b> 1 1 </b>−20.0 0.0

<b> [CHARGES OF ATOMS IN SOLVENT1]</b> 3

<b> 1 </b>−0.8476

<b> 2</b> 0.4238

<b> 3 </b>0.4238

<b> [LJ PARAMETERS OF ATOMS IN SOLVENT1]</b> 9

<b> 1 1</b> 3.166 0.15535

<b> 1 2 </b>1.000 0.00

<b> 1 3 </b>1.000 0.00

<b> 2 1 </b>1.000 0.00

<b> 2 2 </b>0.000 0.00

<b> 2 3 </b>0.000 0.00

<b> 3 1 </b>1.000 0.00

<b> 3 2 </b>0.000 0.00

<b> 3 3 </b>0.000 0.00

<b> [GRAPH SOLVENT2 SOLVENT2] </b>0

<b> [GRAPH SOLVENT3 SOLVENT3] </b>0

<b> [GRAPH SOLVENT1 SOLVENT2] </b>0

<b> [GRAPH SOLVENT1 SOLVENT3] </b>0 

<b> [GRAPH SOLVENT1 SOLUTE1] </b>0

<b> [GRAPH SOLVENT1 SOLUTE2] </b>0

<b> [GRAPH SOLVENT2 SOLVENT3] </b></b>0

<b> [GRAPH SOLVENT2 SOLUTE1] </b>0

<b> [GRAPH SOLVENT2 SOLUTE2] </b>0

<b> [GRAPH SOLVENT3 SOLUTE1]</b> 0

<b> [GRAPH SOLVENT3 SOLUTE2] </b>0

<b> [GRAPH SOLVENT1 SOLVENT2 SOLVENT3]</b> 0

<b> [PRINT NUMBER OF NODES] </b>1

<b> [GEODESICS GD] </b>1

<b> [GD SOLVENT1] </b>1

<b> [GD SOLVENT1 EUCLIDEAN DISTANCE]</b> 1

<b> [GD SOLVENT1 EUCLIDEAN REFERENCE]</b> 1 

<b> [GD SOLVENT2] </b>0

<b> [GD SOLVENT2 EUCLIDEAN DISTANCE] </b>0

<b> [GD SOLVENT2 EUCLIDEAN REFERENCE]</b> 0

<b> [GD SOLVENT3]</b> 0

<b> [GD SOLVENT3 EUCLIDEAN DISTANCE] </b>0

<b> [GD SOLVENT3 EUCLIDEAN REFERENCE]</b> 0

<b> [GD SOLVENT1 SOLVENT2] </b>0

<b> [GD SOLVENT1 SOLVENT2 EUCLIDEAN DISTANCE]</b> 0

<b> [GD SOLVENT1 SOLVENT2 EUCLIDEAN REFERENCE]</b> 0 0

<b> [GD SOLVENT1 SOLVENT3]</b> 0

<b> [GD SOLVENT1 SOLVENT3 EUCLIDEAN DISTANCE]</b> 0

<b> [GD SOLVENT1 SOLVENT3 EUCLIDEAN REFERENCE]</b> 0 0

<b> [GD SOLVENT2 SOLVENT3]</b> 0

<b> [GD SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE] </b>0

<b> [GD SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> 0 0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3]</b> 0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b> 0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> 0 0 0

<b> [SOLUTE1 WATER DIPOLE ORIENTATIONS]</b> 0

<b> [SOLUTE2 WATER DIPOLE ORIENTATIONS]</b> 0

<b> [SOLVENT WATER DIPOLE ORIENTATIONS]</b> 0 0 0

<b> [WATER STRUCTURES]</b> 1 1

<b> [WATER HEXAMER RING]</b> 0 

<b> [WATER HEXAMER BOOK]</b> 0 

<b> [WATER HEXAMER PRISM]</b> 0 

<b> [WATER HEXAMER CAGE]</b> 0 

<b> [WATER HEXAMER BAG]</b> 0 

<b> [WATER HEXAMER BOAT]</b> 0 

<b> [WATER HEXAMER CHAIR]</b> 1 

<b> [WATER HEXAMER PRISMBOOK]</b> 0

<b> [WATER PENTAMER SEARCH]</b> 0 

<b> [WATER TETRAMER SEARCH]</b> 1 

<b> [WATER TRIMER SEARCH] </b>0 

<b> [WATER ISOLATED STRUCTURES]</b> 0


Line 1: Required keyword for the number of major species of interest. Integer value is 1, here, as we will construct and analyze the H-bond network of bulk water only.

Line 2: Required keyword for the number of minor species of interest. Integer value is 0, here, as bulk water is the only species of interest.

Line 3: Number of atoms in a water molecule is 3.

Lines 4 to 6: Atom labels used in the .xyz file for the water, and their position/order numbers next to labels. Here, the water molecule is described as OHH; oxygen is the first, with label “O” and then the hydrogens with the label “H” for both.  There are absolutely no restrictions on the atom labels used and the way they are ordered to describe the molecule of interest.  

Line 7: Periodic boundary conditions are requested to be taken into account across the boundaries of the rectangular box. The dimensions must be provided in subsequent keywords.

Lines 8 to 10: X-, Y-, and Z-dimensions of the box, respectively. Must be given in angstroms. Here, we have a cubic box of side-length 18.643Å.

Line 11: The water H-bond graph/network is requested to be constructed.

Line 12: The number of H-bond interactions to be used to describe the edges (H-bond) between two water molecules (see Figure 1). Here, there are four possible O...H pairs for the two waters. 

Lines 13 to 16: Distance criteria for the edge formation. The cutoff distance between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom) is set to 2.5 Å (line 13). The cutoff distance between the 1st atom of water A (O-atom) and the 3rd atom of water B (H-atom) is set to 2.5 Å (line 14).  The cutoff distance between the 2nd atom of water A (H-atom) and the 1st atom of water B (O-atom) is set to 2.5 Å (line 15).  The cutoff distance between the 3rd atom of water A (H-atom) and the 1st atom of water B (O-atom) is set to 2.5 Å (line 16). (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)

Line 17: The number of H-bond interactions to be used to describe the edges (H-bond) between two water molecules (see Figure 1). Here, there are four possible O...H pairs for the two waters. The value here must match the one entered in line 12.

Lines 18 to 25: Angle criteria for the edge formation. Angle requires 3 atoms, 2 of which from one water and 1 atom from the other water. The H-atom is always at the middle (OHO) in the angle definitions. When 1 atom is from the 1st molecule and 2 atoms from the 2nd molecule “1 2” combination is used (lines 18 and 20), whereas when 2 atoms are from the 1st molecule and 1 atom from the 2nd molecule “2 1” combination is used (lines 22 and 24). The order of angle criteria must be consistent with the order of distance criteria. Line 18: 1 atom from water A, and 2 atoms from water B. Line 19: The angle cutoff between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom), these two atoms are used in line 13, and the 1st atom of water B (O-atom) is set to 150.0°. Therefore, whenever the distance between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom) is less than 2.5 Å, and the angle between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom), and the 1st atom of water B (O-atom) is larger than 150.0°, an edge will be formed between the water A and water B. As you can see, lines 13 and 19 define geometric criteria (distance and angle) together. Similarly, lines 14 and 21; lines 15 and 23; and lines 16 and 25 match in defining the geometric criteria. All pairs of water molecules satisfying one of these criteria will be considered H-bonded and printed as pairs in the output file ending with .Graph. (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)  

Lines 26 to 44: Energy criteria for the edge formation for bulk water (TIP3P, SPC, SPC/E). The value at Line 26 should be the same number as in Line 12 and Line 17. Then in Line 27 to 30 are the actual energetic criteria in determining the H-bond between a water pair. In each line, the first two integer numbers (not used in code) are needed; the next two floating numbers are the lower boundary Emin & upper boundary Emax for the pairwise interaction. Thus, a water pair is considered to be H-bonded if their pair interaction energy is Emin < E < Emax. Keep in mind that H-bond criteria are imposed on top of each other, in other words, a particular water pair should satisfy all the specified criteria to be considered as H-bonded (Distance & Angle & Energy). However, if you do not want to use one type of criteria, just set its lower boundary to be a very small number and set its upper boundary to be a very large number. Then, Line 31 to 34 defines the atomic charge, and Line 35 to 44 defines the Lennard-Jones (12-6) parameters, that are used in calculating the pairwise interaction energy for water.

Lines 45 to 56: All other keywords (starting with GRAPH) for graph/network construction are set to 0, as bulk water is the only chemical system at hand.

Line 57: The number of vertices/nodes (water molecules in this example) making up the graph/network will be printed in an output file ending with .GraphNumnodes.

Line 58: The geodesic distance (gd) matrix and Euclidean distance for the water H-bond network will be calculated.

Line 59: The gd matrix for the network obtained from the keyword in line 11 will be calculated.

Line 60: Euclidean distance between the terminal water molecules of all geodesic pathways within the water H-bond network will be calculated.

Line 61: Euclidean distance between the 1st atom (O-atom) of terminal water molecules of all geodesic pathways will be calculated. 

Lines 62 to 79: All other keywords (starting with GD) for gd matrix construction for other graph/network types are set to 0, as bulk water is the only chemical system at hand.

Lines 80 to 81: Water dipole moment orientations with respect to a solute are requested using these keywords. In this example, there are no solutes present.

Line 82: Water dipole moment orientations with respect to other solvent molecules are requested using this keyword. In this example, there are no other solvents.

Line 83: The search for water oligomers will be done. Water is the 1st “solvent” defined above.

Lines 84 to 94: The search only for the chair hexamers, and cyclic water tetramers will be done.

Line 95: The isolated oligomers (that are isolated from the rest of the network) won’t be searched.

Running: ./ChemNetworks.exe Water.input water.xyz

<b> Output Files</b>  
Water.input.water.xyz.Water.Cyclic.Tetramers  
Water.input.water.xyz.Water.Hexamer.Chairs  
Water.input.water.xyz.water.xyz.Graph  
Water.input.water.xyz.water.xyz.GraphGeod  
Water.input.water.xyz.water.xyz.GraphNumnodes  
water.xyz.geocard  
water.xyz.geopath  
