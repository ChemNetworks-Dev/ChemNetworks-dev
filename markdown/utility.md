ChemNetworks Capabilities (including post-processing) {#utility}
====================================

ChemNetworks can create unweighted graphs (most common usage) or weighted graphs.

The code is generally run as a post-processing to existing trajectories or snapshots from MD or MC (any xyz data) but has also been implemented as a "fix" in LAMMPS so that it can be run internally as LAMMPS for real-time network creation and analysis during an MD simulation. In this version, using weighted networks and an external driver, ChemNetworks can be used within biased MD simulations - specifically using PageRank as a collective variable with a harmonic potential to push the system from one PR to another. 

The ChemNetworksV3.0 output files can contain the following information (using the appropriate input flags)

- list of all edges within the network (adjacency matrix of connectivity) 
- list of all geodesics within the network
- identification of simple geometric cycles (using geometric criterion, not network criterion)
- Calculation of node PageRank (from unweighted or weighted graphs)

Additionally, within the post-processing folder there are scripts that will read ChemNetworks output files to perform the following tasks:

- Calculate edge distributions
- Calculate and correct edge lifetimes to remove artifacts that are caused by the rigid geometric or energetic criterion used in the definition of an edge
- Calculate the geodesic index
- Identify clusters within the network 
- Identify transitions between specific patterns in the network using 2-state and 3-state models











