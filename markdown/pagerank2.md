Weighted and Unweighted Graphs using PageRank {#pagerank2}
====================================

## Weighted and Unweighted Graphs Using Pagerank ## 


## ALUMINATE IN WATER ##

Here is an example of using the Weighted Graph & PageRank Analysis (see release Mar-2018) part of the ChemNetworks C++ version. The goal of this example is to introduce the Keywords associated with Weighted Graph & PageRank Analysis, and to introduce the information in the output files.

The example shown here is for a single aluminate ion in bulk water: 1 [Al(OH)4]1- in 90 H2O with a cubic box of 14.3 Å. The hydronium ion [OH]1- is named by [OkHk]1- in order to differentiate it with the water oxygen and hydrogen. The system contains [Al]3+, 4 [OkHk]1- , and 90 H2O. The [Al]3+ is treated as a solute, while [OkHk]1- and H2O are treated as two solvents. The weighted graph is constructed by considering a local cluster within 3.5 Å to the [Al]3+ solute. The edge-weights for solute-solvent, solvent-solvent interactions are all considered, such as [Al]3+ … [OkHk]1- , [Al]3+ … H2O ,  [OkHk]1- … [OkHk]1- , H2O … H2O, [OkHk]1- … H2O interaction. 

There are three input-xyz-files corresponding to the atom coordinates of [Al]3+, 4 [OkHk]1- , and 90 H2O. There is a standard input file for ChemNetworks that contains all the Keywords, which is listed as following:

1	[NUMBER OF SOLVENT TYPES] 2  
2	[NUMBER OF SOLUTE TYPES] 1  
3	[NUMBER OF ATOMS IN SOLVENT1] 3  
4	O 1  
5	H 2  
6	H 3  
7	[NUMBER OF ATOMS IN SOLVENT2] 2  
8	Ok 1  
9	Hk 2  
10	[NUMBER OF ATOMS IN SOLUTE1] 1  
11	Al 1  
12	[PERIODIC BOUNDARY CONDITIONS] 1  
13	[BOX XSIDE] 14.3  
14	[BOX YSIDE] 14.3  
15	[BOX ZSIDE] 14.3  
16	[WEIGHTED GRAPH] 1  
17	[WEIGHTED GRAPH BY SOLUTE1 CLUSTER] 2  
18	1  1  1  1  0.0  3.5  
19	1  2  1  1  0.0  3.5  
20	[WEIGHTED GRAPH BY SOLUTE TYPE] 0  
21	[WEIGHTED GRAPH BY SOLVENT TYPE] 0  
22	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] 1  
23	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE] 1  
24	1 1  
25	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE FUNCTION] 1  
26	10.0  1.9  
27	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2] 1  
28	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE] 1  
29	1 1  
30	[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE FUNCTION] 1  
31	10.0  1.9  
32	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1] 0  
33	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE] 0  
34	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE FUNCTION] 0  
35	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2] 0  
36	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE] 0  
37	[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE FUNCTION] 0  
38	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1] 1  
39	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE] 1  
40	1 1  
41	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE FUNCTION] 1  
42	10.0 2.8  
43	[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2] 1  
44	[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE] 1  
45	1 1  
46	[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE FUNCTION] 1  
47	10.0  2.8  
48	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2] 1  
49	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE] 1  
50	1 1  
51	[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE FUNCTION] 1  
52	10.0  2.8  
53	[PAGERANK ON WEIGHTED GRAPH] 1  
54	[PAGERANK DAMPING FACTOR] 0.9  
55	[PAGERANK FORCE CALCULATION] 1  
56	[PAGERANK FORCE CALCULATION BIAS] 1  
57	1  0.165  10.0  
58	[GRAPH SOLVENT1 SOLVENT1] 0  
59	[SOLVENT1 SOLVENT1 HBOND DISTANCE] 0  
60	[SOLVENT1 SOLVENT1 HBOND ANGLE] 0  
61	[SOLVENT1 SOLVENT1 HBOND ENERGY] 0  
62	[CHARGES OF ATOMS IN SOLVENT1] 0  
63	[LJ PARAMETERS OF ATOMS IN SOLVENT1] 0  
64	[GRAPH SOLVENT2 SOLVENT2] 0  
65	[GRAPH SOLVENT3 SOLVENT3] 0  
66	[GRAPH SOLVENT1 SOLVENT2] 0  
67	[GRAPH SOLVENT1 SOLVENT3] 0  
68	[GRAPH SOLVENT2 SOLVENT3] 0  
69	[GRAPH SOLVENT1 SOLUTE1] 0  
70	[GRAPH SOLVENT1 SOLUTE2] 0  
71	[GRAPH SOLVENT2 SOLUTE1] 0  
72	[GRAPH SOLVENT2 SOLUTE2] 0  
73	[GRAPH SOLVENT3 SOLUTE1] 0  
74	[GRAPH SOLVENT3 SOLUTE2] 0  
75	[GRAPH SOLVENT1 SOLVENT2 SOLVENT3] 0  
76	[PRINT NUMBER OF NODES] 0  
77	[GEODESICS GD] 0  
78	 [SOLUTE1 WATER DIPOLE ORIENTATIONS] 0  
79	[SOLUTE2 WATER DIPOLE ORIENTATIONS] 0  
80	[SOLVENT WATER DIPOLE ORIENTATIONS] 0 0 0  
81	[WATER STRUCTURES] 0 1  
82	 [POLYHEDRA] 0  
83	[MAX SHELL SIZE] 0  
84	[PYHEDRA EDGE CERTAINTY BONDS] 0  
85	[PAIRED SHELL DISTANCES] 0  

Line 1:  Required keyword for the number of major species of interest. Integer value is 2, here, as we have two solvent type H2O and [OkHk]1- . 

Line 2: One solute species is present [Al]3+ . Therefore, value is set to 1.

Line 3: Number of atoms in a water molecule (“solvent 1”) is 3.

Lines 4 to 6: Atom labels used in the .xyz file for the water, and their position/order numbers next to labels. Here, the water molecule is described as OHH; oxygen is the first, with label “O” and then the hydrogens with the label “H” for both.

Line 7 to 9: Specify the number of atoms in [OkHk]1- (“solvent2”) and the corresponding order in the .xyz file.

Line 10 to 11: Number of atoms in [Al]3+ (“solute1”) and its order.

Line 12 to 15: Periodic boundary conditions are requested to be taken into account across the boundaries of the rectangular box. The X-, Y-, and Z-dimensions of the box have been specified, respectively. In this example, we have a cubic box with side length 14.3 Angstroms. 

Lines 16: Request the Weighted Graph Calculation.

Lines 17 to 19: Selecting the water molecules (“solvent1”) and hydronium (“solvent2”) within 3.5 Å to the [Al]3+ solute. For water molecules, the distance is calculated from oxygen (atom index = 1) to Al, and for hydronium ion, the distance is calculated from Ok (atom index = 1) to Al. The distance range for selecting the molecules is 0.0 ~ 3.5 Å. For example, “1 1 1 1 0.0 3.5” in line 18 means selecting a water molecule (solvent 1) if its oxygen (atom index 1) is within 3.5 Å to Al (solute 1, atom index 1).

Lines 20 to 21: Not select nodes by solvent/solute type. If you want to select all the water molecules, no matter how far it is from the solute, then you can use these keywords (see details in section 6.0).

Lines 22 to 26: Define the edge weights between [Al]3+ (solute1) and water (solvent1). Line 22, the keyword is set to 1, which means that the edge between [Al]3+ and water is included in the weighted graph. There is only one site-site interaction is considered (1 in line 23), which is from the Al atom in [Al]3+ (atom index = 1) to the oxygen atom in water (atom index = 1), i.e. the first integer 1 in line 24 corresponds to Al in [Al]3+ while the second integer 1 corresponds to O in water. Furthermore, the edge-weight is calculated using a fermi-function (line 25) with two parameters set as 10.0 and 1.9 (line 26).

Lines 27 to 31: Define the edge weights between [Al]3+ (solute1) and [OkHk]1- (solvent2)

Lines 32 to 37: There is no solute2, therefore, no edges with solute2.

Lines 38 to 52: Define the edge weights between water (solvent1) and water (solvent1), and those between [OkHk]1- (solvent2) and [OkHk]1- (solvent2), and those between water (solvent1) and [OkHk]1- (solvent2).

Lines 53: This keyword is set to 1, which requests for PageRank calculation based on the Weighted Graph. Note: usually, one should first run a PageRank analysis (set line 53 and 54) without applying, to determine the node-id of your interest, which should be a fixed id and will be put in line 57.

Lines 54: The damping factor in PageRank calculation is set to be 0.9.

Lines 55 to 57: Request the PageRank Force calculation (integer 1 in line 55) with only 1 bias (integer 1 in line 56). The bias is applied considering the PageRank value on node 1 (integer 1 in line 57) with targeted value of 0.165 and a bias-force-constant of 10.0. Note, it can be seen later in this section that node 1 in the weighted graph is the [Al]3+, this node id should be fixed (in this example, [Al]3+ is always labeled as node 1 in the weighted graph). 

Lines 58 to 85: Some keywords for the unweighted graph. They are inactive in this example.


Run ChemNetworks:

This example needs to have three xyz-files that contains the coordinates of [Al]3+ ion (al.xyz), [OkHk]1- ion (oh.xyz), and H2O (h2o.xyz). It also needs a standard ChemNetworks input file with keywords listed above. Then, run ChemNetworks as:
./ChemNetworks-C++.exe  Input-sample-C++  h2o.xyz  oh.xyz  al.xyz  
Note, the order of the xyz-files should be the same as in “Input-sample-C++”, i.e. two solvents go first (h2o.xyz & oh.xyz) and then follows the solute (al.xyz)

Output Format:

By running this example, there produces three output files:
1. Weighted.Graph.Input-sample-C++  :	Contains all the weighted edges in the graph with one line per edge.
2. PageRank.Input-sample-C++  :		Contains the PageRank vector, i.e. the PageRank value on every nodes in the graph with one line per node.
3. Force.Input-sample-C++      : 		Contains the Forces on each atom of the selected molecules (nodes of the graph), with one line per atom.

Details on the information presented in the output files, as an example:

1. Weighted.Graph.Input-sample-C++  
This file contains all the weighted edges in the graph with one line per edge. For example, the first line “1 ( st1 1 ) - 2 ( sv1 56 ) edge-weight: 0.123857” corresponds to an edge in the weighted graph between node#1 and node#2 (node id starts from 1 to total_number_of_nodes) with edge-weight of 0.123857. The information of the two nodes are listed in the parenthesis, i.e. node#1 is a solute1 molecule (in this example, it is [Al]3+ ion) with its molecule-id 1 (the first molecule in its xyz-file); while node#2 is a solvent1 molecule (in this example, it is water molecule) with its molecule-id 56 (the 56th molecule in h2o.xyz). 


2. PageRank.Input-sample-C++
This file contains the PageRank values on every node in the weighted graph. It will be empty if the keyword [PAGERANK ON WEIGHTED GRAPH] is set to 0 (line 53). In this example, the keyword is set to 1, therefore PageRank calculation is performed. 
There lists the PageRank value for every node in the weighted graph. For example, the first line “1 ( st1 1 ) pagerank: 1.649306e-01” shows that the PageRank value on node#1 is 1.649306e-01 (scientific notation of 0.1649306). The node#1 is a solute1 molecule with its molecule-id 1. It is convenient to see in this file that the defined weighted graph has 7 nodes, with 1 solute1 ([Al]3+), 2 solvent1 (water), and 4 solvent2 ([OkHk]1-). This is due to the cluster-selection keyword [WEIGHTED GRAPH BY SOLUTE1 CLUSTER] in line 17 to 19, specifying that the weighted graph contains nodes by selecting solvent molecules that are within 3.5 Å to the solute. 

Note: it is important to know the node-id, i.e. node#1 for the solute ([Al]3+), before applying on any PageRank bias. Also, the node you want to put a PageRank bias should have a fixed node-id, otherwise, it won’t work. 


3. Force.Input-sample-C++
This file contains the force on each atoms of molecules in the weighted graph, due to an applied PageRank bias (line 55 to 57). Since the edge-weights are parameterized on site-site distances (line 22 to 52), there are effective forces only on the atoms that are the interacting sites. For example, in this example, the interacting sites are Al atom (atom-id 1) in [Al]3+, O atom in H2O, and Ok atom in [OkHk]1-. As a result, the effective forces are non-zero only on these atoms:

For example, the first line  
“1 ( st1 1 atom 1 ) force: -7.062603e-06 6.263201e-06 3.090272e-06”  
corresponds to the forces on Al atom from [Al]3+, with force components in X-, Y-, Z-directions. And the 2nd to 4th lines  
“2 ( sv1 56 atom 1 ) force: 3.601290e-06 -1.191387e-05 -9.797268e-07  
2 ( sv1 56 atom 2 ) force: 0.000000e+00 0.000000e+00 0.000000e+00  
2 ( sv1 56 atom 3 ) force: 0.000000e+00 0.000000e+00 0.000000e+00”  
corresponds to the forces on atoms of water molecule (molecule-id 56, i.e. the 56th water in h2o.xyz), where there are no forces on the two hydrogens (atom 2 & atom 3) of that water since there is no edge weight parameterized on those atom-sites.
Note: If the edge-weights are parameterized on distances associated with water hydrogen atoms, then there will be forces on those hydrogens. 

