Analyzing ChemNetworks Outputs {#post-processing}
====================================

Although the core ChemNetworks code does several analyses, there are available within this package a large range of scripts meant to provide further insight into network organization and dynamics (time dependent properties). These scripts, provided in the Post-processing subdirectory include:

1. Determining edge distributions 
2. Calculating the Geodesic Index and Scripts for calculating statistics and properties of geodesics
3. Determining lifetimes of edges and correcting those lifetimes for artifacts associated with the rigid edge criterion (transient breaks and bonds)
4. Survival Probabilities
4. Identification of Clusters
5. Denticity of ligands to metal ions
6. Ion Solvation composition
7. Creating matrices of the transitions between specific network patterns (2-state and 3-state models)
8. Analysis of the prevalence and lifetimes of cyclic structures of water










