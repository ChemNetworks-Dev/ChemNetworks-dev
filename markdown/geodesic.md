Optional Input for Calculating Geodesics (shortest paths) {#geodesic}
====================================

Below is optional input to calculate geodesics in the network.

<b> [GEODESICS GD]</b>  1

<b> [GD SOLVENT1]</b>  1

<b> [GD SOLVENT1 EUCLIDEAN DISTANCE]</b>  1

<b> [GD SOLVENT1 EUCLIDEAN REFERENCE]</b>  1 

<b> [GD SOLVENT2]</b>  0

<b> [GD SOLVENT2 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT2 EUCLIDEAN REFERENCE]</b>  0 

<b> [GD SOLVENT3]</b>  0

<b> [GD SOLVENT3 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT3 EUCLIDEAN REFERENCE]</b>  0

<b> [GD SOLVENT1 SOLVENT2]</b>  0

<b> [GD SOLVENT1 SOLVENT2 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT1 SOLVENT2 EUCLIDEAN REFERENCE]</b>  0 0

<b> [GD SOLVENT1 SOLVENT3]</b>  0

<b> [GD SOLVENT1 SOLVENT3 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT1 SOLVENT3 EUCLIDEAN REFERENCE]</b>  0 0

<b> [GD SOLVENT2 SOLVENT3]</b>  0

<b> [GD SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b>  0 0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3]</b>  0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b>  0

<b> [GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b>  0 0 0






<b>[GEODESICS GD]</b> 1 (Yes, calculate the geodesic distance (gd) matrix and Euclidean distance for all requested graph/network type. Then, all keywords below starting with GD are required); 0 (No)

<b>[GD SOLVENT1]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT1] keyword); 0 (No)

<b>[GD SOLVENT1</b> EUCLIDEAN DISTANCE] 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT1] keyword); 0 (No) 

<b>[GD SOLVENT1 EUCLIDEAN REFERENCE]</b> N -> Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT1] keyword, of terminal nodes.  

<b>[GD SOLVENT2]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT2 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT2 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT2 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT2 EUCLIDEAN REFERENCE]</b> N -> Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT2] keyword, of terminal nodes.   

<b>[GD SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT3 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT3 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT3 EUCLIDEAN REFERENCE]</b> N -> Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT3] keyword, of terminal nodes.  

<b>[GD SOLVENT1 SOLVENT2]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 EUCLIDEAN REFERENCE]</b> M N -> Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent1” and N for “solvent2”, of terminal nodes.

<b>[GD SOLVENT1 SOLVENT3]</b>1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N -> Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent1” and N for “solvent3”, of terminal nodes.

<b>[GD SOLVENT2 SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N -> Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent2” and N for “solvent3”, of terminal nodes.

<b>[GD SOLVENT1 SOLVENT2 SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT2 SOLVENT3] keyword); 0 (No)


<b>[GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N P -> Calculate the Euclidean distance between the reference atoms (whose order number are M for “solvent1”, N for “solvent2”, and P for “solvent3”) of terminal nodes.



<b> CODE FOR GEODESIC PATH ANALYSIS </b>
Once the geodesic distance matrix is obtained from the ChemNetworks, further analyses of the geodesics paths of each length can be carried out using the utility code, geodesic-statistics.c. This C code reads the ChemNetworks output files with the extension .geocard. The file geodesic-statistics.c is provided along with the ChemNetworks package. It can be used to obtain a detailed histogram of the geodesics for the network of interst cross-correlated with the Euclidean distance between the terminal vertices of each geodesic path. Moreover, geodesic-statistics.c can be use to determine the persistence/lifetime of the specific length geodecis paths.

The geodesic distance matrix is printed in the output file named water.xyz.geopath. The first and the second integers at each line represents the water ID numbers, the third integer is the minimum number of contiguous H-bonds that connects the two waters. The actual geodesic path involving the participating waters is also printed. The output file named water.xyz.geocard holds the same information in addition to the Euclidean distance printed at the end of each line. Note that the geodesic distance matrix is a symmetric N x N matrix, i.e. the shortest path from vertex i to vertex j is the same as the shortest path from vertex j to vertex i. Therefore, to prevent double counting only the matrix elements from the upper triangle are printed in the output files ending with .geopath and .geocard.


