Input file to ChemNetworks{#input}
====================================

The input for ChemNetworks consists of an input file, along with the separated xyz coordinates of the chemical species defined to be nodes in the network. There are two major classifications of chemical species (largely for historical reasons): solvent and solute. This classification will be changed in future versions of the code. At this point in time you may define up to 3 solvents and 2 solutes in the input file.

Solvents can be defined using energetic, distance, and angle criterion. Whereas solutes can only be defined using distance criterion. 

Occasionally you may find redundancies in the input file. For example - you have to "turn on"  the ability to create a network between nodes before you can define the definition of the edge between nodes. Further, the code was originally only envisioned for water containing solutions and as such, there are multiple references to "HB definition" in the input file. Of course, the HB is merely an edge and so any references to HB in the input file are merely referring to an edge definition. 

There are multiple example input files provided in the "tests" folder and a template input file is also available that contains the minimum specifications required in the input file. 

Note that the order of the filenames used when running the executable and the order of the chemical species defined in the input file must match. 


<b> <span style="background-color: #FFFF00"> ChemNetworks Input </span> </b>


<b>[NUMBER OF SOLVENT TYPES]</b> The number of different types of “solvent” species. Max. is 3. 

<b>[NUMBER OF SOLUTE TYPES]</b> The number of different types of “solute” species. Max. is 2.

<b>[NUMBER OF ATOMS IN SOLVENT1]</b> The number of atoms in “solvent1” molecule. Then, the atom labels with their positions/orders within the molecule must be written. These must be consistent with the corresponding .xyz file.

<b>[NUMBER OF ATOMS IN SOLVENT2]</b> Must be specified if the [NUMBER OF SOLVENT TYPES] has value of 2.

<b>[NUMBER OF ATOMS IN SOLVENT3]</b> Must be specified if the [NUMBER OF SOLVENT TYPES] has value of 3.

<b>[NUMBER OF ATOMS IN SOLUTE1]</b> Must be specified if the [NUMBER OF SOLUTE TYPES] has value of 1. The number of atoms in “solute1” molecule. Then, the atom labels with their positions in the molecule must be written. These must be consistent with the corresponding .xyz file.

<b>[NUMBER OF ATOMS IN SOLUTE2]</b> Must be specified if the [NUMBER OF SOLUTE TYPES] has value of 2.

<b>[PERIODIC BOUNDARY CONDITIONS] 1</b> (Yes, take into account the periodic boundary conditions); 0 (No)

<b>[BOX XSIDE]</b> Specify the rectangular simulation box dimensions (x-side) if the value of [PERIODIC BOUNDARY CONDITIONS] is 1.  
<b>[BOX YSIDE]</b> The simulation box dimensions (y-side)  
<b>[BOX ZSIDE]</b> The simulation box dimensions (z-side)

<b>[GRAPH SOLVENT1 SOLVENT1]</b> 1 (Yes, construct the H-bond network of “solvent1”); 0 (No)

<b>[SOLVENT1 SOLVENT1 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT1] is 1. The H-bond distance criteria between “solvent1” molecules are specified between the pairs of atoms of “solvent1” molecules. Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT1 SOLVENT1 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT1] is 1. The H-bond angle criteria between “solvent1” molecules are specified between the atoms of “solvent1” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT1 SOLVENT1 HBOND ENERGY]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT1] is 1. The pairwise H-bond energy calculation is implemented for water (TIP3P, SPC, SPC/E).

<b>[CHARGES OF ATOMS IN SOLVENT1]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT1] is 1. This is to define the atomic partial charge that is used in calculating pair energy.

<b>[LJ PARAMETERS OF ATOMS IN SOLVENT1]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT1] is 1. This is the pairwise Lennard-Jones (12-6) parameter for water.

<b>[GRAPH SOLVENT2 SOLVENT2]</b> 1 (Yes, construct the H-bond network of “solvent2”); 0 (No)

<b>[SOLVENT2 SOLVENT2 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLVENT2] is 1. The H-bond distance criteria between “solvent2” molecules are specified between the pairs of atoms of “solvent2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT2 SOLVENT2 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLVENT2] is 1. The H-bond angle criteria between “solvent2” molecules are specified between the atoms of “solvent2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT3 SOLVENT3]</b> 1 (Yes, construct the H-bond network of “solvent3”); 0 (No)

<b>[SOLVENT3 SOLVENT3 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT3 SOLVENT3] is 1. The H-bond distance criteria between “solvent3” molecules are specified between the pairs of atoms of “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT3 SOLVENT3 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT3 SOLVENT3] is 1. The H-bond angle criteria between “solvent3” molecules are specified between the atoms of “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT1 SOLVENT2]</b> 1 (Yes, construct the network formed between “solvent1” and “solvent2” H-bonds); 0 (No)

<b>[SOLVENT1 SOLVENT2 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT2] is 1. The H-bond distance criteria between “solvent1” and “solvent2” molecules are specified between the pairs of atoms of “solvent1” and “solvent2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT1 SOLVENT2 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT2] is 1. The H-bond angle criteria between “solvent1” and “solvent2”molecules are specified between the atoms of “solvent1” and “solvent2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT1 SOLVENT3]</b> 1 (Yes, construct the network formed between “solvent1” and “solvent3” H-bonds); 0 (No)

<b>[SOLVENT1 SOLVENT3 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT3] is 1. The H-bond distance criteria between “solvent1” and “solvent3” molecules are specified between the pairs of atoms of “solvent1” and “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT1 SOLVENT3 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLVENT3] is 1. The H-bond angle criteria between “solvent1” and “solvent3” molecules are specified between the atoms of “solvent1” and “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT1 SOLUTE1]</b> 1 (Yes, construct the network formed between “solvent1” and “solute1” H-bonds); 0 (No)

<b>[SOLVENT1 SOLUTE1 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLUTE1] is 1. The cutoff distance criteria between “solvent1” and “solute1” molecules are specified between the pairs of atoms of “solvent1” and “solute1” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT1 SOLUTE2]</b> 1 (Yes, construct the network formed between “solvent1” and “solute2” H-bonds); 0 (No)

<b>[SOLVENT1 SOLUTE2 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT1 SOLUTE2] is 1. The cutoff distance criteria between “solvent1” and “solute2” molecules are specified between the pairs of atoms of “solvent1” and “solute2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT2 SOLVENT3]</b> 1 (Yes, construct the network formed between “solvent2” and “solvent3” H-bonds); 0 (No)

<b>[SOLVENT2 SOLVENT3 HBOND DISTANCE]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLVENT3] is 1. The H-bond distance criteria between “solvent2” and “solvent3” molecules are specified between the pairs of atoms of “solvent2” and “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[SOLVENT2 SOLVENT3 HBOND ANGLE]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLVENT3] is 1. The H-bond angle criteria between “solvent2” and “solvent3” molecules are specified between the atoms of “solvent2” and “solvent3” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT2 SOLUTE1]</b> 1 (Yes, construct the network formed between “solvent2” and “solute1” H-bonds); 0 (No)

<b>[SOLVENT2 SOLUTE1 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLUTE1] is 1. The cutoff distance criteria between “solvent2” and “solute1” molecules are specified between the pairs of atoms of “solvent2” and “solute1” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT2 SOLUTE2]</b> 1 (Yes, construct the network formed between “solvent2” and “solute2” H-bonds); 0 (No)

<b>[SOLVENT2 SOLUTE2 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT2 SOLUTE2] is 1. The cutoff distance criteria between “solvent2” and “solute2” molecules are specified between the pairs of atoms of “solvent2” and “solute2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT3 SOLUTE1]</b> 1 (Yes, construct the network formed between “solvent3” and “solute1” H-bonds); 0 (No)

<b>[SOLVENT3 SOLUTE1 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT3 SOLUTE1] is 1. The cutoff distance criteria between “solvent3” and “solute1” molecules are specified between the pairs of atoms of “solvent3” and “solute1” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT3 SOLUTE2]</b> 1 (Yes, construct the network formed between “solvent3” and “solute2” H-bonds); 0 (No)

<b>[SOLVENT3 SOLUTE2 CUTOFF]</b> Must be specified if the value of [GRAPH SOLVENT3 SOLUTE2] is 1. The cutoff distance criteria between “solvent3” and “solute2” molecules are specified between the pairs of atoms of “solvent3” and “solute2” molecules.  Each criterion is given with two real numbers to specify a range.

<b>[GRAPH SOLVENT1 SOLVENT2 SOLVENT3]</b> 1 (Yes, construct the H-bond network of entire system of  “solvent1”, “solvent2” and “solvent3”); 0 (No)

<b>[PRINT NUMBER OF NODES]</b> 1 (Yes, print the number of nodes within every graph); 0 (No)

<b>[SOLUTE1 WATER DIPOLE ORIENTATIONS]</b> 1 N  1: (Yes, get the water dipole angles w.r.t. the “solute1” species); N: Water is the Nth “solvent” defined in the input file.

<b>[SOLUTE2 WATER DIPOLE ORIENTATIONS]</b> 0 (No)

<b>[SOLVENT WATER DIPOLE ORIENTATIONS]</b> 1 N M  1: (Yes, get the water dipole angles w.r.t. the Mth “solvent” defined in the input file);  N: Water is the Nth “solvent” defined in the input file. 

<b>[GEODESICS GD]</b> 1 (Yes, calculate the geodesic distance (gd) matrix and Euclidean distance for all requested graph/network type. Then, all keywords below starting with GD are required); 0 (No)

<b>[GD SOLVENT1]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT1] keyword); 0 (No)

<b>[GD SOLVENT1 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT1] keyword); 0 (No) 

<b>[GD SOLVENT1 EUCLIDEAN REFERENCE]</b> N  Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT1] keyword, of terminal nodes.  

<b>[GD SOLVENT2]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT2 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT2 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT2 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT2 EUCLIDEAN REFERENCE]</b> N  Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT2] keyword, of terminal nodes.   

<b>[GD SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT3 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT3 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT3 EUCLIDEAN REFERENCE]</b> N  Calculate the Euclidean distance between the reference atoms, whose order number is N as defined in [NUMBER OF ATOMS IN SOLVENT3] keyword, of terminal nodes.  

<b>[GD SOLVENT1 SOLVENT2]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT2] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 EUCLIDEAN REFERENCE]</b> M N  Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent1” and N for “solvent2”, of terminal nodes.

<b>[GD SOLVENT1 SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N  Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent1” and N for “solvent3”, of terminal nodes.

<b>[GD SOLVENT2 SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N  Calculate the Euclidean distance between the reference atoms, whose order number are M for “solvent2” and N for “solvent3”, of terminal nodes.

<b>[GD SOLVENT1 SOLVENT2 SOLVENT3]</b> 1 (Yes, calculate gd matrix for the network obtained from [GRAPH SOLVENT1 SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN DISTANCE]</b> 1 (Yes, calculate Euclidean distance between the terminal nodes of all geodesic pathways within the network obtained from [GRAPH SOLVENT1 SOLVENT2 SOLVENT3] keyword); 0 (No)

<b>[GD SOLVENT1 SOLVENT2 SOLVENT3 EUCLIDEAN REFERENCE]</b> M N P Calculate the Euclidean distance between the reference atoms (whose order number are M for “solvent1”, N for “solvent2”, and P for “solvent3”) of terminal nodes.

<b>[WATER STRUCTURES]</b> 1 N  1: (Yes, search for water oligomers. Then, all keywords below starting with WATER are required), 0 (No); N: Water is the Nth “solvent” defined in the input file.

<b>[WATER HEXAMER RING]</b> 1 (Yes, search for cyclic ring hexamers); 0 (No)  
<b>[WATER HEXAMER BOOK]</b> 1 (Yes, search for book hexamers); 0 (No)  
<b>[WATER HEXAMER PRISM]</b> 1 (Yes, search for prism hexamers); 0 (No)  
<b>[WATER HEXAMER CAGE]</b> 1 (Yes, search for cage hexamers); 0 (No)  
<b>[WATER HEXAMER BAG]</b> 1 (Yes, search for bag hexamers); 0 (No)  
<b>[WATER HEXAMER BOAT]</b> 1 (Yes, search for boat hexamers); 0 (No)  
<b>[WATER HEXAMER CHAIR]</b> 1 (Yes, search for chair hexamers); 0 (No)  
<b>[WATER HEXAMER PRISMBOOK]</b> 1 (Yes, search for prismbook hexamers); 0 (No)  
<b>[WATER PENTAMER SEARCH]</b> 1 (Yes, search for cyclic pentamers); 0 (No)  
<b>[WATER TETRAMER SEARCH]</b> 1 (Yes, search for cyclic tetramers); 0 (No)  
<b>[WATER TRIMER SEARCH]</b> 1 (Yes, search for cyclic trimers); 0 (No)  
<b>[WATER ISOLATED STRUCTURES]</b> 1 (Yes, search for all isolated oligomers requested above); 0 (No)  
<b>[POLYHEDRA]</b> 1 (Yes,  search for polyhedral graphs. Then the following keywords are required) 0 (No)

<b>[MAX SHELL SIZE]</b> N (Only include up to N atoms in in the solvation shell. Will print a warning if this is exceeded.)

<b>[POLYHEDRA EDGE CERTAINTY BOUNDS]</b> N (There are N bounds described below)

SHELLSIZE MIN_EDGE MAX_EDGE (For SHELLSIZE, edges are created for all lengths below MIN_EDGE, and are considered for all edges between MIN_EDGE and MAX_EDGE.) 

<b>[PAIRED SHELL DISTANCES]</b> 1 (Yes, output distances between “polyhedra-eligible” atoms in the solvation shell. Requires MAX SHELL SIZE Keyword.), 0 (No)


<b>----------OPTIONAL INPUT FOR WEIGHTED GRAPHS--------------------<b>

The construction of weighted networks requires an additional section in ChemNetworks along with the existing unweighted graph analysis. Generally, the construction of Weighted Graph is a two-step process: 1) select the nodes, 2) determine the edge weights between the selected nodes. For the first step, there are two ways to select the nodes, one is by distance to solute (i.e. select a local cluster to solute) and the other is by solvent/solute type (i.e. select all solvent/solute molecules from input xyz-files). For the second step, the edge weights are parameterized on interaction distances using a smooth function (see release note Mar-2018).


<b>[WEIGHTED GRAPH]</b> 1 (Yes, request weighted graph calculation); 0 (No)

<b>[WEIGHTED GRAPH BY SOLUTE1 CLUSTER]</b> N (number of ways to select solvent molecules within the solute1 cluster). If N > 0, this line should be followed directly by N lines of parameter list for each way of selecting cluster. As an example, “1 1 1 3 0.0 3.2” means selecting all the solvent 1 (the 1st solvent) that are 0.0 ~3.2 Å to solute 1 (the 1st solute), the distance is calculated from atom 1 in solute 1 to atom 3 in solvent 1.

<b>[WEIGHTED GRAPH BY SOLUTE2 CLUSTER]</b> N (number of ways to select the solvent molecules within solute2 cluster). This is the same for solute2 as it is for solute1.

<b>[WEIGHTED GRAPH BY SOLUTE TYPE]</b> N (number of solute types to be included in the nodes of weighted graph). If N > 0, this line should be followed directly by N lines of solute-type id (only 1 for solute1 and/or 2 for solute2 is accepted).

<b>[WEIGHTED GRAPH BY SOLVENT TYPE]</b> N (number of solvent types to be included in the nodes of weighted graph). If N > 0, this line should be followed directly by N lines of solvent-type id (only 1 for solvent1 and/or 2 for solvent2 is accepted).

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> 1 (Yes, request weighted-edges between solute1 and solvent1) or 0 (No).

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solvent1, this is to allow multiple interactions). Must be specified if keyword [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] is set to 1. If N > 0, this line should be followed directly by N lines of atom-index pairs. As an example, a pair “2 3” means the edge-weight is parameterized on the distance between atom 2 in solute1 and atom 3 in solvent1. 

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). Must be specified if keyword [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] is set to 1. This function-type = 1 is corresponding to a fermi function. This keyword should be followed directly by 1 line with the associated function parameters n0 and r0. As an example, “10.0 2.8” directly following this keyword means n0 = 10.0 and r0 = 2.8.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute2 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute2 and solvent2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE2 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solute2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLUTE1 SOLUTE2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solute1-solute2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solvent1 and solvent1), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT1 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent1-solvent1 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solvent2 and solvent2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT2 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent2-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2]</b> 1 (Yes) or 0 (No), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE]</b> N (N ≧ 0, number of interaction distances between solute1 and solute2), similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1] but for solvent1-solvent2 edges.

<b>[WEIGHTED GRAPH EDGE SOLVENT1 SOLVENT2 DISTANCE FUNCTION]</b> 1 (function-type that is to determine edge-weight based on distance, only 1 is allowed, see release note Mar-2018). similar to [WEIGHTED GRAPH EDGE SOLUTE1 SOLVENT1]</b> but for solvent1-solvent2 edges.
