Example 2 : Water/Formamide Binary Mixture {#example2}
====================================

<b>WATER/FORMAMIDE BINARY MIXTURE</b>
ChemNetworks input and output files will be discussed for the binary mixture of solvents water and formamide (CH3NO). Mixture of 200 H2O molecules and 50 CH3NO molecules are put inside a rectangular box of sides 22 Å x 27 Å x 24 Å. In this example, three types of graphs will be constructed: (i) Water only network, (ii) Formamide only network, and (iii) the entire network including (i), (ii), and the network between the water and formamide.

<b>Input File (WaterFormamide.input)</b>


<b>[NUMBER OF SOLVENT TYPES]</b> 2

<b>[NUMBER OF SOLUTE TYPES]</b> 0

<b>[NUMBER OF ATOMS IN SOLVENT1]</b> 3


<b>O 1 </b>

<b>H 2 </b>

<b>H 3 </b>

<b>[NUMBER OF ATOMS IN SOLVENT2]</b> 6

<b>N </b>1

<b>H </b>2

<b>H </b>3

<b>C </b>4

<b>O </b>5


<b>H </b>6

<b>[PERIODIC BOUNDARY CONDITIONS]</b> 1

<b>[BOX XSIDE]</b> 22.0

<b>[BOX YSIDE]</b> 27.0

<b>[BOX ZSIDE]</b> 24.0

<b>[GRAPH SOLVENT1 SOLVENT1]</b> 1

<b>[SOLVENT1 SOLVENT1 HBOND DISTANCE]</b> 4

<b>1 2 </b>0.00 2.50

<b>1 3 </b>0.00 2.50

<b>2 1 </b>0.00 2.50

<b>3 1 </b>0.00 2.50

<b>[SOLVENT1 SOLVENT1 HBOND ANGLE] </b>4

<b>1 2</b>

<b>1 2 1 </b>145.0 180.0

<b>1 2</b>

<b>1 3 1 </b>145.0 180.0

<b>2 1</b>

<b>1 2 1 </b>145.0 180.0                                                                    

<b>2 1 </b>

<b>1 3 1 </b>145.0 180.0



<b>[GRAPH SOLVENT2 SOLVENT2]</b> 1

<b>[SOLVENT2 SOLVENT2 HBOND DISTANCE]</b> 4

<b>5 2</b> 0.00 2.85

<b>5 3</b> 0.00 2.85

<b>2 5 </b>0.00 2.85

<b>3 5</b> 0.00 2.85

<b>[SOLVENT2 SOLVENT2 HBOND ANGLE]</b> 4

<b>1 2</b>

<b>5 2 1</b> 125 180.0

<b>1 2</b>

<b>5 3 1 </b>125 180.0

<b>2 1</b>

<b>1 2 5</b> 125 180.0                                                                                       




<b>2 1</b>

<b>1 3 5 </b>125 180.0

<b>[GRAPH SOLVENT3 SOLVENT3] </b>0

<b>[GRAPH SOLVENT1 SOLVENT2] </b>1 

<b>[SOLVENT1 SOLVENT2 HBOND DISTANCE]</b> 6

<b>1 2</b> 0.00 2.75

<b>1 3</b> 0.00 2.75

<b>2 1 </b>0.00 2.75

<b>3 1 </b>0.00 2.75

<b>2 5 </b>0.00 2.75

<b>3 5 </b>0.00 2.75

<b>[SOLVENT1 SOLVENT2 HBOND ANGLE]</b> 6

<b>1 2</b>

<b>1 2 1 </b>125 180.0

<b>1 2</b>

<b>1 3 1</b> 125 180.0

<b>2 1</b>

<b>1 2 1 </b>125 180.0

<b>2 1  </b>                                                                                                 


<b>1 3 1</b> 125 180.0

<b>2 1</b>

<b>1 2 5 </b>125 180.0

<b>2 1</b>

<b>1 3 5</b> 125 180.0

<b>[GRAPH SOLVENT1 SOLVENT3] </b>0

<b>[GRAPH SOLVENT1 SOLUTE1] </b>0

<b>[GRAPH SOLVENT1 SOLUTE2]</b> 0

<b>[GRAPH SOLVENT2 SOLVENT3]</b> 0

<b>[GRAPH SOLVENT2 SOLUTE1] </b>0

<b>[GRAPH SOLVENT2 SOLUTE2]</b> 0


<b>[GRAPH SOLVENT3 SOLUTE1]</b> 0

<b>[GRAPH SOLVENT3 SOLUTE2] </b>0

<b>[GRAPH SOLVENT1 SOLVENT2 SOLVENT3]</b> 0

<b>[PRINT NUMBER OF NODES] </b>1

<b>[GEODESICS GD]</b> 0

<b>[SOLUTE1 WATER DIPOLE ORIENTATIONS]</b> 0

<b>[SOLUTE2 WATER DIPOLE ORIENTATIONS] </b>0

<b>[SOLVENT WATER DIPOLE ORIENTATIONS]</b> 0 0 0

<b>[WATER STRUCTURES]</b> 0


Line 1: Required keyword for the number of major species of interest. Integer value is 2, here, as we have two chemical species.

Line 2: No solute species are present. Therefore, value is set to 0.

Line 3: Number of atoms in a water molecule (“solvent 1”) is 3.

Lines 4 to 6: Atom labels used in the .xyz file for the water, and their position/order numbers next to labels. Here, the water molecule is described as OHH; oxygen is the first, with label “O” and then the hydrogens with the label “H” for both.

Line 7: Number of atoms in a formamide molecule (“solvent 2”) is 6.

Lines 8 to 13: Atom labels used in the .xyz file for the formamide, and their position/order numbers next to labels. Here, the formamide molecule is described as NHHCOH. There are absolutely no restrictions on the atom labels used and the way they are ordered to describe the molecule of interest.  

Line 14: Periodic boundary conditions are requested to be taken into account across the boundaries of the rectangular box of which has dimensions must be provided.

Lines 15 to 17: X-, Y-, and Z-dimensions of the box, respectively. In this example, we have a rectangular box of side-lengths 22 Å x 27 Å x 24 Å.

Line 18: The water H-bond graph/network is requested to be constructed.

Line 19: The number of H-bond interactions to be used to describe the edges (H-bond) between two water molecules (see Figure 1). Here, there are four possible O...H pairs for the two waters. 

Lines 20 to 23: Distance criteria for the edge formation. The cutoff distance between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom) is set to 2.5 Å (line 20). The cutoff distance between the 1st atom of water A (O-atom) and the 3rd atom of water B (H-atom) is set to 2.5 Å (line 21).  The cutoff distance between the 2nd atom of water A (H-atom) and the 1st atom of water B (O-atom) is set to 2.5 Å (line 22).  The cutoff distance between the 3rd atom of water A (H-atom) and the 1st atom of water B (O-atom) is set to 2.5 Å (line 23). (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)

Line 24: The number of H-bond interactions to be used to describe the edges (H-bond) between two water molecules (see Figure 1). Here, there are four possible O...H pairs for the two waters. The value here must match the one entered in line 19.

Lines 25 to 32: Angle criteria for the edge formation. Angle requires 3 atoms, 2 of which from one water and 1 atom from the other water. The H-atom is always at the middle (OHO) in the angle definitions. When 1 atom is from the 1st molecule and 2 atoms from the 2nd molecule “1 2” combination is used (lines 25 and 27), whereas when 2 atoms are from the 1st molecule and 1 atom from the 2nd molecule “2 1” combination is used (lines 29 and 31). The order of angle criteria must be consistent with the order of distance criteria. Line 25: 1 atom from water A, and 2 atoms from water B. Line 26: The angle cutoff between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom), these two atoms were used in line 20, and the 1st atom of water B (O-atom) is set to 145.0°. Therefore, whenever the distance between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom) is less than 2.5 Å, and the angle between the 1st atom of water A (O-atom) and the 2nd atom of water B (H-atom), and the 1st atom of water B (O-atom) is larger than 145.0°, an edge will be formed between the water A and water B. As you can see, lines 20 and 26 define geometric criteria (distance and angle) together. Similarly, lines 21 and 28; lines 22 and 30; and lines 23 and 32 match in defining the geometric criteria. All pairs of water molecules satisfying one of these criteria will be considered H-bonded and printed as pairs in the output file ending with .Graph. (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)  

Line 33: Formamide only H-bond graph/network is requested to be constructed.

Line 34: The number of H-bond interactions to be used to describe the edges (H-bond) between two formamide molecules (see Figure 2). In this example, the hydrogen bond interactions involving only the O...H pairs are taken into account. 

Lines 35 to 38: Distance criteria for the edge formation. The cutoff distance between the 5th atom of formamide A (O-atom) and the 2nd atom of formamide B (H-atom) is set to 2.85 Å (line 35). The cutoff distance between the 5th atom of formamide A (O-atom) and the 3rd atom of formamide B (H-atom) is set to 2.85 Å (line 36).  The cutoff distance between the 2nd atom of formamide A (H-atom) and the 5th atom of formamide B (O-atom) is set to 2.85 Å (line 37).  The cutoff distance between the 3rd atom of formamide A (H-atom) and the 5th atom of formamide B (O-atom) is set to 2.85 Å (line 38). (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)

Line 39: The number of H-bond interactions to be used to describe the edges between the two formamide molecules (see Figure 2). The value here must match the one entered in line 34.

Lines 40 to 47: Angle criteria for the edge formation. Angle requires 3 atoms, 2 of which from one formamide and 1 atom from the other formamide. The H-atom is always at the middle in the angle definitions. When 1 atom is from the 1st molecule and 2 atoms from the 2nd molecule “1 2” combination is used (lines 40 and 42), whereas when 2 atoms are from the 1st molecule and 1 atom from the 2nd molecule “2 1” combination is used (lines 44 and 46). The order of angle criteria must be consistent with the order of distance criteria. Line 40: 1 atom from formamide A, and 2 atoms from formamide B. Line 41: The angle cutoff between the 5th atom of formamide A (O-atom) and the 2nd atom of formamide B (H-atom), these two atoms were used in line 35, and the 1st atom of formamide B (N-atom) is set to 125.0°. Therefore, whenever the distance between the 5th atom of formamide A (O-atom) and the 2nd atom of formamide B (H-atom) is less than 2.5 Å, and the angle between the 5th atom of formamide A (O-atom) and the 2nd atom of formamide B (H-atom), and the 1st atom of formamide B (N-atom) is larger than 125.0°, an edge will be formed between the formamide A and formamide B. As you can see, lines 35 and 41 define geometric criteria (distance and angle) together. Similarly, lines 36 and 43; lines 37 and 45; and lines 38 and 47 match in defining the geometric criteria. All pairs of formamide molecules satisfying one of these criteria will be considered H-bonded and printed as pairs in the output file ending with .Graph. (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)  

Line 48: Value is set to 0, as there is no third type species present in the chemical system.

Line 49: The entire network formed between the water and formamide including water-water, formamide-water, and water-formamide interactions/edges will be constructed. If the network of only the water-formamide interactions/edges is needed, then only the keyword in line 49 should be activated and the keywords in lines 18 and 33 must be turned off.

Line 50: The number of H-bond interactions to be used to describe the edges between water-formamide molecules (see Figure 3).

Lines 51 to 56: Distance criteria for the edge formation. The cutoff distance between the 1st atom of water (O-atom) and the 2nd atom of formamide (H-atom) is set to 2.75 Å (line 51). The cutoff distance between the 1st atom of water (O-atom) and the 3rd atom of formamide (H-atom) is set to 2.75 Å (line 52).  The cutoff distance between the 2nd atom of water (H-atom) and the 5th atom of formamide (O-atom) is set to 2.75 Å (line 55).  The cutoff distance between the 3rd atom of water (H-atom) and the 5th atom of formamide (O-atom) is set to 2.75 Å (line 56). The cutoff distance between the 2nd atom of water (H-atom) and the 1st atom of formamide (N-atom) is set to 2.75 Å (line 53). The cutoff distance between the 3rd atom of water (H-atom) and the 1st atom of formamide (N-atom) is set to 2.75 Å (line 54). (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)  

Line 57: The number of H-bond interactions to be used to describe the edges between the water- formamide molecules (see Figure 3). The value here must match the one entered in line 50.

Lines 58 to 69: Angle criteria for the edge formation. Angle requires 3 atoms, 2 of which from one molecule and 1 atom from the other molecule. The H-atom is always at the middle in the angle definitions. When 1 atom is from the 1st molecule and 2 atoms from the 2nd molecule “1 2” combination is used (lines 58 and 60), whereas when 2 atoms are from the 1st molecule and 1 atom from the 2nd molecule “2 1” combination is used (lines 62, 64, 66, and 68). The order of angle criteria must be consistent with the order of distance criteria. Line 58: 1 atom from water, and 2 atoms from formamide. Line 59: The angle cutoff between the 1st atom of water (O-atom) and the 2nd atom of formamide (H-atom), these two atoms were used in line 51, and the 1st atom of formamide (N-atom) is set to 125.0°. Therefore, whenever the distance between the 1st atom of water (O-atom) and the 2nd atom of formamide (H-atom) is less than 2.75 Å, and the angle between the 1st atom of water (O-atom) and the 2nd atom of formamide (H-atom), and the 1st atom of formamide (N-atom) is larger than 125.0°, an edge will be formed between the water and formamide molecules. As you can see, lines 51 and 59 define geometric criteria (distance and angle) together. Similarly, lines 52 and 61; lines 53 and 63; lines 54 and 65; lines 55 and 67; and lines 56 and 69 match in defining the geometric criteria. All pairs of water-formamide molecules satisfying one of these criteria will be considered H-bonded and printed as pairs in the output file ending with .Graph. (Version-2.2 updates in blue, use two criteria with a lower boundary and an upper boundary)   

Lines 70 to 78: All other keywords (starting with GRAPH) for graph/network construction are set to 0.

Line 79: The number of vertices/nodes (water molecules in this example) making up all requested graphs will be printed in an output files ending with .GraphNumnodes.

Line 80: No geodesic distance analysis is requested.

Lines 81 to 83: No water dipole orientation calculations are requested.

Line 84: No water oligomer search is requested.

Running: ./ChemNetworks.exe WaterFormamide.input water.xyz formamide.xyz

<b>Output Files</b>

The list of all output file names is shown below.

Water only [GRAPH SOLVENT1 SOLVENT1]

- WaterFormamide.input.water.xyz.water.xyz.Graph

- WaterFormamide.input.water.xyz.water.xyz.GraphGeod

- WaterFormamide.input.water.xyz.water.xyz.GraphNumnodes

 
 Formamide only [GRAPH SOLVENT2 SOLVENT2]

- WaterFormamide.input.formamide.xyz.formamide.xyz.Graph
- WaterFormamide.input.formamide.xyz.formamide.xyz.GraphGeod
- WaterFormamide.input.formamide.xyz.formamide.xyz.GraphNumnodes
 
 Water-Formamide edges [GRAPH SOLVENT1 SOLVENT2]

- WaterFormamide.input.water.xyz.formamide.xyz.Graph
- WaterFormamide.input.water.xyz.formamide.xyz.GraphGeod
- WaterFormamide.input.water.xyz.formamide.xyz.GraphNumnodes
