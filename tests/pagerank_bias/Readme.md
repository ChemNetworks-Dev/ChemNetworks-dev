We can use ChemNetworks in combination with a MD driver engine to bias a system using forces from a harmonic biased in PageRank space. 

## Workflow

1. Running ChemNetworks using input with PR bias functionality activated (see input template: in-pr-pmf-0.18)

2. Copy atom-specific Forces from pr-force file output and add each component Fi to the forces obtained from a single point energy/force calculations using a conventional software (In this example we use CP2K) (we provide a script that performs this task, (bb))

3. Use our external MD driver to update forces and velocities and perform the molecular translation. (md.new.f)

4. To perform MD simulations edit timestep, number-of-steps, temperature, velocity-rescaling-frequency, random-number-generator values from the md input file (in-md-pmf)

In this Folder we provide a working example of the Workflow described above. 

The submission script (jmpf-0.18.srun) contains all the steps in a single routine. 

To sucessfully run a PageRank bias simulation you will need the following intial files

## Initial Files needed in Working Folder

    1. temp.xyz and new.xyz files : Both contain the initial structure of the system
   
    2. tvel.xyz : sample atom velocity file of the new.xyz file 

### Input files

    3. in-pr : input file read by ChemNetworks containing parameters for PR bias simulations (Read Input Guide of ChemN for specifics on these parameters) 
   
    4. in-md-pmf : input file with MD-related directives (see above)

### Software-based files 

    5. cp2k.popt : executable of CP2K software (you can put this in a separate path)
    
    6. mask : cp2k input template ready for coordinate transcription (modify this template according to your system characteristics) 

### Code scripts

    7. usran.f, gasdev.f, bb,  md.new.f : External MD-driver codes needed to perform velocity rescaling and force and position updates. 

    8. chemn.exe or pr.exe : PageRank biased calculator from ChemNetworks software. 


# Example 

## Bias of PR(Al) in Aluminate Al(OH)4- solvated system 
### PR(Al) targeted value = 0.18 ; Force constant = 10 a.u


### Ouput files: 

    1. Trajectory in .xyz format from PR biased simulations
    2. Velocity files 
    3. Output file with PR(Al) values in slurm.out

