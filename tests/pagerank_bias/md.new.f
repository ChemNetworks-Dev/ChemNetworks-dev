c 
c  md.f
c
      include "gasdev.f"
      include "usran.f"
c
      integer n
      integer NP
      parameter (NP=1000)
      character*2 sy(NP)
      real*8 z(NP),m(NP)
      real*8 r(NP,3)
      real*8 v0(NP,3),v1(NP,3),v2(NP,3)
c 
      integer i,k,itemp,idum
c 
      integer it,nt,pt   ! add pt, which is the saving frequency
      real*8  t,dt,irand,gasdev,usran
      real*8 f(NP,3),fpr(NP,3)
      real*8 PE,KE,TEMP,T0
c
      T0 = 300.0
      itemp = 100  ! frequent thermo
      idum = -1  ! seed of random number generator
c 
      i=1
      open(10,FILE='system')
100   read(10,*,END=110) sy(i),m(i)
      i=i+1
      goto 100
110   close(10)
      n=i-1
c 
      read(*,*) dt,nt,pt,T0,itemp,idum
      idum = -abs(idum)
      irand = usran(idum)
      write(*,*) dt,nt,pt,T0,itemp,idum
c
      do i=1,n
         m(i)=m(i)/5.485799d-4
      enddo
c
      open(10,FILE='coord0')
      do i=1,n
        read(10,*) (r(i,k),k=1,3)
      enddo
      close(10)
      do i=1,n
        do k=1,3
          r(i,k)=r(i,k)/0.529177258d0
        enddo
      enddo
c
      do i=1,n
        do k=1,3
c          v0(i,k)=0.0d0
          v0(i,k)=sqrt(T0/3.1577464d5/m(i))*gasdev(idum) ! freq-thermo
        enddo
      enddo
c
c      open(10,FILE='tvel.txt')
c      do i=1,n
c        read(10,*) (v0(i,k),k=1,3)
c      enddo
c      close(10)
c 
      open(10,FILE='coord')
      do i=1,n
        write(10,'(3f20.10)') (r(i,k)*0.529177258d0,k=1,3)
      enddo
      close(10)
c 
      call system("./bb")
c 
      open(10,FILE='grad')
      read(10,*) PE
      do i=1,n
        read(10,*) (f(i,k),k=1,3)
      enddo
      close(10)
c      do i=1,n
c        do k=1,3
c          f(i,k)= -f(i,k)
c        enddo
c      enddo
c
      do i=1,n
        do k=1,3
          v1(i,k)=v0(i,k)+0.5d0*dt*f(i,k)/m(i)
        enddo
      enddo
c
      open(11,FILE='E.out')
      open(12,FILE='traj.xyz')
c
      t=0.0d0
c
      do it=1,nt
c 
        open(10,FILE='coord')
        do i=1,n
          write(10,'(3f20.10)') (r(i,k)*0.529177258d0,k=1,3)
        enddo
        close(10)
c 
        call system("./bb")
c 
        open(10,FILE='grad')
        read(10,*) PE
        do i=1,n
          read(10,*) (f(i,k),k=1,3)
        enddo
        close(10)
c        do i=1,n
c          do k=1,3
c            f(i,k)= -f(i,k)
c          enddo
c        enddo
c
        open(13,FILE='temp.xyz') ! instantaneous position
        write(13,'(i10)') n
        write(13,'(a4)') '    '
        do i=1,n
          write(13,'(a2,3f20.10)') sy(i),(r(i,k)*0.529177258d0,k=1,3)
        enddo
        close(13)
c
        call system("./pr.exe temp.xyz < in-pr")

        open(14,FILE='./snap1.total-force')
        do i=1,n
          read(14,*) (fpr(i,k),k=1,3)
        enddo
        close(14)

        do i=1,n
          do k=1,3
            f(i,k) = f(i,k)+fpr(i,k)
          enddo
        enddo
c
        do i=1,n
          do k=1,3
            v2(i,k)=v1(i,k)+dt*f(i,k)/m(i)
          enddo
        enddo
c
        do i=1,n
          do k=1,3
            v0(i,k)=0.5d0*(v1(i,k)+v2(i,k))
          enddo
        enddo
c
        KE=0.0d0
        do i=1,n
          do k=1,3
            KE=KE+0.5d0*m(i)*v0(i,k)**2
          enddo
        enddo
c
        TEMP = KE*2/3/n*3.1577464d5
c 
        write(*,'(5f20.10)') t,KE,PE,KE+PE,TEMP
        write(11,'(5f20.10)') t,KE,PE,KE+PE,TEMP
        call flush(11)
c
      if(mod(it,pt) .eq. 0) then ! added, pt is saving frequency
        write(12,'(i10)') n
        write(12,'(a)') ' '
        do i=1,n
          write(12,'(a2,3f20.10)') sy(i),(r(i,k)*0.529177258d0,k=1,3)
        enddo
        call flush(12)
      endif
c
      open(15,FILE='tvel.txt') ! instantaneous velovity
        do i=1,n
          write(15,'(3f20.10)') (v2(i,k),k=1,3)
        enddo
      close(15)
       
c
c       ! update position and velocity
        t=t+dt
        do i=1,n
          do k=1,3
            r(i,k)=r(i,k)+dt*v2(i,k)
            if(mod(it,itemp) .eq. 0) then
              v1(i,k)=sqrt(T0/3.1577464d5/m(i))*gasdev(idum) ! freq-thermo
c              v1(i,k)=v0(i,k)*sqrt(T0/TEMP) ! rescale
            else
              v1(i,k)=v2(i,k)
            endif
          enddo
        enddo
c
      enddo
c
      close(11)
      close(12)
c 
      end
