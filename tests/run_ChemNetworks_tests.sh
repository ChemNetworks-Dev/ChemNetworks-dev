#!/bin/bash

echo "here are test jobs for ChemNetworks-C++"
echo ""

# test1 is bulk-water (1 solvent)
echo "test1.."
cd ./test1/
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-test1 water1.xyz
lab=$?
rm ChemNetworks-C++.exe
if test $lab -eq 0; then
  echo "test1 passed"
  echo ""
else       # if test1 failed
  echo "test1: Error!"
  exit 1
fi

cd ../

# test2 is a single methane in water (1 solvent + 1 solute) 
echo "test2.."
cd ./test2/
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-test2 water1.xyz solB1.xyz
lab=$?
rm ChemNetworks-C++.exe
if test $lab -eq 0; then
   echo "test2 passed"
   echo ""
else               # if test2 failed
   echo "test2: Error!"
   exit 2
fi 

cd ../

# test3 is 27 NaOH in water (2 solvent + 1 solute) 
echo "test3.."
cd ./test3/
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-test3 water1.xyz solB1.xyz solC1.xyz
lab=$?
rm ChemNetworks-C++.exe
if test $lab -eq 0; then
   echo "test3 passed"
   echo ""
else               # if test3 failed
   echo "test3: Error!"
   exit 3
fi

cd ../

# test4 is 1 TBP at water/hexane interface (2 solvent + 1 solute) 
echo "test4.."
cd ./test4/
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-test4 water1.xyz solC1.xyz solB1.xyz
lab=$?
rm ChemNetworks-C++.exe
if test $lab -eq 0; then
   echo "test4 passed"
   echo ""
else
   echo "test4: Error"  # if test4 failed
   exit 4
fi

cd ../

# pagerank is test for weighted graph & pagerank analysis
# two examples are: 1 Al(OH)4 in water, and 1 NaOH in water
echo "test-pagerank"
cd ./pagerank/
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-Aluminate h2o.xyz oh.xyz al.xyz
lab=$?
rm ./ChemNetworks-C++.exe
if test $lab -eq 0; then
   echo "test-pagerank: passed for Aluminate example"
else
   echo "test-pagerank: Error"
   exit 5
fi
cp ../../src/ChemNetworks-C++.exe ./
./ChemNetworks-C++.exe Input-NaOH CIP-H2O.xyz CIP-Na.xyz CIP-OH.xyz
lab=$?
rm ./ChemNetworks-C++.exe
if test $lab -eq 0; then
   echo "test-pagerank: passed for NaOH ion-pair example"
else
   echo "test-pagerank: Error"
   exit 5
fi
cd ../

echo ""
echo "ALL TESTS PASSED"
echo ""


