
# Local Documentation for ChemNetworks using Doxygen

**Download the following:** <br />

1.  Doxygen:

- `brew install doxygen` if using homebrew on MacOS otherwise download it [here](http://www.doxygen.nl/download.html)

Run `doxygen cn_newconfig` to generate documentation where **cn_newconfig** is the doxygen configuration file

The html pages will be within the doxyoutput/html directory

To view once in the doxyoutput/html directory:

- `open index.html`

- To create the main pages, use markdown.

# Automatic Documentation for ChemNetworks

Doxygen generates C++ API documentation by parsing a specific commenting style.

The file .gitlab-ci.yml is a YAML file which holds the specifications for the docker image. The image used is an Alpine Linux Distro due to its lightweight nature and ease with containers. Inside the YAML file:

- the pages segment is for deployment into GitLab pages which hosts the website.
- the artifacts are the files that are created on the docker container which will be moved to a public dir (that you don't see).
- the only segment is the branch you want to monitor

Everytime a change is pushed, a pipeline job is executed which can be monitored under CI/CD pipelines. When done, there should be two checkmarks

![picture](pictures/pipeline_success.png)

If you click the first checkmark, and then pages it'll open up the log for the script that ran, this is especially useful if you encounter an **_ERROR_**

The website where the documentation is: https://auclark1212.gitlab.io/ChemNetworks-dev and is public

![picture](pictures/website.png)

**IMPORTANT**: If you receive a 404 error but the pipeline has two checkmarks indicating it worked, it can sometimes take a bit of time to update the website. In this event, it is easier to view the index.html file locally as it will render the _exact_ same online

# Code Documentation Commenting Guidelines

- You can find information on Doxygen and commenting [here](http://www.doxygen.nl/manual/docblocks.html)
- Markdown information can be found [here](https://daringfireball.net/projects/markdown/syntax)
-

# How to document the code examples:

To reference a function in ChemNetworks in markdown, use: `\ref CN_NS::ChemNetworkNew::input_read() "input_read()"`

```cpp
/**
 * \brief Example class, feel free to edit this
 * Also possible to do:
 * \class ExampleClassRandom
 * \brief feel free to edit this
 */
class ExampleClassRandom
{
    /**
     * \brief       some member function random func does a random thing with 4 parameters
     * \param a     an integer value which holds something
     * \param b     an integer value which reps something
     * \param c     a character which does something
     * \param d     a double which holds some value required by the function
     * \return      returns a boolean value, used in maybe another function?
    */
    bool randomFunc(int a, int b, char c, double d);

    int truthValue=0; ///< this is some member variable which will hold a truth value as a number
}
```

The structural commands used to document certain members are:

- \struct
- \union
- \enum
- \fn for function
- \var for variable
- \def for documenting a constant macro
- \typedef for a type definition
- \file
- \namespace
- \package
- \interface
