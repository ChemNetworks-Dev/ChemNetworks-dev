      PROGRAM switchDist

!**********************************************************************
!    The program tracks each water donor H and counts the type of
!    swithcing event pairs (Type A, Type B: Class 1, Type B :Class 2) as
!    they occur sequentially
!
!    This program requires a specific input format (shown below) that is
!    made by combining Type A (a part of the Switching-1-1 file
!    outputted from the corrected lifetime) and Data-with-Class (made
!    from statistic.c) 
!
!
!    last modified: June 22, 2016 by L. Edens
!**********************************************************************
      IMPLICIT NONE
      integer, parameter :: mxLine = 100000
      integer, parameter :: filelen = 50
      integer, parameter :: extlen = 8
      character*(filelen) :: filename, xyzheader, xyzfile
      character*(filelen+extlen) :: outfile1
      character*(extlen) :: str1, str2, fext1 = '.bin'
      character*9 :: chdummy, atmnam 
      integer :: nline, rline, class, mol, atm, nbin, nsnap
      integer :: tempH(3), tempAltH(3),nortempH(3), nortempAltH(3)
      integer, dimension(2) :: snap
      real :: mxHdist,mnHdist, mxAltdist, mnAltdist, Hbin, Altbin
      real :: normxHdist,normnHdist, normxAltdist
      real :: normnAltdist, norHbin, norAltbin
      real, dimension(3) :: avgH, avgAltH, noravgH, noravgAltH
      real, dimension(3,2) :: xyzH, xyzAltH
      real, allocatable :: distH(:,:,:), distAltH(:,:,:)
      real, allocatable :: histH(:,:,:), histAltH(:,:,:)

      character (len=30), dimension(24):: Label
      integer :: i, j, k, l, m, n, ex, dummy
      real :: dx, dy, dz 
!**********************************************************************
!input  file

!input data should be ordered by donor molecule number
!input format:
!  Switch Type | donor molecule | donor | acceptor mol. 1 | acceptor mol. 2 | start1 snap | end1 snap | start2 snap | end2 snap |
!
!Also requires all the original .xyz coordinate files 
!
!Switch type: 0 = Type A
!             1 = TypeB: Class1
!             2 = TypeB: Class2

      !user inputs
      nbin = 20

      mxHdist = 1.5
      mxAltdist = 1.5
      mnHdist = .1
      mnAltdist = .1

      normxHdist = 0.06
      normxAltdist = 0.035
      normnHdist = .001
      normnAltdist = .001

      filename = 'Class-Switching'
      xyzheader = 'water'
      str2 = '.xyz'
      
      open(7,file=filename, iostat=ex)
      if(ex/=0) then
         write(6, '(a)')'Could not open file. Exiting.'
         stop
      endif

      !determine the number of lines for array allocation
      do i=1, mxLine
             read(7,*,end=200)  !figure out how many lines in file
      enddo
200   nline=i-1
      rewind 7
!**********************************************************************
!Allocate and zero arrays

      allocate(distH(nline+1,3,2))
      allocate(distAltH(nline+1,3,2))
      allocate(histH(nbin,3,4))
      allocate(histAltH(nbin,3,4))

      do i=1,3
         do j=1, nline+1
            distH(j,i,1) = 0.0
            distH(j,i,2) = 0.0
            distAltH(j,i,1) = 0.0
            distAltH(j,i,2) = 0.0
         enddo
         do j=1,4
            do k=1,nbin 
               histH(k,i,j) = 0.0
               histAltH(k,i,j) = 0.0
            enddo
         enddo
         do j=1,2
            xyzH(i,j) = 0.0
            xyzAltH(i,j) = 0.0
         enddo
         avgH(i) = 0.0
         avgAltH(i) = 0.0
         noravgH(i) = 0.0
         noravgAltH(i) = 0.0
      enddo
      snap(1) = 0
      snap(2) = 0
      Hbin = 0.0
      Altbin = 0.0

!**********************************************************************
!Main loop

      iloop: do i=1,nline
         !Read in line data and from Class-Switching
         read(7,*,iostat=ex) class,mol,atm,dummy,dummy,dummy,snap(1), &
            snap(2),dummy
         if(ex>0 .OR. atm-1 < 1) then
            write(6,'(a)')'Error with input file'
            stop
         else if(ex<0) then    !exit loop at end of file
            exit iloop
         else
            nsnap = -1
            if(snap(2)-snap(1) > 0) nsnap = snap(2)-snap(1)
            do j=1,2
               !xyz file
               write(str1,'(I8)') snap(j)
               str1 = adjustl(str1)
               xyzfile = trim(xyzheader)//trim(str1)//str2

               !Open xyz file at beginning of break
               open(10,file=xyzfile, iostat=ex)
               if(ex/=0) then
                  write(6, '(a)')'Could not open .xyz file. Exiting.'
                  stop
               endif

               !Use mol to determine correct line to read
               rline = (mol*3)-2 !should be +1 but the loop takes care of it

               !Read in xyz header
               read(10,*,iostat=ex)dummy
               read(10,*,iostat=ex)
               if(ex>0) then
                  write(6,'(a)')'Error with .xyz file read skip'
                  stop
               endif               

               !Read in xyz location for two H atoms
               do k=1,rline
                  read(10,*,iostat=ex) chdummy,dx,dy,dz
                  if(ex>0) then
                     write(6,'(a)')'Error with .xyz file read skip'
                     stop
                  endif 
               enddo
           
               if(atm == 2) then
                  read(10,*,iostat=ex) atmnam,xyzH(1,j),xyzH(2,j), &
                      xyzH(3,j)
                  if(ex>0 .OR. atmnam /= 'H') then
                     write(6,'(a)')'Error with .xyz file read'
                     stop
                  endif
                  read(10,*,iostat=ex) atmnam,xyzAltH(1,j), &
                     xyzAltH(2,j), xyzAltH(3,j)
                  if(ex>0 .OR. atmnam /= 'H') then
                     write(6,'(a)')'Error with .xyz file read'
                     stop
                  endif
               endif

               if(atm == 3) then
                  read(10,*,iostat=ex) atmnam,xyzAltH(1,j), &
                     xyzAltH(2,j), xyzAltH(3,j)
                  if(ex>0 .OR. atmnam /= 'H') then
                     write(6,'(a)')'Error with .xyz file read'
                     stop
                  endif
                  read(10,*,iostat=ex) atmnam,xyzH(1,j),xyzH(2,j), &
                      xyzH(3,j)
                  if(ex>0 .OR. atmnam /= 'H') then
                     write(6,'(a)')'Error with .xyz file read'
                     stop
                  endif
               endif 

               close(10)


            enddo !end j loop 

            !find distance change and store in array

          !distH(nline+1,3,1) and distAltH(nline+1,3,1): 
            !        |TypeA | TypeB: Class1 | TypeB: Class2 |
            !   1    |__1,1_|______1,2______|______1,3______|
            !  ...   |______|_______________|_______________|
            !nline+1 |_Total|_____Total_____|_____Total_____|
          !Note: There will be many zeroes in this array as it is set for max size
          !distH(nline+1,3,2) and distAltH(nline+1,3,2: same as above
          !but normalized by the number of snapshots
 
            !Find ditance change for primary H
            distH(i,class+1,1)=SQRT((xyzH(1,2)-xyzH(1,1))**2 &
                                + (xyzH(2,2)-xyzH(2,1))**2 &
                                + (xyzH(3,2)-xyzH(3,1))**2 )
            distH(nline+1,class+1,1)=distH(nline+1,class+1,1) + 1

            !Average 
            avgH(class+1) = avgH(class+1) + distH(i,class+1,1)

            !Max/min
!            if(distH(i,class+1) > mxHdist) mxHdist = distH(i,class+1)
!            if(distH(i,class+1) < mnHdist .AND. distH(i,class+1) /= 0) then
!               mnHdist = distH(i,class+1)
!            endif

            !Find ditance change for Alt H
            distAltH(i,class+1,1)=SQRT((xyzAltH(1,2)-xyzAltH(1,1))**2 &
                                   + (xyzAltH(2,2)-xyzAltH(2,1))**2 &
                                   + (xyzAltH(3,2)-xyzAltH(3,1))**2 )
            distAltH(nline+1,class+1,1)=distAltH(nline+1,class+1,1) + 1

            !Average
            avgAltH(class+1) = avgAltH(class+1) + distAltH(i,class+1,1)

!            !Max/min
!            if(distAltH(i,class+1) > mxAltdist) then
!                mxAltdist = distAltH(i,class+1)
!            endif
!           if(distAltH(i,class+1) < mnAltdist) then
!               if(distAltH(i,class+1) /= 0) then
!                  mnAltdist = distAltH(i,class+1)
!               endif
!            endif

            !snap normalized distances
            if(nsnap > 0) then
               distH(i,class+1,2) = distH(i,class+1,1) / nsnap
               distH(nline+1,class+1,2)= distH(nline+1,class+1,2) + 1
               noravgH(class+1) = noravgH(class+1) + distH(i,class+1,2)

               distAltH(i,class+1,2) = distAltH(i,class+1,1) / nsnap
               distAltH(nline+1,class+1,2)=distAltH(nline+1,class+1,2)+1
               noravgAltH(class+1) = noravgAltH(class+1) + &
                                     distAltH(i,class+1,2)
            endif


         endif !end read-in
      enddo iloop
      close(7)
!**********************************************************************
!average distances

      do i=1, 3
         avgH(i) = avgH(i)/distH(nline+1,i,1)
         avgAltH(i) = avgAltH(i)/distAltH(nline+1,i,1)
         noravgH(i) = noravgH(i)/distH(nline+1,i,2)
         noravgAltH(i) = noravgAltH(i)/distAltH(nline+1,i,2)
      enddo

!**********************************************************************
!bin distances
      Hbin  = (mxHdist - mnHdist)/nbin
      Altbin = (mxAltdist - mnAltdist)/nbin
      norHbin  = (normxHdist - normnHdist)/nbin
      norAltbin = (normxAltdist - normnAltdist)/nbin

      !fill in histogram distances
      do i=1, nbin
         do j=1, 3
            histH(i,j,1)=((mnHdist+(Hbin*(i-1)))+ &
               (mnHdist+(Hbin*i)))/2.0
            histAltH(i,j,1)=((mnAltdist+(Altbin*(i-1)))+ &
               (mnAltdist+(Altbin*i)))/2.0
            histH(i,j,3)=((normnHdist+(norHbin*(i-1)))+ &
               (normnHdist+(norHbin*i)))/2.0
            histAltH(i,j,3)=((normnAltdist+(norAltbin*(i-1)))+ &
               (normnAltdist+(norAltbin*i)))/2.0
         enddo
      enddo

      !fill in histogram counts
      do i=1, nline !per array line
         do j=1, 3    !per class
            if(distH(i,j,1) /= 0.0) then !skip if zero
               m = 1
               binloop: do k=1, nbin
                  !sort starting at min and escape once count is hit
                  if(distH(i,j,1)<mnHdist+(Hbin*k)) then
                     histH(k,j,2) = histH(k,j,2) + 1.0
                     tempH(j) = tempH(j) + 1
                     m = 0
                     exit binloop    
                  endif 
               enddo binloop
               if( m == 1) then
                  histH(nbin,j,2) = histH(nbin,j,2) + 1.0
                  tempH(j) = tempH(j) + 1
               endif
             endif
            if(distAltH(i,j,1) /= 0.0) then
               n = 1
               binAltloop: do k=1, nbin
                  if(distAltH(i,j,1)<mnAltdist+(Altbin*k)) then
                     histAltH(k,j,2) = histAltH(k,j,2) + 1.0
                     tempAltH(j) = tempAltH(j) + 1
                     n = 0
                     exit binAltloop
                  endif
               enddo binAltloop
               if(n==1) then
                  histAltH(nbin,j,2) = histAltH(nbin,j,2)+1.0
                  tempAltH(j) = tempAltH(j) + 1
               endif
            endif

            !snap normalized
            if(distH(i,j,2) /= 0.0) then !skip if zero
               m = 1
               norbinloop: do k=1, nbin
                  !sort starting at min and escape once count is hit
                  if(distH(i,j,2)<normnHdist+(norHbin*k)) then
                     histH(k,j,4) = histH(k,j,4) + 1.0
                     nortempH(j) = nortempH(j) + 1
                     m = 0
                     exit norbinloop
                  endif
               enddo norbinloop
               if( m == 1) then
                  histH(nbin,j,4) = histH(nbin,j,4) + 1.0
                  nortempH(j) = nortempH(j) + 1
               endif
             endif
            if(distAltH(i,j,2) /= 0.0) then
               n = 1
               norbinAltloop: do k=1, nbin
                  if(distAltH(i,j,2)<normnAltdist+(norAltbin*k)) then
                     histAltH(k,j,4) = histAltH(k,j,4) + 1.0
                     nortempAltH(j) = nortempAltH(j) + 1
                     n = 0
                     exit norbinAltloop
                  endif
               enddo norbinAltloop
               if(n==1) then
                  histAltH(nbin,j,4) = histAltH(nbin,j,4)+1.0
                  nortempAltH(j) = nortempAltH(j) + 1
               endif
            endif

         enddo !per class
      enddo !per array line

      deallocate(distH)
      deallocate(distAltH)
!**********************************************************************
!Labels
      Label(1) = 'Avg TypeA'
      Label(2) = 'Avg TypeB_Class1'
      Label(3) = 'Avg TypeB_Class2'
      Label(4) = 'Avg Alt:TypeA'
      Label(5) = 'Avg Alt:TypeB_Class1'
      Label(6) = 'Avg Alt:TypeB_Class2'

      Label(7) = 'histH Type A'
      Label(8) = 'histH TypeB_Class1'
      Label(9) = 'histH TypeB_Class2'
      Label(10) = 'histAltH Type A'
      Label(11) = 'histAltH TypeB_Class1'
      Label(12) = 'histAltH TypeB_Class2'

      Label(13) = 'norm Avg A'
      Label(14) = 'norm Avg B_C1'
      Label(15) = 'norm Avg B_C2'
      Label(16) = 'norm Avg Alt:A'
      Label(17) = 'norm Avg Alt:B_C1'
      Label(18) = 'norm Avg Alt:B_C2'

      Label(19) =  'norm A'
      Label(20) = 'norm B_C1'
      Label(21) = 'norm B_C2'
      Label(22) = 'norm Alt:A'
      Label(23) = 'norm Alt:B_C1'
      Label(24) = 'norm Alt:B_C2'

!**********************************************************************
!Generate output filename

      j = 1
      k = 1
      m = 0
      n = 0
      !add extension to the user inputted filename
       !this is done by stepping through the filename until a the 
       !first ' ' is found. The extension is then added.
       !trim() is used to to eliminate the ' ' at the end of the file 
      do i=1,(filelen+extlen)
         if(filename(j:j) /= ' ' .AND. j /= filelen) then
            outfile1(i:i) = filename(j:j)
            j = j+1
         else if(k < extlen+1) then
            !found first ' ', add extension
            outfile1(i:i) = fext1(k:k)
            k = k+1
         else
            outfile1(i:i) = ' '
         endif
      enddo
!!**********************************************************************
!Write outputs

      open(8,file=trim(outfile1), status='new')
      k=1
      write(8,'(a)')'switch Distances Output: Distance each primary H &
       &and alternate H moved during an H-bond switch (of the primary)'
400   FORMAT(A21,1X,F8.2)
      do i=1,3
         write(8,400)adjustl(trim(Label(i))), avgH(i) 
      enddo
      write(8,'(/)')
      do i=1,3
         write(8,400)adjustl(trim(Label(i+3))), avgAltH(i)
      enddo
      write(8,'(/)')

500   FORMAT(A17,1X,F8.4)
      do i=1,3
         write(8,500)adjustl(trim(Label(i+12))), noravgH(i)
      enddo
      write(8,'(/)')
      do i=1,3
         write(8,500)adjustl(trim(Label(i+15))), noravgAltH(i)    
      enddo
      write(8,'(/)')


600   FORMAT(A15,3X,A21,2X,A21)
      write(8,600)adjustl(trim(Label(7))), &
         adjustl(trim(Label(8))), adjustl(trim(Label(9)))
         
700   FORMAT(F7.3,2X,F5.1,8X,F7.3,2X,F5.1,8X,F7.3,2X,F5.1)
      do i=1,nbin
         write(8,700)(histH(i,j,1), &
            (histH(i,j,2)/(tempH(j)*1.0))*100.0,j=1,3)
      enddo
      write(8,'(/)')
     
      write(8,600)adjustl(trim(Label(10))), &
         adjustl(trim(Label(11))), adjustl(trim(Label(12)))
      do i=1,nbin
         write(8,700)(histAltH(i,j,1), &
            (histAltH(i,j,2)/(tempAltH(j)*1.0))*100.0,j=1,3)
      enddo
      write(8,'(/)')

800   FORMAT(A10,10X,A13,10X,A13)
      write(8,800)adjustl(trim(Label(19))), &
         adjustl(trim(Label(20))), adjustl(trim(Label(21)))

900   FORMAT(F8.4,2X,F5.1,8X,F8.4,2X,F5.1,8X,F8.4,2X,F5.1)
      do i=1,nbin
         write(8,900)(histH(i,j,3), &
            (histH(i,j,4)/(nortempH(j)*1.0))*100.0,j=1,3)
      enddo
      write(8,'(/)')

      write(8,800)adjustl(trim(Label(22))), &
         adjustl(trim(Label(23))), adjustl(trim(Label(24)))
      do i=1,nbin
         write(8,900)(histAltH(i,j,3), &
            (histAltH(i,j,4)/(nortempAltH(j)*1.0))*100.0,j=1,3)
      enddo

      deallocate(histH)
      deallocate(histAltH)
      close(8)
!***********************************************************************
      END PROGRAM switchDist   
