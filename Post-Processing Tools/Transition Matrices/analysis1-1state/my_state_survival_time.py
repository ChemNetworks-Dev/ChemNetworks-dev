## this code is used for lifetime-analysis based on 3-state-transition-matrix
##   here is for: state-survival-tcf, i.e. how long a state may last

import sys

# here is a class object for one particular event in 3-state-transition-matrix
class Event :
    mol1 = 0
    mol2 = 0
    mol3 = 0
    state1_id = 0
    state1_ti = 0
    state1_tf = 0
    state2_id = 0
    state2_ti = 0
    state2_tf = 0
    state3_id = 0
    state3_ti = 0
    state3_tf = 0

    def __init__(self, id1, id2, id3, k1, t1, t2, k2, t3, t4, k3, t5,t6) :
       self.mol1 = id1
       self.mol2 = id2
       self.mol3 = id3
       self.state1_id = k1
       self.state1_ti = t1
       self.state1_tf = t2
       self.state2_id = k2
       self.state2_ti = t3
       self.state2_tf = t4
       self.state3_id = k3
       self.state3_ti = t5
       self.state3_tf = t6


# here is a class object for the 3-state-transition-matrix
class TSM(Event) :
    data_list = []
    data_number = 0
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    filename = "tsm"

    def __init__(self) :
       self.data_list = []
       self.data_number = 0

    def read_data_from_file(self,fname) :
       print "reading the TSM data from file"
       self.filename = fname
       num = 0
       with open(fname, "r") as fp:
         iline = fp.readline()
         while iline :  # read line by line
           fields = iline.split()
           id1 = int(fields[0])
           id2 = int(fields[1])
           id3 = int(fields[2])
           k1 = int(fields[4])
           t1 = int(fields[5])
           t2 = int(fields[6])
           k2 = int(fields[8])
           t3 = int(fields[9])
           t4 = int(fields[10])
           k3 = int(fields[12])
           t5 = int(fields[13])
           t6 = int(fields[14])
           ievent = Event(id1,id2,id3,k1,t1,t2,k2,t3,t4,k3,t5,t6)
           self.data_list.append( ievent )
           num = num + 1
           #print "ttttt", num, iline, fields, ievent.state1_id, ievent.state1_ti, ievent.state1_tf, ievent
           iline = fp.readline()
       self.data_number = num


# here is a class object for state-survival-time
class SST_from_TSM (Event, TSM) :
    duration = 1
    max_time = 0
    filename = "sst"
    current_TSM = TSM()
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    count_sst = []  # this is the actual surival tcf
    si = 0
    tj = 0
    tt = 0
    norm_factor = 0
    count_event = 0

    def __init__(self, input_TSM, lastsnap, time_range) :
       self.current_TSM = input_TSM
       self.filename = input_TSM.filename
       self.max_time = lastsnap
       self.duration = time_range
       self.count_sst = [0] * time_range

    def calculate_SST(self, id) :
       self.norm_factor = 0
       self.count_event = 0
       for si in range(0, self.duration, 1) :
         self.count_sst[si] = 0  # initialize it every time
       print "calculating SST from TSM ", id
       for si in range(0, self.current_TSM.data_number, 1) :
         ievent = self.current_TSM.data_list[si]
         if ievent.state1_id == id :  # the SST is for a particular state
           self.count_event = self.count_event + 1
           for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
             if tj <= self.max_time :    # this is time-zero for state-survival-distribution
               self.norm_factor = self.norm_factor + 1
               for tt in range(0, self.duration, 1) :
                 if tj + tt <= ievent.state1_tf :
                   self.count_sst[tt] = self.count_sst[tt] + 1
                 else :
                   self.count_sst[tt] = self.count_sst[tt] + 0

    def my_print_SST(self, id) :
       #print "printing information", id
       foutname = self.filename + ".sst." + str(id) 
       with open(foutname, "w") as fp :
         fp.write("state-survival tcf for State {0} :\n".format(id))
         fp.write("norm_factor {0} {1} \n".format(self.count_event, self.norm_factor))
         fp.write("time  count  \n")
         for tt in range(0, self.duration, 1) :
           fp.write("{0} {1}\n".format(tt,self.count_sst[tt]))





# below is the main part of code to be executed

# use sys to read the command-line arguments
if len(sys.argv) != 4 :
  print "use this code by\n {0} TSM-file tcf-duration maximum-snap\n".format(sys.argv[0])
  sys.exit()
else :
  input_file_name = sys.argv[1]
  input_tcf_duration = int(sys.argv[2])
  input_max_snap = int(sys.argv[3])
  print "{0} {1} {2}".format(input_file_name, input_tcf_duration, input_max_snap)

# creating transition-state-matrix
myTSM = TSM()
myTSM.read_data_from_file(input_file_name)

# creating state-survival-time
mysst = SST_from_TSM(myTSM, input_max_snap, input_tcf_duration)
for mi in range(1, 9, 1) :
  mysst.calculate_SST(mi)
  mysst.my_print_SST(mi)





