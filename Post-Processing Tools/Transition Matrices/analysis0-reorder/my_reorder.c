
// this code is re-ordering the information of "result" file after Transition State Theory

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#define FLN 1000

struct line_info
{
	int mol1;
	int mol2;
	int mol3;
	int index1;
	int snap11;
	int snap12;
	int index2;
	int snap21;
	int snap22;
	int index3;
	int snap31;
	int snap32;
};
typedef struct line_info Info;

int main(int argc, char *argv[])
{
	char filename[FLN];
	FILE *fip,*fop;
	Info *Allinfo;
	int totline,iline,jline;
	int tmol1,tmol2,tmol3,tindex1,tsnap11,tsnap12,tindex2,tsnap21,tsnap22,tindex3,tsnap31,tsnap32;
	int *checked;
	char tc1,tc2,tc3,tc4,tc5;
	char buffer[FLN];
	Info i_info, j_info, k_info;

	if(argc != 2)
	{
		printf("Use this code:\n  %s result-file\n\n",argv[0]);
		exit(-1);
	}
	sprintf(filename,"%s",argv[1]);
	fip = fopen(filename,"r");
	if(fip == NULL)
	{
		printf("Error: cannot find %s\n",filename);
		exit(-2);
	}
	else
	{
		rewind(fip);
		totline = 0;
		while(fgets(buffer,sizeof(buffer),fip) != NULL)
		{
			totline++;
		}

		Allinfo = (Info *)calloc(totline+1, sizeof(Info));  // allocate memory
		checked = (int *)calloc(totline+1, sizeof(int));
		rewind(fip);
		iline = 0;
		while(fscanf(fip,"%d %d %d %c %d %d %d %c%c %d %d %d %c%c %d %d %d",&tmol1,&tmol2,&tmol3,&tc1,&tindex1,&tsnap11,&tsnap12,&tc2,&tc3,&tindex2,&tsnap21,&tsnap22,&tc4,&tc5,&tindex3,&tsnap31,&tsnap32) == 17)
		{
			if(fgets(buffer,sizeof(buffer),fip) == NULL)
				break;
			iline++;
			Allinfo[iline] = (Info) {tmol1,tmol2,tmol3,tindex1,tsnap11,tsnap12,tindex2,tsnap21,tsnap22,tindex3,tsnap31,tsnap32};
		//	printf("%d %d %d %c %d %d %d %c%c %d %d %d %c%c %d %d %d\n",tmol1,tmol2,tmol3,tc1,tindex1,tsnap11,tsnap12,tc2,tc3,tindex2,tsnap21,tsnap22,tc4,tc5,tindex3,tsnap31,tsnap32);
		}
		fclose(fip);

		sprintf(filename,"%s.reordered",argv[1]);
		fop = fopen(filename,"w");

		for(iline=1; iline<=totline; iline++)
		{
			printf("line %d of %d, %.2f completed\n",iline,totline,(iline+0.0)/(totline+0.0)*100.0);
			if(checked[iline] == 0)
			{
				i_info = Allinfo[iline];
				tmol1 = i_info.mol1;
				tmol2 = i_info.mol2;
				tmol3 = i_info.mol3;

				k_info = i_info;
				for(jline = iline+1; jline<=totline; jline++)
				{
					if(checked[jline] == 0)
					{
						j_info = Allinfo[jline];
						if(j_info.mol1 == k_info.mol1 && j_info.mol2 == k_info.mol2 && j_info.mol3 == k_info.mol3)
						{
							if(k_info.index2 == j_info.index1 && k_info.snap21 == j_info.snap11 && k_info.snap22 == j_info.snap12 && k_info.index3 == j_info.index2 && k_info.snap31 == j_info.snap21)
							{
								fprintf(fop,"%d %d %d : %d %d %d -> %d %d %d -> %d %d %d\n",k_info.mol1, k_info.mol2, k_info.mol3, k_info.index1, k_info.snap11, k_info.snap12, k_info.index2, k_info.snap21, k_info.snap22, j_info.index2, j_info.snap21, j_info.snap22);
							}
							else
							{
								fprintf(fop,"%d %d %d : %d %d %d -> %d %d %d -> %d %d %d\n",k_info.mol1, k_info.mol2, k_info.mol3, k_info.index1, k_info.snap11, k_info.snap12, k_info.index2, k_info.snap21, k_info.snap22, k_info.index3, k_info.snap31, k_info.snap32);
							}
							k_info = j_info;
							checked[jline] = 1;
						}
					}
				}
				fprintf(fop,"%d %d %d : %d %d %d -> %d %d %d -> %d %d %d\n",k_info.mol1, k_info.mol2, k_info.mol3, k_info.index1, k_info.snap11, k_info.snap12, k_info.index2, k_info.snap21, k_info.snap22, k_info.index3, k_info.snap31, k_info.snap32);
				checked[iline] = 1;
			}
		}
		fclose(fop);

		free(Allinfo);
		free(checked);
	}

	return(0);
}


