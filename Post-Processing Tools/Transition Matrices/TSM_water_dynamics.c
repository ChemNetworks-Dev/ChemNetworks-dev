
/* here is the code for calculating water rotation dynamics from transition state matrix (TSM)
 *    rotation states are defined based on hydrogen-bond connectivity of arbitary 3-waters (within 2nd shell of each other)
 *    the transition state matrix is construct based on initial-state, intermediate-state, final-state
 *  by Tiecheng Zhou, 2017.09.05
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#define Cut_dist 5.51  // the 2nd minimum in RDF, from [J. Chem. Phys., Vol. 113, No. 20, 22 November 2000]
#define FLN 1000

struct STATE {
	int index;  // the state index of 3 water molecules
	int mol_center;  // the id of center rotating water
	int mol_left;   // the molecule on the left side of rotating water, possible initial HB partner
	int mol_right;  // on the right side, possible final HB partner
	int snap_begin;  // the begin snapshot of this state
	int snap_end;  // the end snapshot of this state
};


struct TRANSITION {    // this is define a transition state
	struct STATE initial;
	struct STATE intermediate;
	struct STATE final;
};


double pbcshift(double inp, double ref, double Size); // consider PBC of the box
int my_read_dist(int snapshot, int molecule1, int array_mol1_shell[], double Xsize, double Ysize, double Zsize); // get the 2nd solvation shell of water molecule1
double my_get_dist23(int snapshot, int molecule2, int molecule3, double Xsize, double Ysize, double Zsize); // get the distance from molecule2 and molecule3
int my_read_graph(int snapshot, int molecule1, int molecule2, int molecule3, int matrix[3][3]); // get the 3-water graph
int my_find_state(int matrix[3][3]);  // determine the 3-water states, based on their connectivity graph


int main(int argc, char *argv[])
{
	int Nwaters;
	int Nsnaps;
	double Xsize,Ysize,Zsize;

	if(argc != 6)
	{
		printf("Use this code as:\n  %s Nwaters Nsnaps Xsize Ysize Zsize\n",argv[0]);
		exit(-1);
	}

	Nwaters = atoi(argv[1]);
	Nsnaps = atoi(argv[2]);
	Xsize = atof(argv[3]);
	Ysize = atof(argv[4]);
	Zsize = atof(argv[5]);
	printf(" You entered Nwaters %d , Nsnaps %d , Xsize %f , Ysize %f , Zsize %f\n",Nwaters,Nsnaps,Xsize,Ysize,Zsize);

	FILE *fout,*ftm;
	fout = fopen("result","w");
	ftm = fopen("result.TM","w");

	int mol1,mol2,mol3; // the 3-water molecules
	int snap,snap1,snap2;
	int array_mol1_shell[Nwaters+1]; // to indicate the 2nd solvation shell of mol1
	int current_state_id,before_state_id;
	int sym_dist,sym_graph;
	struct STATE state_now,state_before,state_after;
	int graph_now[3][3];  // this is 3*3 matrix indicating the HB connectivity 
	long count[9][9][9];  // this is the counting of transition events, three index are initial_state, intermediate_state, final_state
	int i,j,k;
	double dist23;
	struct STATE Allstates_now[Nwaters+1][Nwaters+1][Nwaters+1], Allstates_before[Nwaters+1][Nwaters+1][Nwaters+1], Allstates_after[Nwaters+1][Nwaters+1][Nwaters+1]; // this is to keep record of the states for every 3-waters

	for(i=0;i<=8;i++)
		for(j=0;j<=8;j++)
			for(k=0;k<=8;k++)
				count[i][j][k] = 0;
	for(i=0;i<=Nwaters;i++)
		for(j=0;j<=Nwaters;j++)
			for(k=0;k<=Nwaters;k++)
				Allstates_now[i][j][k] = Allstates_before[i][j][k] = Allstates_after[i][j][k] = (struct STATE) {0,i,j,k,0,0};   // initialize the STATES for every 3-waters

	sym_dist = sym_graph = 0;

	// here I changed the order of loop, such that it first goes over snapshots. This is because: if we loop over 3-water molecules first, it will spend a huge amount of time looping over 3-far-apart molecules, which we are not interested
	for(snap=1; snap<= Nsnaps; snap++)
	{
		printf("Processing snapshot %d\n",snap);
		for(mol1=1; mol1<=Nwaters; mol1++)
		{
			for(i=0;i<=Nwaters;i++)
				array_mol1_shell[i] = 0;

			sym_dist = my_read_dist(snap,mol1,array_mol1_shell,Xsize,Ysize,Zsize); // this function is changed to read the 2nd solvation shell of molecule1, i.e. the center rotating water
			if(sym_dist != 0)
				break;

			for(i=1; i<=array_mol1_shell[0]; i++)     // here I pick the other 2 water molecules from 2nd solvation shell of molecule1
			{
				mol2 = array_mol1_shell[i];
				for(j=1; j<=array_mol1_shell[0]; j++)
				{
					mol3 = array_mol1_shell[j];
					dist23 = 0.0;
					if(j != i)
					{
						dist23 = my_get_dist23(snap,mol2,mol3,Xsize,Ysize,Zsize);  // calculate the distance between molecule2 and molecule3
						if(dist23 > Xsize+Ysize+Zsize) // there is error in this distance calculation
							break;
					}

					if(j != i && dist23 <= Cut_dist)
					{
			//			printf(" snap %d, waters %d %d %d\n",snap,mol1,mol2,mol3);

						sym_graph = my_read_graph(snap,mol1,mol2,mol3,graph_now);
						if(sym_graph != 0)
							break;

						current_state_id = my_find_state(graph_now);   // determine the connectivty state

						if(snap == 1)
						{
							before_state_id = 0;
							Allstates_now[mol1][mol2][mol3] = (struct STATE) {current_state_id, mol1, mol2, mol3, 1, 1};
						}
						else
						{
							before_state_id = Allstates_now[mol1][mol2][mol3].index;   // before_state_id is the state-index of this 3-waters at the previous snapshot

							if(current_state_id == before_state_id)   // no change in the state-index
							{
								Allstates_now[mol1][mol2][mol3].snap_end = snap;
							}
							else   // there is change in the state-index, i.e. the 3-water undergoes some state-transition
							{
								if(Allstates_before[mol1][mol2][mol3].snap_end == 0)   // before-state has not been assigned
								{
									Allstates_before[mol1][mol2][mol3] = Allstates_now[mol1][mol2][mol3];
									Allstates_now[mol1][mol2][mol3] = (struct STATE) {current_state_id, mol1, mol2, mol3, snap, snap};
								}
								else
								{
									state_before = Allstates_before[mol1][mol2][mol3];
									state_now = Allstates_now[mol1][mol2][mol3];
									state_after = (struct STATE) {current_state_id, mol1, mol2, mol3, snap, snap};

									fprintf(fout, " %d %d %d : ", mol1, mol2, mol3);
									fprintf(fout, "%d %d %d -> ",state_before.index, state_before.snap_begin, state_before.snap_end);
									fprintf(fout, "%d %d %d -> ",state_now.index, state_now.snap_begin, state_now.snap_end);
									fprintf(fout, "%d %d %d \n",current_state_id, snap, snap);

									count[state_before.index][state_now.index][state_after.index] += 1;

									Allstates_before[mol1][mol2][mol3] = state_now;
									Allstates_now[mol1][mol2][mol3] = state_after;
								}
		
							}
						}

					}  // end of " if(j != i) "

				}  // end of " for(j=1; j<=array_mol1_shell[0]; j++) "

				if(sym_graph != 0 || dist23 > Xsize+Ysize+Zsize)
					break;

			}  // end of " for(i=1; i<=array_mol1_shell[0]; i++) "

			if(sym_graph != 0 || dist23 > Xsize+Ysize+Zsize)
				break;

		} // end of " for(mol1=1; mol1<=Nwaters; mol1++) "

		if(sym_dist != 0 || sym_graph != 0 || dist23 > Xsize+Ysize+Zsize)
			break;
	}  // end of " for(snap=1; snap<= Nsnaps; snap++) "

	
	for(k=1;k<=8;k++)  // this is the intermediate state
	{
		fprintf(ftm, "\ntransition matrix through intermediate state %d\n\n",k);
		for(i=1;i<=8;i++)
		{
			for(j=1;j<=8;j++)
				fprintf(ftm, "%d ",count[i][k][j]);
			fprintf(ftm, "\n");
		}
		fprintf(ftm, "\n");
	}	

	fclose(fout);
	fclose(ftm);
	return(0);
}

int my_read_dist(int snapshot, int molecule1, int array_mol1_shell[], double Xsize, double Ysize, double Zsize)
{
	FILE *fip;
	char filename[FLN];
	char buffer[FLN];
	char *token;
	int Numatoms,line, dummy, num;
	double *Coord_atoms;
	double O1x,O1y,O1z,O2x,O2y,O2z,distance;
	int result;

	sprintf(filename,"water%d.xyz",snapshot);
	fip = fopen(filename,"r");
	if(fip == NULL)
	{
		printf("Error: in func 'my_read_dist', cannot find file water%d.xyz \n",snapshot);
		result = -1;
	}
	else
	{
	
		rewind(fip);
		fscanf(fip,"%d",&Numatoms);
		Coord_atoms = (double *) calloc(Numatoms*3,sizeof(double));   // atom coordinates starting from 0 to Numatoms*3-1, i.e 0,1,2 is oxygen, 3,4,5 is hydrogen...

		if(molecule1 > Numatoms/3)
		{
			printf("Error: in func 'my_read_dist', not enough water molecules in water%d.xyz, wanted %d \n",snapshot,molecule1);
			fclose(fip);
			result = -2;
		}
		else
		{
			fgets(buffer,sizeof(buffer),fip);
			fgets(buffer,sizeof(buffer),fip);  // skip the first 2 lines from xyz file
	
			for(line=1;line<=Numatoms;line++)
			{
				if(fgets(buffer,sizeof(buffer),fip) == NULL)
					break;
				token = strtok(buffer," \n");
				token = strtok(NULL," \n");
				Coord_atoms[line*3-3] = atof(token);
				token = strtok(NULL," \n");
				Coord_atoms[line*3-2] = atof(token);
				token = strtok(NULL," \n");
				Coord_atoms[line*3-1] = atof(token);
			}
			fclose(fip);

			if(line != Numatoms+1)
			{
				printf("Error: in func 'my_read_dist', reading coordinates error, file water%d.xyz\n",snapshot);
				result = -3;
			}
			else
			{
				O1x = Coord_atoms[molecule1*9-9];  // molecule1, its oxygen coordinates, if molecule1=1, should get atom coordinates 0,1,2
				O1y = Coord_atoms[molecule1*9-8];
				O1z = Coord_atoms[molecule1*9-7];
	
				num = 0; // the number of water molecules in 2nd shell of water molecule1
				for(dummy=1; dummy<= Numatoms/3; dummy++)
				{
					if(dummy != molecule1)
					{
						O2x = Coord_atoms[dummy*9-9];
						O2y = Coord_atoms[dummy*9-8];
						O2z = Coord_atoms[dummy*9-7];

						O2x = pbcshift(O2x,O1x,Xsize);  // consider PBC of the box before calculating the distance
						O2y = pbcshift(O2y,O1y,Ysize);
						O2z = pbcshift(O2z,O1z,Zsize);
						distance = sqrt((O2x-O1x)*(O2x-O1x)+(O2y-O1y)*(O2y-O1y)+(O2z-O1z)*(O2z-O1z));

						if(distance <= Cut_dist)
						{
							num++;
							array_mol1_shell[num] = dummy;   // the element in array_mol1_shell keeps the molecule index
						}
					}
				}

				array_mol1_shell[0] = num;   // the 0 element in array_mol1_shell keeps the number of water molecules in 2nd shell of the center water

				result = 0;
			}

		}
		free(Coord_atoms);
	}

	return(result);
}

double my_get_dist23(int snapshot, int molecule2, int molecule3, double Xsize, double Ysize, double Zsize)
{
	FILE *fip;
	char filename[FLN];
	char buffer[FLN];
	char *token;
	int Numatoms,line;
	double *Coord_atoms;
	double O1x,O1y,O1z,O2x,O2y,O2z,distance;

	sprintf(filename,"water%d.xyz",snapshot);
	fip = fopen(filename,"r");
	if(fip == NULL)
	{
		printf("Error: in func 'my_read_dist', cannot find file water%d.xyz \n",snapshot);
		distance =  Xsize+Ysize+Xsize+1.0;
	}
	else
	{
		rewind(fip);
		fscanf(fip,"%d",&Numatoms);
		Coord_atoms = (double *) calloc(Numatoms*3,sizeof(double));   // atom coordinates starting from 0 to Numatoms*3-1, i.e 0,1,2 is oxygen, 3,4,5 is hydrogen...

		if(molecule2 > Numatoms/3 || molecule3 > Numatoms/3)
		{
			printf("Error: in func 'my_read_dist', not enough water molecules in water%d.xyz, wanted %d %d \n",snapshot,molecule2,molecule3);
			fclose(fip);
			distance =  Xsize+Ysize+Zsize+2.0;
		}
		else
		{
			fgets(buffer,sizeof(buffer),fip);
			fgets(buffer,sizeof(buffer),fip);  // skip the first 2 lines from xyz file

			for(line=1;line<=Numatoms;line++)
			{
				if(fgets(buffer,sizeof(buffer),fip) == NULL)
					break;
				token = strtok(buffer," \n");
				token = strtok(NULL," \n");
				Coord_atoms[line*3-3] = atof(token);
				token = strtok(NULL," \n");
				Coord_atoms[line*3-2] = atof(token);
				token = strtok(NULL," \n");
				Coord_atoms[line*3-1] = atof(token);
			}
			fclose(fip);

			if(line != Numatoms+1)
			{
				printf("Error: in func 'my_read_dist', reading coordinates error, file water%d.xyz\n",snapshot);
				distance = Xsize+Ysize+Zsize+3.0;
			}
			else
			{
				O1x = Coord_atoms[molecule2*9-9];  // molecule1, its oxygen coordinates, if molecule1=1, should get atom coordinates 0,1,2
				O1y = Coord_atoms[molecule2*9-8];
				O1z = Coord_atoms[molecule2*9-7];
			
				O2x = Coord_atoms[molecule3*9-9];
				O2y = Coord_atoms[molecule3*9-8];
				O2z = Coord_atoms[molecule3*9-7];

				O2x = pbcshift(O2x,O1x,Xsize);  // consider PBC of the box before calculating the distance
				O2y = pbcshift(O2y,O1y,Ysize);
				O2z = pbcshift(O2z,O1z,Zsize);
				distance = sqrt((O2x-O1x)*(O2x-O1x)+(O2y-O1y)*(O2y-O1y)+(O2z-O1z)*(O2z-O1z));
			}

		}
		free(Coord_atoms);
	}

	return(distance);

}

int my_read_graph(int snapshot, int molecule1, int molecule2, int molecule3, int matrix[3][3])
{
	FILE *fpp;
	char filename[FLN];
	int id1,id2,atm1,atm2,pbcx,pbcy,pbcz;
	char buffer[FLN];
	int result;

	sprintf(filename,"Input.water%d.xyz.water%d.xyz.GraphGeod",snapshot,snapshot);
	fpp = fopen(filename,"r");
	if( fpp == NULL )
	{
		printf("Error: in func 'my_read_graph', cannot find GraphGeod file at snapshot %d\n",snapshot);
		result =  -1;
	}
	else
	{
		for(id1=0;id1<3;id1++)
			for(id2=0;id2<3;id2++)
				matrix[id1][id2] = 0;

		rewind(fpp);
		while( (fscanf(fpp,"%d %d %d %d %d %d %d",&id1,&id2,&pbcx,&pbcy,&pbcz,&atm1,&atm2)) == 7)
		{
			if(fgets(buffer,sizeof(buffer),fpp) == NULL)
				break;

			if(id1 == molecule1 && id2 == molecule2)
			{
			//	if(atm1 == 2 || atm1 == 3)   // consider only when molecule1 (the center rotating water) is donor, 2017.09.26, not used for O..O definition
					matrix[0][1] = matrix[1][0] = 1;  // regard as undirected graph, this can be changed later for directed graph
			}

			if(id1 == molecule1 && id2 == molecule3)
			{
			//	if(atm1 == 2 || atm1 == 3)
					matrix[0][2] = matrix[2][0] = 1;
			}

			if(id1 == molecule2 && id2 == molecule1)
			{
			//	if(atm2 == 2 || atm2 == 3)
					matrix[0][1] = matrix[1][0] = 1;
			}

			if(id1 == molecule2 && id2 == molecule3)
			{
				matrix[1][2] = matrix[2][1] = 1;  // for HBs between molecule2 and molecule3, no restriction on which one is donor/acceptor
			}
	
			if(id1 == molecule3 && id2 == molecule1)
			{
			//	if(atm2 == 2 || atm2 == 3)
					matrix[0][2] = matrix[2][0] = 1;
			}

			if(id1 == molecule3 && id2 == molecule2)
			{
				matrix[1][2] = matrix[2][1] = 1;
			}

		}
		result = 0;
	}

	fclose(fpp);
	return(result);
}

int my_find_state(int matrix[3][3])
{
	int result=0;  // there are only 8 possible states if we treat undirected graph for 3-waters

	if( matrix[0][1] == 0 && matrix[0][2] == 0 && matrix[1][2] == 0 )  // regard it as undirected graph, symmetric matrix
		result = 1;    // state-1 is three waters are not connected
	if( matrix[0][1] == 0 && matrix[0][2] == 1 && matrix[1][2] == 0 )
		result = 2;   // state-2 is only water1 is connected to water3
	if( matrix[0][1] == 0 && matrix[0][2] == 0 && matrix[1][2] == 1 )
		result = 3;   // state-3 is only water2 is connected to water3
	if( matrix[0][1] == 0 && matrix[0][2] == 1 && matrix[1][2] == 1 )
		result = 4;   // state-4 is: water1-water3, water2-water3 connected
	if( matrix[0][1] == 1 && matrix[0][2] == 0 && matrix[1][2] == 0 )
		result = 5;   // state-5 is only water1 is connected to water6
	if( matrix[0][1] == 1 && matrix[0][2] == 1 && matrix[1][2] == 0 )
		result = 6;   // state-6 is: water1-water2, water1-water3 connected
	if( matrix[0][1] == 1 && matrix[0][2] == 0 && matrix[1][2] == 1 )
		result = 7;   // state-7 is: water1-water2, water2-water3 connected
	if( matrix[0][1] == 1 && matrix[0][2] == 1 && matrix[1][2] == 1 )
		result = 8;   // state-8 is: water1-water2, water1-water3, water2-water3 connected

	return(result);
}

double pbcshift(double inp, double ref, double Size)
{
	double result;
	if(inp - ref > Size/2.0)
		result = inp - Size;
	else if(inp - ref < -0.5*Size)
		result = inp + Size;
	else
		result = inp;

	return(result);
}



