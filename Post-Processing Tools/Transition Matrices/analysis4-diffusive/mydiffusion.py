## this code is used for diffusive-motion-analysis based on 3-state-transition-matrix
##   calculating the three distances

import sys

# here is a class object for one particular event in 3-state-transition-matrix
class Event :
    mol1 = 0
    mol2 = 0
    mol3 = 0
    state1_id = 0
    state1_ti = 0
    state1_tf = 0
    state2_id = 0
    state2_ti = 0
    state2_tf = 0
    state3_id = 0
    state3_ti = 0
    state3_tf = 0

    def __init__(self, id1, id2, id3, k1, t1, t2, k2, t3, t4, k3, t5,t6) :
       self.mol1 = id1
       self.mol2 = id2
       self.mol3 = id3
       self.state1_id = k1
       self.state1_ti = t1
       self.state1_tf = t2
       self.state2_id = k2
       self.state2_ti = t3
       self.state2_tf = t4
       self.state3_id = k3
       self.state3_ti = t5
       self.state3_tf = t6

    # this function is to label an event if it is associated with state1-state2 stable transition
    def checkstable(self, state1, state2, cutoff_duration) :
       result = 0
       if(self.state1_id == state1 and self.state2_id == state2) :  # stable transition without intermediate
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state2_tf - self.state2_ti >= cutoff_duration) :
           result = 1
       if(self.state1_id == state1 and self.state3_id == state2) :  # stable transition via an intermediate-state
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state3_tf - self.state3_ti >= cutoff_duration) :
           result = 2
       return result

    # this is to check whether an event is subject to absorbing bondary condition, i.e. only the first transition is subject to abc
    def checkabc(self, state1, state2, stable_tag, max_gap) : 
       out = 0
       if(stable_tag == 1) :  # this stable_tag should be obtained from function "checkstable"
         if(self.state2_ti - self.state1_tf <= max_gap) :   # max_gap is the maximum gap-duration to regard there is no other transitions in between
           out = 1    # out = 1 means it is subject to abc
         else :
           out = 2    # out = 2 means it is stable-transition, yet not subject to abc, i.e. there are some steps in between
       elif (stable_tag == 2) :
         if(self.state3_ti - self.state1_tf <= max_gap) :
           out = 1
         else :
           out = 2
       else :
         out = 0
       return out


# here is a class object for the 3-state-transition-matrix
class TSM(Event) :
    data_list = []
    data_number = 0
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    filename = "tsm"

    def __init__(self) :
       self.data_list = []
       self.data_number = 0

    def read_data_from_file(self,fname) :
       print "reading the TSM data from file"
       self.filename = fname
       num = 0
       with open(fname, "r") as fp:
         iline = fp.readline()
         while iline :  # read line by line
           fields = iline.split()
           id1 = int(fields[0])
           id2 = int(fields[1])
           id3 = int(fields[2])
           k1 = int(fields[4])
           t1 = int(fields[5])
           t2 = int(fields[6])
           k2 = int(fields[8])
           t3 = int(fields[9])
           t4 = int(fields[10])
           k3 = int(fields[12])
           t5 = int(fields[13])
           t6 = int(fields[14])
           ievent = Event(id1,id2,id3,k1,t1,t2,k2,t3,t4,k3,t5,t6)
           self.data_list.append( ievent )
           num = num + 1
           #print "ttttt", num, iline, fields, ievent.state1_id, ievent.state1_ti, ievent.state1_tf, ievent
           iline = fp.readline()
       self.data_number = num


# here is a class object for the atomic coordinates
class Coords :
    num_atoms = 0
    Atomlist = []

    def __init__(self) :
       num_atoms = 1
       Atomlist = [["O", 0.0, 0.0, 0.0]]

    def read_xyz(self, snapid) :
       self.num_atoms = 0
       self.Atomlist = []
       out = 0
       fname = "water" + str(snapid) + ".xyz"
       try :
         fp = open(fname, "r")
       except IOError :
         print "Error, cannot find the file " + fname
         out = 1
       if out == 0 :
         iline = fp.readline()
         iline_split = iline.split()
         self.num_atoms = int(iline_split[0])
         iline = fp.readline()  # read the empty line
         for k in range(0, self.num_atoms, 1) :
           iline = fp.readline()
           iline_split = iline.split()
           if len(iline_split) == 4 :
             iline_data = [iline_split[0], float(iline_split[1]), float(iline_split[2]), float(iline_split[3])]
             self.Atomlist.append(iline_data)
           else :
             print "Error, reading the coordinates " + fname
             out = 2
             break             
         fp.close()
       return out


    # to calculate the distance between three water molecules, I'm using oxygen positions
    def get_distance(self, wat1, wat2, wat3, Boxsize) : 
       O1x = self.Atomlist[(wat1-1)*3][1]
       O1y = self.Atomlist[(wat1-1)*3][2]
       O1z = self.Atomlist[(wat1-1)*3][3]
       O2x = self.Atomlist[(wat2-1)*3][1]
       O2y = self.Atomlist[(wat2-1)*3][2]
       O2z = self.Atomlist[(wat2-1)*3][3]
       O3x = self.Atomlist[(wat3-1)*3][1]
       O3y = self.Atomlist[(wat3-1)*3][2]
       O3z = self.Atomlist[(wat3-1)*3][3]

       distx = O1x - O2x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O1y - O2y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O1z - O2z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist12 = (distx * distx + disty * disty + distz * distz)**(0.5)

       distx = O1x - O3x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O1y - O3y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O1z - O3z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist13 = (distx * distx + disty * disty + distz * distz)**(0.5)

       distx = O2x - O3x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O2y - O3y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O2z - O3z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist23 = (distx * distx + disty * disty + distz * distz)**(0.5)

       return (dist12, dist13, dist23)



# here is a class object for diffusive-motion among stable-state transition
class SSdiff_from_TSM (Coords, Event, TSM) :
    current_TSM = TSM()    
    cut_lifetime = 1
    max_gap = 1
    pbc_box_size = 0.0
    count_event = 0
    event_list = []

    def __init__(self, input_TSM, lifetime_cutoff, gap_cutoff, pbc_box) :
       self.current_TSM = input_TSM
       self.cut_lifetime = lifetime_cutoff
       self.max_gap = gap_cutoff
       self.pbc_box_size = pbc_box


    def calculate_diffusive_motion(self, stateA, stateB) :  # calculate the distance between three waters in stateA and in stateB
       print "calculating the diffusive separtions for ", stateA, " to ", stateB
       self.count_event = 0
       self.event_list = []
       for si in range(0, self.current_TSM.data_number, 1) :
         ievent = self.current_TSM.data_list[si]
         stable_tag = ievent.checkstable(stateA, stateB, self.cut_lifetime)
         abc_tag = ievent.checkabc(stateA, stateB, stable_tag, self.max_gap)
         if( (stable_tag == 1 or stable_tag == 2) and abc_tag == 1 ) :  # this is to select stable-stable transitions & subject to abc
           water1 = ievent.mol1
           water2 = ievent.mol2
           water3 = ievent.mol3
           if( stable_tag == 1 ) :    # stable-stable transition, without an intermediate
             self.count_event = self.count_event + 1
             myevent_dist_data = []
             myevent_dist_data.append(si)  # keep record the event-id from TSM-input-file
             myevent_dist_data.append(stable_tag)
             avg_dist12 = 0.0
             avg_dist13 = 0.0
             avg_dist23 = 0.0
             num_avg = 0.0
             err_tag = 0
             for ti in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               temp_coords = Coords()
               err_tag = temp_coords.read_xyz(ti)
               if err_tag == 0 :
                 num_avg = num_avg + 1.0
                 (dist12, dist13, dist23) = temp_coords.get_distance(water1, water2, water3, self.pbc_box_size)
                 avg_dist12 = avg_dist12 + dist12
                 avg_dist13 = avg_dist13 + dist13
                 avg_dist23 = avg_dist23 + dist23
               else :
                 break
             if err_tag == 0 :
               avg_dist12 = avg_dist12 / num_avg   # here is the distance between water1 and water2 during initial-state "state1"
               avg_dist13 = avg_dist13 / num_avg
               avg_dist23 = avg_dist23 / num_avg
               myevent_dist_data.append(avg_dist12) 
               myevent_dist_data.append(avg_dist13) 
               myevent_dist_data.append(avg_dist23) 
             avg_dist12 = 0.0
             avg_dist13 = 0.0
             avg_dist23 = 0.0
             num_avg = 0.0
             err_tag = 0
             for ti in range(ievent.state2_ti, ievent.state2_tf+1, 1) :
               temp_coords = Coords()
               err_tag = temp_coords.read_xyz(ti)
               if err_tag == 0 :
                 num_avg = num_avg + 1.0
                 (dist12, dist13, dist23) = temp_coords.get_distance(water1, water2, water3, self.pbc_box_size)
                 avg_dist12 = avg_dist12 + dist12
                 avg_dist13 = avg_dist13 + dist13
                 avg_dist23 = avg_dist23 + dist23
               else :
                 break
             if err_tag == 0 :
               avg_dist12 = avg_dist12 / num_avg   # here is the distance between water1 and water2 during final-state "state2"
               avg_dist13 = avg_dist13 / num_avg
               avg_dist23 = avg_dist23 / num_avg
               myevent_dist_data.append(avg_dist12) 
               myevent_dist_data.append(avg_dist13) 
               myevent_dist_data.append(avg_dist23) 
             self.event_list.append(myevent_dist_data)   # keep record this event

           elif( stable_tag == 2) :    # stable-stable transition, with an intermediate
             self.count_event = self.count_event + 1
             myevent_dist_data = []
             myevent_dist_data.append(si)  # keep record the event-id from TSM-input-file
             myevent_dist_data.append(stable_tag)
             avg_dist12 = 0.0
             avg_dist13 = 0.0
             avg_dist23 = 0.0
             num_avg = 0.0
             err_tag = 0
             for ti in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               temp_coords = Coords()
               err_tag = temp_coords.read_xyz(ti)
               if err_tag == 0 :
                 num_avg = num_avg + 1.0
                 (dist12, dist13, dist23) = temp_coords.get_distance(water1, water2, water3, self.pbc_box_size)
                 avg_dist12 = avg_dist12 + dist12
                 avg_dist13 = avg_dist13 + dist13
                 avg_dist23 = avg_dist23 + dist23
               else :
                 break
             if err_tag == 0 :
               avg_dist12 = avg_dist12 / num_avg   # here is the distance between water1 and water2 during initial-state "state1"
               avg_dist13 = avg_dist13 / num_avg
               avg_dist23 = avg_dist23 / num_avg
               myevent_dist_data.append(avg_dist12) 
               myevent_dist_data.append(avg_dist13) 
               myevent_dist_data.append(avg_dist23) 
             avg_dist12 = 0.0
             avg_dist13 = 0.0
             avg_dist23 = 0.0
             num_avg = 0.0
             err_tag = 0
             for ti in range(ievent.state3_ti, ievent.state3_tf+1, 1) :
               temp_coords = Coords()
               err_tag = temp_coords.read_xyz(ti)
               if err_tag == 0 :
                 num_avg = num_avg + 1.0
                 (dist12, dist13, dist23) = temp_coords.get_distance(water1, water2, water3, self.pbc_box_size)
                 avg_dist12 = avg_dist12 + dist12
                 avg_dist13 = avg_dist13 + dist13
                 avg_dist23 = avg_dist23 + dist23
               else :
                 break
             if err_tag == 0 :
               avg_dist12 = avg_dist12 / num_avg   # here is the distance between water1 and water2 during final-state "state3"
               avg_dist13 = avg_dist13 / num_avg
               avg_dist23 = avg_dist23 / num_avg
               myevent_dist_data.append(avg_dist12) 
               myevent_dist_data.append(avg_dist13) 
               myevent_dist_data.append(avg_dist23) 
             self.event_list.append(myevent_dist_data)   # keep record this event


    def my_print_diff(self, stateA, stateB) :
       Adist12_avg_events = 0.0  # to get the average dist12 at stateA over all events
       Adist13_avg_events = 0.0
       Adist23_avg_events = 0.0
       Bdist12_avg_events = 0.0
       Bdist13_avg_events = 0.0
       Bdist23_avg_events = 0.0
       if self.count_event > 0 :
         for mi in range(0, self.count_event, 1) :
           Adist12_avg_events = Adist12_avg_events + self.event_list[mi][2]
           Adist13_avg_events = Adist13_avg_events + self.event_list[mi][3]
           Adist23_avg_events = Adist23_avg_events + self.event_list[mi][4]
           Bdist12_avg_events = Bdist12_avg_events + self.event_list[mi][5]
           Bdist13_avg_events = Bdist13_avg_events + self.event_list[mi][6]
           Bdist23_avg_events = Bdist23_avg_events + self.event_list[mi][7]
         Adist12_avg_events = Adist12_avg_events / self.count_event  # now it is the average dist12 at stateA over all events
         Adist13_avg_events = Adist13_avg_events / self.count_event
         Adist23_avg_events = Adist23_avg_events / self.count_event
         Bdist12_avg_events = Bdist12_avg_events / self.count_event
         Bdist13_avg_events = Bdist13_avg_events / self.count_event
         Bdist23_avg_events = Bdist23_avg_events / self.count_event
       fname = self.filename + ".diff." + str(stateA) + "." + str(stateB)
       with open(fname, "w") as fp :
         fp.write("diffusive-motion from state {0} to state {1} with lifetime_cutoff {2} and gap_cutoff {3} :\n".format(stateA, stateB, self.cut_lifetime, self.max_gap))
         fp.write("count_event {0} \n".format(self.count_event))
         fp.write("{0} {1} {2} {3} {4} {5} \n".format(Adist12_avg_events, Adist13_avg_events, Adist23_avg_events, Bdist12_avg_events, Bdist13_avg_events, Bdist23_avg_events))
         for tt in range(0, self.count_event, 1) :
           fp.write("{0} {1} {2} {3} {4} {5} {6} {7}\n".format(self.event_list[tt][0], self.event_list[tt][1], self.event_list[tt][2], self.event_list[tt][3], self.event_list[tt][4], self.event_list[tt][5], self.event_list[tt][6], self.event_list[tt][7]))   # here 0: event-id, 1: stable_tag, 2,3,4: distances at stateA, 5,6,7: distances at stateB




# below is the main part of code to be executed

# use sys to read the command-line arguments
if len(sys.argv) != 5 :
  print "use this code by\n {0} TSM-file cut-lifetime cut-gap box-size\n".format(sys.argv[0])
  sys.exit()
else :
  input_file_name = sys.argv[1]
  input_cut_lifetime = int(sys.argv[2])
  input_cut_gap = int(sys.argv[3])
  input_box_size = float(sys.argv[4])
  print "{0} {1} {2}".format(input_file_name, input_cut_lifetime, input_cut_gap)

# creating transition-state-matrix
myTSM = TSM()
myTSM.read_data_from_file(input_file_name)

# perform diffusive-motion-analysis
mydiff = SSdiff_from_TSM(myTSM, input_cut_lifetime, input_cut_gap, input_box_size)
for sa in range(1, 9, 1) :   # loop over initial stateA 
  for sb in range(1, 9, 1) :  # loop over final stateB
    mydiff.calculate_diffusive_motion(sa, sb)
    mydiff.my_print_diff(sa, sb)




