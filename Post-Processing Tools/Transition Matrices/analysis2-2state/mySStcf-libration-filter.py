## this code is used for lifetime-analysis based on 3-state-transition-matrix
##   including: state-survival-tcf, 2-state(stable-state)-tcf, and 3-state-tcf

import sys

# here is a class object for one particular event in 3-state-transition-matrix
class Event :
    mol1 = 0
    mol2 = 0
    mol3 = 0
    state1_id = 0
    state1_ti = 0
    state1_tf = 0
    state2_id = 0
    state2_ti = 0
    state2_tf = 0
    state3_id = 0
    state3_ti = 0
    state3_tf = 0

    def __init__(self, id1, id2, id3, k1, t1, t2, k2, t3, t4, k3, t5,t6) :
       self.mol1 = id1
       self.mol2 = id2
       self.mol3 = id3
       self.state1_id = k1
       self.state1_ti = t1
       self.state1_tf = t2
       self.state2_id = k2
       self.state2_ti = t3
       self.state2_tf = t4
       self.state3_id = k3
       self.state3_ti = t5
       self.state3_tf = t6

    # this function is to label an event if it is associated with state1-state2 stable transition
    def checkstable(self, state1, state2, cutoff_duration) :
       result = 0
       if(self.state1_id == state1 and self.state2_id == state2) :  # stable transition without intermediate
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state2_tf - self.state2_ti >= cutoff_duration) :
           result = 1
       if(self.state1_id == state1 and self.state3_id == state2) :  # stable transition via an intermediate-state
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state3_tf - self.state3_ti >= cutoff_duration) :
           result = 2
       return result

    # this is to check whether an event is subject to absorbing bondary condition, i.e. only the first transition is subject to abc
    def checkabc(self, state1, state2, stable_tag, max_gap) : 
       out = 0
       if(stable_tag == 1) :  # this stable_tag should be obtained from function "checkstable"
         if(self.state2_ti - self.state1_tf <= max_gap) :   # max_gap is the maximum gap-duration to regard there is no other transitions in between
           out = 1    # out = 1 means it is subject to abc
         else :
           out = 2    # out = 2 means it is stable-transition, yet not subject to abc, i.e. there are some steps in between
       elif (stable_tag == 2) :
         if(self.state3_ti - self.state1_tf <= max_gap) :
           out = 1
         else :
           out = 2
       else :
         out = 0
       return out


# here is a class object for the atomic coordinates
class Coords :
    num_atoms = 0
    Atomlist = []

    def __init__(self) :
       num_atoms = 1
       Atomlist = [["O", 0.0, 0.0, 0.0]]

    def read_xyz(self, snapid) :
       self.num_atoms = 0
       self.Atomlist = []
       out = 0
       fname = "water" + str(snapid) + ".xyz"
       try :
         fp = open(fname, "r")
       except IOError :
         print "Error, cannot find the file " + fname
         out = 1
       if out == 0 :
         iline = fp.readline()
         iline_split = iline.split()
         self.num_atoms = int(iline_split[0])
         iline = fp.readline()  # read the empty line
         for k in range(0, self.num_atoms, 1) :
           iline = fp.readline()
           iline_split = iline.split()
           if len(iline_split) == 4 :
             iline_data = [iline_split[0], float(iline_split[1]), float(iline_split[2]), float(iline_split[3])]
             self.Atomlist.append(iline_data)
           else :
             print "Error, reading the coordinates " + fname
             out = 2
             break             
         fp.close()
       return out

    # to calculate the distance between three water molecules, I'm using oxygen positions
    def get_distance(self, wat1, wat2, wat3, Boxsize) : 
       O1x = self.Atomlist[(wat1-1)*3][1]
       O1y = self.Atomlist[(wat1-1)*3][2]
       O1z = self.Atomlist[(wat1-1)*3][3]
       O2x = self.Atomlist[(wat2-1)*3][1]
       O2y = self.Atomlist[(wat2-1)*3][2]
       O2z = self.Atomlist[(wat2-1)*3][3]
       O3x = self.Atomlist[(wat3-1)*3][1]
       O3y = self.Atomlist[(wat3-1)*3][2]
       O3z = self.Atomlist[(wat3-1)*3][3]

       distx = O1x - O2x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O1y - O2y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O1z - O2z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist12 = (distx * distx + disty * disty + distz * distz)**(0.5)

       distx = O1x - O3x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O1y - O3y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O1z - O3z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist13 = (distx * distx + disty * disty + distz * distz)**(0.5)

       distx = O2x - O3x
       if distx < -0.5*Boxsize :
         while distx < -0.5*Boxsize :
           distx = distx + Boxsize
       elif distx > 0.5*Boxsize :
         while distx > 0.5*Boxsize :
           distx = distx - Boxsize
       disty = O2y - O3y
       if disty < -0.5*Boxsize :
         while disty < -0.5*Boxsize :
           disty = disty + Boxsize
       elif disty > 0.5*Boxsize :
         while disty > 0.5*Boxsize :
           disty = disty - Boxsize
       distz = O2z - O3z
       if distz < -0.5*Boxsize :
         while distz < -0.5*Boxsize :
           distz = distz + Boxsize
       elif distz > 0.5*Boxsize :
         while distz > 0.5*Boxsize :
           distz = distz - Boxsize
       dist23 = (distx * distx + disty * disty + distz * distz)**(0.5)

       return (dist12, dist13, dist23)



# here is a class object for the 3-state-transition-matrix
class TSM (Event) :
    data_list = []
    data_number = 0
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    filename = "tsm"

    def __init__(self) :
       self.data_list = []
       self.data_number = 0

    def read_data_from_file(self,fname) :
       print "reading the TSM data from file"
       self.filename = fname
       num = 0
       with open(fname, "r") as fp:
         iline = fp.readline()
         while iline :  # read line by line
           fields = iline.split()
           id1 = int(fields[0])
           id2 = int(fields[1])
           id3 = int(fields[2])
           k1 = int(fields[4])
           t1 = int(fields[5])
           t2 = int(fields[6])
           k2 = int(fields[8])
           t3 = int(fields[9])
           t4 = int(fields[10])
           k3 = int(fields[12])
           t5 = int(fields[13])
           t6 = int(fields[14])
           ievent = Event(id1,id2,id3,k1,t1,t2,k2,t3,t4,k3,t5,t6)
           self.data_list.append( ievent )
           num = num + 1
           #print "ttttt", num, iline, fields, ievent.state1_id, ievent.state1_ti, ievent.state1_tf, ievent
           iline = fp.readline()
       self.data_number = num


# here is a class object for stable-state (subject to absorbing boundary condition) time-correlation-function
class SSTCF_from_TSM_filter (Event, TSM, Coords) :
    tcf = []
    duration = 1
    max_time = 1
    count_event = 0
    norm_factor = 0
    filename = "tcf"
    current_TSM = TSM()
    currentcoords = Coords()
    cut_lifetime = 1  # this is used to check whether a stabel is stable or not
    max_gap = 1   # this is used to check whether a stateA to stateB transition is subject to absorbing boundary condition, or not

    def __init__(self, input_TSM, lastsnap, time_range, lifetime_cutoff, gap_cutoff) :
       self.current_TSM = input_TSM
       self.max_time = lastsnap
       self.duration = time_range
       self.cut_lifetime = lifetime_cutoff
       self.max_gap = gap_cutoff
       self.tcf = [0] * time_range
       self.count_event = 0
       self.filename = input_TSM.filename

    def is_initial_state_2nd_shell(self, id1, id2, id3, snapid) :
       self.currentcoords.read_xyz(snapid)
       (d12, d13, d23) = self.currentcoords.get_distance(id1, id2, id3, 18.488)  # box size of 18.488
       #print (id1, id2, id3, snapid, d12, d13, d23)
       out = 1   # if all three distances are larger than 3.50 (1st-shell), then they are in 2nd-shell
       if d12 < 3.50 :
         out = 0
       elif d13 < 3.50 :
         out = 0
       elif d23 < 3.50 :
         out = 0
       return out

    def calculate_tcf(self, stateA, stateB) :  # stateA and stateB are the requested state-id
       self.count_event = 0
       self.norm_factor = 0
       print "calculating stable-state tcf for", stateA, " to ", stateB
       for si in range(0, self.duration, 1) :
         self.tcf[si] = 0
       for si in range(0, self.current_TSM.data_number, 1) :
         ievent = self.current_TSM.data_list[si]
         stable_tag = ievent.checkstable(stateA, stateB, self.cut_lifetime)
         abc_tag = ievent.checkabc(stateA, stateB, stable_tag, self.max_gap)
         if( (stable_tag == 1 or stable_tag == 2) and abc_tag == 1 ) :  # this is to select stable-stable transitions & subject to abc
           if( stable_tag == 1 ) :    # this correspond to stateA-stateB transition without intermediate
             tag_2nd_shell = 0   # add this part to filter out the event, that its initial state has all waters in 2nd shell
             for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               tag_2nd_shell = self.is_initial_state_2nd_shell(ievent.mol1, ievent.mol2, ievent.mol3, tj)
               if tag_2nd_shell == 1 :
                 break
             if tag_2nd_shell == 1 :
               print "skip it", ievent.mol1, ievent.mol2, ievent.mol3, ievent.state1_ti, ievent.state1_tf
               continue
             print "count it", ievent.mol1, ievent.mol2, ievent.mol3, ievent.state1_ti, ievent.state1_tf
             self.count_event = self.count_event + 1
             for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               if( tj <= self.max_time ):  # tj is time-zero in tcf
                 self.norm_factor = self.norm_factor + 1
                 for tt in range(0, self.duration, 1) :
                   if(tj + tt >= ievent.state2_ti) :   # stateB is formed starting from ievent.state2_ti
                     self.tcf[tt] = self.tcf[tt] + 1
                   else :
                     self.tcf[tt] = self.tcf[tt] + 0
           elif( stable_tag == 2) :    # this corresponds to stateA-stateB transition with a short-lived intermediate
             tag_2nd_shell = 0   # add this part to filter out the event, that its initial state has all waters in 2nd shell
             for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               tag_2nd_shell = self.is_initial_state_2nd_shell(ievent.mol1, ievent.mol2, ievent.mol3, tj)
               if tag_2nd_shell == 1 :
                 break
             if tag_2nd_shell == 1 :
               print "skip it", ievent.mol1, ievent.mol2, ievent.mol3, ievent.state1_ti, ievent.state1_tf
               continue
             print "count it", ievent.mol1, ievent.mol2, ievent.mol3, ievent.state1_ti, ievent.state1_tf
             self.count_event = self.count_event + 1
             for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
               if( tj <= self.max_time ):  # tj is time-zero in tcf
                 self.norm_factor = self.norm_factor + 1
                 for tt in range(0, self.duration, 1) :
                   if(tj + tt >= ievent.state3_ti) :   # stateB is formed starting from ievent.state3_ti
                     self.tcf[tt] = self.tcf[tt] + 1
                   else :
                     self.tcf[tt] = self.tcf[tt] + 0


    def my_print_tcf(self, stateA, stateB) :
       fname = self.filename + ".tcf." + str(stateA) + "." + str(stateB)
       with open(fname, "w") as fp :
         fp.write("tcf from state {0} to state {1} with lifetime_cutoff {2} and gap_cutoff {3} :\n".format(stateA, stateB, self.cut_lifetime, self.max_gap))
         fp.write("count_event {0} norm {1}\n".format(self.count_event, self.norm_factor))
         for tt in range(0, self.duration, 1) :
           fp.write("{0} {1}\n".format(tt,self.tcf[tt]))





# below is the main part of code to be executed

# use sys to read the command-line arguments
if len(sys.argv) != 6 :
  print "use this code by\n {0} TSM-file tcf-duration maximum-snap lifetime_cutoff gap_cutoff\n".format(sys.argv[0])
  sys.exit()
else :
  input_file_name = sys.argv[1]
  input_tcf_duration = int(sys.argv[2])
  input_max_snap = int(sys.argv[3])
  input_lifetime_cutoff = int(sys.argv[4])
  input_gap_cutoff = int(sys.argv[5])
  print "{0} {1} {2} {3} {4}".format(input_file_name, input_tcf_duration, input_max_snap, input_lifetime_cutoff, input_gap_cutoff)

# creating transition-state-matrix
myTSM = TSM()
myTSM.read_data_from_file(input_file_name)

# creating stable-state-time-correlation-function
mysstcf = SSTCF_from_TSM_filter(myTSM, input_max_snap, input_tcf_duration, input_lifetime_cutoff, input_gap_cutoff)
for sa in range(1, 9, 1) :   # loop over initial stateA 
  for sb in range(1, 9, 1) :  # loop over final stateB
    mysstcf.calculate_tcf(sa, sb)
    mysstcf.my_print_tcf(sa, sb)



