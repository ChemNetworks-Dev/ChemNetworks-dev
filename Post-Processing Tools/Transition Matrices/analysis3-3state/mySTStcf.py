## this code is used for lifetime-analysis based on 3-state-transition-matrix
##   including: state-survival-tcf, 2-state(stable-state)-tcf, and 3-state-tcf

import sys

# here is a class object for one particular event in 3-state-transition-matrix
class Event :
    mol1 = 0
    mol2 = 0
    mol3 = 0
    state1_id = 0
    state1_ti = 0
    state1_tf = 0
    state2_id = 0
    state2_ti = 0
    state2_tf = 0
    state3_id = 0
    state3_ti = 0
    state3_tf = 0

    def __init__(self, id1, id2, id3, k1, t1, t2, k2, t3, t4, k3, t5,t6) :
       self.mol1 = id1
       self.mol2 = id2
       self.mol3 = id3
       self.state1_id = k1
       self.state1_ti = t1
       self.state1_tf = t2
       self.state2_id = k2
       self.state2_ti = t3
       self.state2_tf = t4
       self.state3_id = k3
       self.state3_ti = t5
       self.state3_tf = t6

    # this function is to label an event if it is associated with state1-state2 stable transition
    def checkstable(self, state1, state2, cutoff_duration) :
       result = 0
       if(self.state1_id == state1 and self.state2_id == state2) :  # stable transition without intermediate
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state2_tf - self.state2_ti >= cutoff_duration) :
           result = 1
       if(self.state1_id == state1 and self.state3_id == state2) :  # stable transition via an intermediate-state
         if(self.state1_tf - self.state1_ti >= cutoff_duration and self.state3_tf - self.state3_ti >= cutoff_duration) :
           result = 2
       return result

    # this is to check whether an event is subject to absorbing bondary condition, i.e. only the first transition is subject to abc
    def checkabc(self, state1, state2, stable_tag, max_gap) : 
       out = 0
       if(stable_tag == 1) :  # this stable_tag should be obtained from function "checkstable"
         if(self.state2_ti - self.state1_tf <= max_gap) :   # max_gap is the maximum gap-duration to regard there is no other transitions in between
           out = 1    # out = 1 means it is subject to abc
         else :
           out = 2    # out = 2 means it is stable-transition, yet not subject to abc, i.e. there are some steps in between
       elif (stable_tag == 2) :
         if(self.state3_ti - self.state1_tf <= max_gap) :
           out = 1
         else :
           out = 2
       else :
         out = 0
       return out


# here is a class object for the 3-state-transition-matrix
class TSM(Event) :
    data_list = []
    data_number = 0
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    filename = "tsm"

    def __init__(self) :
       self.data_list = []
       self.data_number = 0

    def read_data_from_file(self,fname) :
       print "reading the TSM data from file"
       self.filename = fname
       num = 0
       with open(fname, "r") as fp:
         iline = fp.readline()
         while iline :  # read line by line
           fields = iline.split()
           id1 = int(fields[0])
           id2 = int(fields[1])
           id3 = int(fields[2])
           k1 = int(fields[4])
           t1 = int(fields[5])
           t2 = int(fields[6])
           k2 = int(fields[8])
           t3 = int(fields[9])
           t4 = int(fields[10])
           k3 = int(fields[12])
           t5 = int(fields[13])
           t6 = int(fields[14])
           ievent = Event(id1,id2,id3,k1,t1,t2,k2,t3,t4,k3,t5,t6)
           self.data_list.append( ievent )
           num = num + 1
           #print "ttttt", num, iline, fields, ievent.state1_id, ievent.state1_ti, ievent.state1_tf, ievent
           iline = fp.readline()
       self.data_number = num


# here is a class object for state-survival-time
class SST_from_TSM (Event, TSM) :
    duration = 1
    max_time = 0
    filename = "sst"
    current_TSM = TSM()
    ievent = Event(1,2,3,4,5,6,7,8,9,10,11,12)
    count_sst = []  # this is the actual surival tcf
    si = 0
    tj = 0
    tt = 0
    norm_factor = 0
    count_event = 0

    def __init__(self, input_TSM, lastsnap, time_range) :
       self.current_TSM = input_TSM
       self.filename = input_TSM.filename
       self.max_time = lastsnap
       self.duration = time_range
       self.count_sst = [0] * time_range

    def calculate_SST(self, id) :
       self.norm_factor = 0
       self.count_event = 0
       for si in range(0, self.duration, 1) :
         self.count_sst[si] = 0  # initialize it every time
       print "calculating SST from TSM ", id
       for si in range(0, self.current_TSM.data_number, 1) :
         ievent = self.current_TSM.data_list[si]
         if ievent.state1_id == id :  # the SST is for a particular state
           self.count_event = self.count_event + 1
           for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
             if tj <= self.max_time :    # this is time-zero for state-survival-distribution
               self.norm_factor = self.norm_factor + 1
               for tt in range(0, self.duration, 1) :
                 if tj + tt <= ievent.state1_tf :
                   self.count_sst[tt] = self.count_sst[tt] + 1
                 else :
                   self.count_sst[tt] = self.count_sst[tt] + 0

    def my_print_SST(self, id) :
       #print "printing information", id
       foutname = self.filename + ".sst." + str(id) 
       with open(foutname, "w") as fp :
         fp.write("state-survival tcf for State {0} :\n".format(id))
         fp.write("norm_factor {0} {1} \n".format(self.count_event, self.norm_factor))
         fp.write("time  count  \n")
         for tt in range(0, self.duration, 1) :
           fp.write("{0} {1}\n".format(tt,self.count_sst[tt]))


# here is a class object for stable-state (subject to absorbing boundary condition) time-correlation-function
class SSTCF_from_TSM (Event, TSM) :
    tcf = []
    duration = 1
    max_time = 1
    count_event = 0
    norm_factor = 0
    filename = "tcf"
    current_TSM = TSM()
    cut_lifetime = 1  # this is used to check whether a stabel is stable or not
    max_gap = 1   # this is used to check whether a stateA to stateB transition is subject to absorbing boundary condition, or not

    def __init__(self, input_TSM, lastsnap, time_range, lifetime_cutoff, gap_cutoff) :
       self.current_TSM = input_TSM
       self.max_time = lastsnap
       self.duration = time_range
       self.cut_lifetime = lifetime_cutoff
       self.max_gap = gap_cutoff
       self.tcf = [0] * time_range
       self.count_event = 0
       self.filename = input_TSM.filename

    def calculate_tcf(self, stateA, stateB, stateI) :  # stateA and stateB are the requested state-id, through stateI
       self.count_event = 0
       self.norm_factor = 0
       print "calculating stable-state tcf for", stateA, " to ", stateB
       for si in range(0, self.duration, 1) :
         self.tcf[si] = 0
       for si in range(0, self.current_TSM.data_number, 1) :
         ievent = self.current_TSM.data_list[si]
         stable_tag = ievent.checkstable(stateA, stateB, self.cut_lifetime)
         abc_tag = ievent.checkabc(stateA, stateB, stable_tag, self.max_gap)
         if( (stable_tag == 1 or stable_tag == 2) and abc_tag == 1 ) :  # this is to select stable-stable transitions & subject to abc
           if( stable_tag == 1 ) :    # this correspond to stateA-stateB transition without intermediate
             wwwww = 1
#             self.count_event = self.count_event + 1
#             for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
#               if( tj <= self.max_time ):  # tj is time-zero in tcf
#                 self.norm_factor = self.norm_factor + 1
#                 for tt in range(0, self.duration, 1) :
#                   if(tj + tt >= ievent.state2_ti) :   # stateB is formed starting from ievent.state2_ti
#                     self.tcf[tt] = self.tcf[tt] + 1
#                   else :
#                     self.tcf[tt] = self.tcf[tt] + 0
           elif( stable_tag == 2) :    # this corresponds to stateA-stateB transition with a short-lived intermediate
             if ievent.state2_id == stateI :  # here is to select events with a specific intermediate stateI
               self.count_event = self.count_event + 1
               for tj in range(ievent.state1_ti, ievent.state1_tf+1, 1) :
                 if( tj <= self.max_time ):  # tj is time-zero in tcf
                   self.norm_factor = self.norm_factor + 1
                   for tt in range(0, self.duration, 1) :
                     if(tj + tt >= ievent.state3_ti) :   # stateB is formed starting from ievent.state3_ti
                       self.tcf[tt] = self.tcf[tt] + 1
                     else :
                       self.tcf[tt] = self.tcf[tt] + 0


    def my_print_tcf(self, stateA, stateB, stateI) :
       fname = self.filename + ".tcf." + str(stateA) + "." + str(stateB) + ".int" + str(stateI)
       with open(fname, "w") as fp :
         fp.write("tcf from state {0} to state {1} through state {2} with lifetime_cutoff {3} and gap_cutoff {4} :\n".format(stateA, stateB, stateI, self.cut_lifetime, self.max_gap))
         fp.write("count_event {0} norm {1}\n".format(self.count_event, self.norm_factor))
         for tt in range(0, self.duration, 1) :
           fp.write("{0} {1}\n".format(tt,self.tcf[tt]))





# below is the main part of code to be executed

# use sys to read the command-line arguments
if len(sys.argv) != 6 :
  print "use this code by\n {0} TSM-file tcf-duration maximum-snap lifetime_cutoff gap_cutoff\n".format(sys.argv[0])
  sys.exit()
else :
  input_file_name = sys.argv[1]
  input_tcf_duration = int(sys.argv[2])
  input_max_snap = int(sys.argv[3])
  input_lifetime_cutoff = int(sys.argv[4])
  input_gap_cutoff = int(sys.argv[5])
  print "{0} {1} {2} {3} {4}".format(input_file_name, input_tcf_duration, input_max_snap, input_lifetime_cutoff, input_gap_cutoff)

# creating transition-state-matrix
myTSM = TSM()
myTSM.read_data_from_file(input_file_name)

# creating stable-state-time-correlation-function
mysstcf = SSTCF_from_TSM(myTSM, input_max_snap, input_tcf_duration, input_lifetime_cutoff, input_gap_cutoff)
#for sa in range(1, 9, 1) :   # loop over initial stateA 
#  for sb in range(1, 9, 1) :  # loop over final stateB
#    mysstcf.calculate_tcf(sa, sb)
#    mysstcf.my_print_tcf(sa, sb)


# for reorientation, example of state2 to state3
for si in range(1,9,1) :
  mysstcf.calculate_tcf(2,3,si)    # si is the intermediate state id
  mysstcf.my_print_tcf(2,3,si)   

# for reorientation, example of state4 to state6
for si in range(1,9,1) :
  mysstcf.calculate_tcf(4,6,si)    # si is the intermediate state id
  mysstcf.my_print_tcf(4,6,si)   




