/*
 *  h2ogd.c
 *  
 *
 *  Created by ckn on 8/13/12.
 *  Copyright 2012 CASAS WSU. All rights reserved.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define XSTEP 18.643
#define YSTEP 18.643
#define ZSTEP 18.643
#define LARGE 100000


struct edge{
    int nodea;
    int nodeb;
    int change[3];
};

void GetPath(int i, int j, int **next, int **gdmatrix, FILE *fout);
int FindChange(int startnode, int endnode, struct edge *edgelist, int numedgelist);

int main(int argc, char *argv[])
{
	FILE *fin, *fout;
    int ngraph;
	int numnodes = 216, nodea, nodeb;
	float **coord;
	char *cptr, buffer[256], *str, *tok, fname[80], fname1[80];
    float dist;
    int prevnode, curnode, totalchange[3], changeindex;
    int **adjmatrix;
    int **gdmatrix;
    int **next;
    int changeval[3];
    int startnode, endnode;
    struct edge *edgelist;
    int numedgelist = 0;
	int i, j, k, cnt, graphcnt;
    
    ngraph = argc/2;
    for (graphcnt=0; graphcnt<ngraph; graphcnt++)
    {
        printf("processing edges of graph %d %s\n", graphcnt+1, argv[graphcnt+1]);
        fin = fopen(argv[graphcnt+1], "r");
        if (fin == NULL)
        {
            printf("error reading graph file %s\n", argv[graphcnt+1]);
            exit(-1);
        }
        
        cptr = fgets(buffer, 256, fin);
        while(cptr != NULL)
        {
            sscanf(buffer, "%d %d %d %d %d", &nodea, &nodeb, &changeval[0], &changeval[1], &changeval[2]);
            
            if (numedgelist == 0)
                edgelist = (struct edge *) malloc(sizeof(struct edge));
            else
                edgelist = (struct edge *) realloc(edgelist, sizeof(struct edge) * (numedgelist + 1));
            edgelist[numedgelist].nodea = nodea-1;
            edgelist[numedgelist].nodeb = nodeb-1;
            edgelist[numedgelist].change[0] = changeval[0];
            edgelist[numedgelist].change[1] = changeval[1];
            edgelist[numedgelist].change[2] = changeval[2];
            numedgelist++;
            cptr = fgets(buffer, 256, fin);
            
        }
        fclose(fin);
        // ---------------------------------
        // compute the geodesic path
        // build the adjaceny matrix
        adjmatrix = (int **)malloc(sizeof(int *) * numnodes);
        for (i=0; i < numnodes; i++)
            adjmatrix[i] = (int *)calloc(numnodes, sizeof(int));
        
        for (i=0; i < numedgelist; i++)
        {
            adjmatrix[edgelist[i].nodea][edgelist[i].nodeb] = 1;
            adjmatrix[edgelist[i].nodeb][edgelist[i].nodea] = 1;
        }
        
        // compute the geo-desic distance between the nodes
        gdmatrix = (int **)malloc(sizeof(float *) * numnodes);
        for (i=0; i < numnodes; i++)
            gdmatrix[i] = (int *)calloc(numnodes, sizeof(int));
        
        next = (int **)malloc(sizeof(int *) * numnodes);
        for (i=0; i<numnodes; i++)
            next[i] = (int *)calloc(numnodes, sizeof(int));
        
        // initialize the geodesic distance matrix with values in the adjacency matrix
        // gd(i, i) = 0;
        // gd(i, j) = adjmatrix(i, j) if there is an edge between i and j
        // gd(i, j) = LARGE if there is no edge between i and j
        for (i=0; i < numnodes; i++)
            for (j=0; j < numnodes; j++)
            {
                if (adjmatrix[i][j] != 0)
                    gdmatrix[i][j] = adjmatrix[i][j];
                else if (i!=j)
                    gdmatrix[i][j] = LARGE;
                
                next[i][j] = LARGE;
            }
        
        // floyd warshall algorithm for computing the shortest path between the nodes
        for (k=0; k < numnodes; k++)
            for (i=0; i < numnodes; i++)
                for (j=0; j<numnodes; j++)
                    if (gdmatrix[i][j] > (gdmatrix[i][k] + gdmatrix[k][j]))
                    {
                        gdmatrix[i][j] = gdmatrix[i][k] + gdmatrix[k][j];
                        next[i][j] = k;
                    }
        
        // print the geodesic distance between the nodes that are connected to each other.
        sprintf(fname, "%sgeopath", argv[graphcnt+1]);
        fout = fopen(fname, "w");
        for (i=0; i<numnodes; i++)
            for( j=i+1; j < numnodes; j++)
                if (gdmatrix[i][j] < LARGE)
                {
                    if (gdmatrix[i][j] > 1)
                    {
                        fprintf(fout, "%d %d %d path: ", i+1, j+1, gdmatrix[i][j]);
                        GetPath(i, j, next, gdmatrix, fout);
                        fprintf(fout, "%d", j+1);
                    }
                    else
                        fprintf(fout, "%d %d %d path: %d %d", i+1, j+1, gdmatrix[i][j], i+1, j+1);
                    fprintf(fout, "\n");
                }
        fclose(fout);
        
        // ---------------------------------	
        // read the cartesian coordinates from the file
        printf("processing coordinates of graph %d %s\n", graphcnt+1, argv[graphcnt+ngraph+1]);
        fin = fopen(argv[graphcnt+ngraph+1], "r");
        if (fin == NULL)
        {
            printf("error reading the file with cartesian coordinates: %s\n", argv[graphcnt+ngraph+1]);
            exit(-1);
        }
        cptr = fgets(buffer, 256, fin);
        cnt = 0;
        while (cptr != NULL)
        {
            // check if the second column corresponds to "O"
            str = strtok(cptr, " \n");
            if (strcmp(str, "O") == 0)
            {
                if (cnt == 0)
                    coord = (float **)malloc(sizeof(float *));
                else
                    coord = (float **)realloc(coord, sizeof(float *) * (cnt+1));
                
                coord[cnt] = (float *)malloc(sizeof(float) * 3);
                
                // store the coordinates for the "O" node
                str = strtok(NULL, " \n");
                coord[cnt][0] = atof(str);
                
                str = strtok(NULL, " \n");
                coord[cnt][1] = atof(str);
                
                str = strtok(NULL, " \n");
                coord[cnt][2] = atof(str);
                
                cnt++;
            }
            cptr = fgets(buffer, 256, fin);
        }
        fclose(fin);
        if (cnt != numnodes)
        {
            printf("number of O nodes in graph file does not match with cartesian coordiante file\n");
            exit(-1);
        }
        // ------------------------------------- 
        fin = fopen(fname, "r");
        if (fin == NULL)
        {
            printf("error reading the file with cartesian coordinates: %s\n", fname);
            exit(-1);
        }
        
        sprintf(fname1, "%sgeocard", argv[graphcnt+1]);
        fout = fopen(fname1, "w");
        // start reading the paths one by one and 
        // compute the distance in the cartesian coordinates
        // the first line will have snapshot in it.
        cptr = fgets(buffer, 256, fin);
        // start node
        str = strtok(cptr, " \n");
        fprintf(fout, "%s ", str);
        // end node
        str = strtok(NULL, " \n");
        fprintf(fout, "%s ", str);
        // length of the paths
        str = strtok(NULL, " \n");
        fprintf(fout, "%s ", str);
        // read path:
        str = strtok(NULL, " \n");
        fprintf(fout, "%s ", str);
        // obtain the start node
        str = strtok(NULL, " \n");
        prevnode = atoi(str)-1;
        startnode = prevnode;
        fprintf(fout, "%d ", startnode+1);
        while (cptr != NULL)
        {
            str = strtok(NULL, " \n");
            totalchange[0] = 0;
            totalchange[1] = 0;
            totalchange[2] = 0;
            while (str != NULL)
            {
                // determine the successor node
                curnode = atoi(str)-1;
                // find the change
                changeindex = FindChange(prevnode, curnode, edgelist, numedgelist);
                // add it to the total change
                if (changeindex < 0)
                {
                    totalchange[0] += edgelist[-1*changeindex].change[0]*-1;
                    totalchange[1] += edgelist[-1*changeindex].change[1]*-1;
                    totalchange[2] += edgelist[-1*changeindex].change[2]*-1;
                }
                else
                {
                    totalchange[0] += edgelist[changeindex].change[0];
                    totalchange[1] += edgelist[changeindex].change[1];
                    totalchange[2] += edgelist[changeindex].change[2];
                }
                // assign the current node as the start node and
                // move to the next node
                prevnode = curnode;
                fprintf(fout, "%d ", curnode+1);
                str = strtok(NULL, " \n");
            }
            endnode = prevnode;
            dist = 0;
            
            dist += (float)pow((coord[startnode][0] - (coord[endnode][0]+XSTEP*totalchange[0])), 2);
            
            dist += (float)pow((coord[startnode][1] - (coord[endnode][1]+YSTEP*totalchange[1])), 2);
            
            dist += (float)pow(((coord[startnode][2] - (coord[endnode][2]+ZSTEP*totalchange[2]))), 2);
            
            fprintf(fout, "distance: %f\n", pow(dist, 0.5));
            cptr = fgets(buffer, 256, fin);
            if (cptr != NULL)
            {
                // start node
                str = strtok(cptr, " \n");
                fprintf(fout, "%s ", str);
                // end node
                str = strtok(NULL, " \n");
                fprintf(fout, "%s ", str);
                // length of the paths
                str = strtok(NULL, " \n");
                fprintf(fout, "%s ", str);
                // read path:
                str = strtok(NULL, " \n");
                fprintf(fout, "%s ", str);
                // obtain the start node
                str = strtok(NULL, " \n");
                prevnode = atoi(str)-1;
                startnode = prevnode;
                fprintf(fout, "%d ", startnode+1);
            }
            
        }
        
        fclose(fin);
        fclose(fout);
        
        // free memory
        for (i=0; i<numnodes; i++)
        {
            free(coord[i]);
            free(gdmatrix[i]);
            free(adjmatrix[i]);
            free(next[i]);
        }
        free(coord);
        free(edgelist);
        free(gdmatrix);
        free(adjmatrix);
        free(next);
        numedgelist=0;
    }
    return 1;
}

int FindChange(int startnode, int endnode, struct edge* edgelist, int numedgelist)
{
    int index;
    int i;
    
    for (i=0; i<numedgelist; i++)
    {
        if (edgelist[i].nodea == startnode)
            if (edgelist[i].nodeb == endnode)
                return(i);
        if (edgelist[i].nodeb == startnode)
            if (edgelist[i].nodea == endnode)
                return(-i);
    }
    
}

void GetPath(int i, int j, int **next, int **gdmatrix, FILE *fout)
{        
    if (gdmatrix[i][j] == 1)
    {
        fprintf(fout, "%d ", i+1);
        return;
    }
    else
    {
        GetPath(i, next[i][j], next, gdmatrix, fout);
        GetPath(next[i][j], j, next, gdmatrix, fout);
        return;
    } 
}
