/****************************************
 *                                      *
 * Author: Abdullah Ozkanlar            *
 *         abdullah.ozkanlar@wsu.edu    *
 *                                      *
 * Washington State University          *
 * Chemistry Department                 *
 * A. Clark Research Group              *
 * Pullman, WA 99164                    *
 *                                      *
 ****************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#define FLN 256

int main(){

FILE *outputf1;
FILE *outputf2; 

char foutput1[FLN];      /* intput file name */
char foutput2[FLN];      /* intput file name */
FILE *fd;                /* file pointer */

int i,snaps,mode,path;

printf("Enter number of snaphots:");
scanf("%d",&snaps);

printf("\nEnter number of the path you want:\n");
printf("0 - for all paths\n");
printf("2 - for dimers\n");
printf("3 - for trimers\n");
printf("4 - for tetramers\n");
printf("and so on...\n");
scanf("%d",&path);

printf("\nEnter mode:\n");
printf("0 - statistics for the geodesic distance\n");
printf("1 - statistics for the euclidean distance\n");
printf("2 - lifetime of a connection\n");
scanf("%d",&mode);

sprintf(foutput1, "sub_geopath_%dW.pbs",path);
outputf1=fopen(foutput1,"w");

sprintf(foutput2, "sub_geocard_%dW.pbs",path);
outputf2=fopen(foutput2,"w");

fprintf(outputf1,"#PBS -N statistics\n");
fprintf(outputf1,"#PBS -l nodes=1:ppn=1\n");
fprintf(outputf1,"#PBS -l mem=15gb\n");
fprintf(outputf1,"#PBS -j oe\n");
fprintf(outputf1,"cd $PBS_O_WORKDIR\n");

fprintf(outputf1,"./graphstats-new %d ",mode);
for(i=1;i<=snaps;i++){
if(path==0) fprintf(outputf1,"graph.%dgeopath ",i);
else fprintf(outputf1,"graph.%dgeopath-%dW ",i,path);
}

fprintf(outputf2,"#PBS -N statistics\n");
fprintf(outputf2,"#PBS -l nodes=1:ppn=1\n");
fprintf(outputf2,"#PBS -l mem=15gb\n");
fprintf(outputf2,"#PBS -j oe\n");
fprintf(outputf2,"cd $PBS_O_WORKDIR\n");

fprintf(outputf2,"./graphstats-new %d ",mode);
for(i=1;i<=snaps;i++){
if(path==0) fprintf(outputf2,"graph.%dgeocard ",i);
else fprintf(outputf2,"graph.%dgeocard-%dW ",i,path);
}

printf("\nDon't forget to modify .pbs file! Won't work otherwise!\n");
printf("\nSubmission: qsub -l nodes=c0-15:ppn=1 sub_geocard_%dW.pbs\n",path);

fclose(outputf1);
fclose(outputf2);

return 0;
}


