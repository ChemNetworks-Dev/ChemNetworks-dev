//
//  graphstats-new.c
//  
//
//  Created by Narayanan Chatapuram Krishnan on 10/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
//  Modified by Abdullah Ozkanlar on 11/06/12
//  to print statitics on minimum euclidean distances.
// 

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define GDCONNECTIONSTATS 0
#define EDCONNECTIONSTATS 1
#define LIFETIME 2
#define NUMNODES 216

struct graph
{
    int **pathindex;
    int **path;
    int *pathlength;
    int npaths;
    int **processed;
};

void GDConnectionStats(int argc, char *argv[]);
void EDConnectionStats(int argc, char *argv[]);
void GDPathLifeTime(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    int mode;
    
    mode = atoi(argv[1]);
    if (mode == GDCONNECTIONSTATS)
    {
        printf("geodesic connection stats:\n");
        GDConnectionStats(argc, argv);
    }
    
    if (mode == EDCONNECTIONSTATS)
    {
        printf("euclidean distance stats: \n");
        EDConnectionStats(argc, argv);
    }

    if (mode == LIFETIME)
    {
        printf("lifetime of geodesic paths: \n");
        GDPathLifeTime(argc, argv);
    }
    
    return(1);
}

void GDConnectionStats(int argc, char *argv[])
{
    int max = 0;
    int maxglobal = 0;
    float avgmaxglobal = 0;
    float avgconnection = 0;
    int nconnections = 0;
    float avgconnectionglobal = 0;
    int nconnectionsglobal = 0;
    int length = 0;
    char *cptr, buffer[1024], *str;
    int i;
    FILE *fin;
    
    // from the second argument till the end
    for (i = 2; i < argc; i++)
    {
        fin = fopen(argv[i], "r");
        if (fin == NULL)
        {
            printf("unable to open %s\n", argv[2]);
            exit(1);
        }
        cptr = fgets(buffer, 1024, fin);
        while (cptr != NULL)
        {
            // start node
            str = strtok(cptr, " \n");
            // end node
            str = strtok(NULL, " \n");
            // length of the paths
            str = strtok(NULL, " \n");
            length = atoi(str);
            if (max < length)
                max = length;
            avgconnection += length;
            nconnections++;
            cptr = fgets(buffer, 1024, fin);
        }
        printf("graph: %d avgconnection: %.4f maxconnection: %d\n", i-1, avgconnection/nconnections, max);
        nconnectionsglobal += nconnections;
        avgconnectionglobal += avgconnection;
        avgmaxglobal += max;
        if (maxglobal < max)
            maxglobal = max;
        nconnections = 0;
        avgconnection = 0;  
        max = 0;
        fclose(fin);
    }
    printf("\nglobal statistics\n---------------------\navgconnection: %.4f maxconnection: %d avgmaxconnection :%.4f\n", avgconnectionglobal/nconnectionsglobal, maxglobal, avgmaxglobal/(argc-2));
    
}

void EDConnectionStats(int argc, char *argv[])
{
    float distance = 0;
    float max = 0;
    float min = 10.0;
    float maxglobal = 0;
    float minglobal = 10;
    float avgmaxglobal = 0;
    float avgminglobal = 0;
    float avgconnection = 0;
    int nconnections = 0;
    float avgconnectionglobal = 0;
    int nconnectionsglobal = 0;
    int length = 0;
    char *cptr, buffer[1024], *str;
    int i, j;
    FILE *fin;
    
    // from the second argument till the end
    for (i = 2; i < argc; i++)
    {
        fin = fopen(argv[i], "r");
        if (fin == NULL)
        {
            printf("unable to open %s\n", argv[i]);
            exit(1);
        }
        cptr = fgets(buffer, 1024, fin);
        while (cptr != NULL)
        {
            // start node
            str = strtok(cptr, " \n");
            // end node
            str = strtok(NULL, " \n");
            // length of the paths
            str = strtok(NULL, " \n");
            length = atoi(str);
            // path
            str = strtok(NULL, " \n");
            for (j=0; j < length+1; j++) 
                str = strtok(NULL, " \n");
            // distance :
            str = strtok(NULL, " \n");
            // euclidean distance
            str = strtok(NULL, " \n");
            distance = atof(str);
            if (max < distance)
                max = distance;
            if (min > distance)
                min = distance;
            avgconnection += distance;
            nconnections++;
            cptr = fgets(buffer, 1024, fin);
        }
        printf("graph: %d avgdistance: %.4f maxdistance: %.4f mindistance: %.4f\n", i-1, avgconnection/nconnections, max, min);
        nconnectionsglobal += nconnections;
        avgconnectionglobal += avgconnection;
        avgmaxglobal += max;
        avgminglobal += min;
        if (maxglobal < max)
            maxglobal = max;
        if (minglobal > min)
            minglobal = min;
        nconnections = 0;
        avgconnection = 0;
        max = 0;
        min = 10;
        fclose(fin);
    }
    printf("\nglobal statistics\n---------------------\navgdistance: %.4f maxdistance: %.4f avgmaxdistance :%.4f mindistance: %.4f avgmindistance :%.4f\n", avgconnectionglobal/nconnectionsglobal, maxglobal, avgmaxglobal/(argc-2), minglobal, avgminglobal/(argc-2));
    
}

void GDPathLifeTime(int argc, char *argv[])
{
    int i, j, k;
    int spindex, cpindex, found;
    int bnode, enode;
    char *cptr, buffer[1024], *str, *tok;
    FILE *fin, *fout;
    struct graph *graphs;
    
    graphs = (struct graph *)malloc(sizeof(struct graph) * (argc-2));
    
    for (i=0; i<argc-2; i++)
    {
        printf("processing graph %d %s\n", i+1, argv[i+2]);
        // read the graph
        fin = fopen(argv[i+2], "r");
        if (fin == NULL)
        {
            printf("Unable to open %s\n", argv[i+2]);
            exit(-1);
        }
        // initialize the path index for the graph
        // if there is no path between two nodes the value in 
        // the path index matrix will be -1
        // else the number will point to the index in the path array
        graphs[i].pathindex = (int **)malloc(sizeof(int *) * NUMNODES);
        for (j=0; j<NUMNODES; j++)
        {
            graphs[i].pathindex[j] = (int *)calloc(NUMNODES, sizeof(int));
            for (k=0; k<NUMNODES; k++)
                graphs[i].pathindex[j][k]=-1;
        }
        // initialize the number of paths in the graph to be 0
        graphs[i].npaths = 0;
        cptr = fgets(buffer, 1024, fin);
        while(cptr != NULL)
        {
            // allocate the memory for a new path
            if (graphs[i].npaths == 0) 
            {
                graphs[i].path = (int **)malloc(sizeof(int*));
                graphs[i].pathlength = (int *)malloc(sizeof(int));
            } else 
            {
                graphs[i].path = (int **)realloc(graphs[i].path, sizeof(int *) * (graphs[i].npaths+1));
                graphs[i].pathlength = (int *)realloc(graphs[i].pathlength, sizeof(int) * (graphs[i].npaths+1));
            }
            // process the path string and populate the memory for the newly created path
            // start node
            tok = strtok(cptr, " \n");
            bnode = atoi(tok);
            // end node
            tok = strtok(NULL, " \n");
            enode = atoi(tok);
            
            // assign the index into the path array
            graphs[i].pathindex[bnode-1][enode-1] = graphs[i].npaths;
            
            // length of the path
            tok = strtok(NULL, " \n");
            graphs[i].pathlength[graphs[i].npaths] = atoi(tok);
            
            // path:
            tok = strtok(NULL, " \n");
            
            // assign the memory for the path
            graphs[i].path[graphs[i].npaths] = (int *)malloc(sizeof(int) * (graphs[i].pathlength[graphs[i].npaths]+1));
            // read the path
            for (j=0; j<graphs[i].pathlength[graphs[i].npaths]+1; j++) 
            {
                tok = strtok(NULL, " \n");
                graphs[i].path[graphs[i].npaths][j] = atoi(tok)-1;
            }
            graphs[i].npaths++;
            cptr = fgets(buffer, 1024, fin);
        }
        fclose(fin);       
    }

    // initialize the processed matrix for every graph.
    for (i=0; i<argc-2; i++)
    {
        graphs[i].processed = (int **)malloc(sizeof(int*)*NUMNODES);
        for (j=0; j<NUMNODES; j++) 
            graphs[i].processed[j] = (int *)calloc(NUMNODES, sizeof(int));
    }
    fout = fopen("lifetime", "w");
    // let us parse through each graph check for the lifetime of a path
    for (i=0; i<argc-2; i++)
    {
        for (bnode=0; bnode<NUMNODES; bnode++)
            for (enode=bnode+1; enode<NUMNODES; enode++)
                if (graphs[i].pathindex[bnode][enode] != -1 && graphs[i].processed[bnode][enode] == 0)
                {
                    // index of the path
                    spindex = graphs[i].pathindex[bnode][enode];
                    fprintf(fout, "%d %d path: ", bnode+1, enode+1);
                    for (k=0; k<graphs[i].pathlength[spindex]+1; k++)
                        fprintf(fout, "%d ", graphs[i].path[spindex][k]+1);
                    fprintf(fout, "lifetime: %d ",i+1);

                    // detected a path that has not been processed
                    // check for its lifetime
                    for (j=i+1; j<argc-2; j++)
                    {
                        found=0;
                        // there exists a path in the succeeding graph
                        if (graphs[j].pathindex[bnode][enode] != -1)
                        {
                            // index of the path in the current graph
                            cpindex = graphs[j].pathindex[bnode][enode];
                            // if the path has the same length as the start graph
                            if (graphs[i].pathlength[spindex] == graphs[j].pathlength[cpindex])
                            {
                                found=1;
                                for (k=0; k<graphs[i].pathlength[spindex]+1; k++)
                                {
                                    // check if the paths are identical
                                    if (graphs[i].path[spindex][k] != graphs[j].path[cpindex][k])
                                    {
                                        found=0;
                                        break;
                                    }
                                }
                            }
                            
                        }
                        // stop processing the succeeding graphs as we have found a break
                        if (found == 0)
                            break;                        
                        else
                        {
                            fprintf(fout, "%d ", j+1);
                            graphs[j].processed[bnode][enode] = 1;
                        }

                    }
                    fprintf(fout, "\n");
                    
                }
        
    }
    
    // free memory
    for (i=0; i<argc-2; i++)
    {
        for (j=0; j<NUMNODES; j++) 
        {
            free(graphs[i].pathindex[j]);
            free(graphs[i].processed[j]);
        }
        for (j=0; j<graphs[i].npaths; j++)
            free(graphs[i].path[j]);
        free(graphs[i].pathlength);
        free(graphs[i].pathindex);
        free(graphs[i].path);
        free(graphs[i].processed);
    }
    free(graphs);

}
