awk '{$1=$2=$3=$4=$5=$6=$7=""}1' lifetime-3W > lifetime-3W-count.tmp
echo "lifetime-3W: first 7 fields deleted ..."
awk '$0="line"NR": "NF' lifetime-3W-count.tmp > lifetime-3W-count1
echo "lifetime-3W-count1 generated ..."
awk '{print $2}' lifetime-3W-count1 > lifetime-3W-count
echo "lifetime-3W-count generated ..." 
rm lifetime-3W-count.tmp
rm lifetime-3W-count1
awk -F, '{if($1==1)print > "lifetime-3W-count-1s";else if($1==2)print > "lifetime-3W-count-2s";else if($1==3)print > "lifetime-3W-count-3s";else if($1==4)print > "lifetime-3W-count-4s";else if($1==5)print > "lifetime-3W-count-5s";else print > "lifetime-3W-count-GrEq6s"}' lifetime-3W-count
echo "lifetime-3W-count-1s 2s 3s 4s 5s and lifetime-3W-count-GrEq6s generated ..."
echo "Done ..."
