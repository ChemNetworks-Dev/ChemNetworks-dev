awk '{$1=$2=$3=$4=$5=$6=""}1' lifetime-2W > lifetime-2W-count.tmp
echo "lifetime-2W: first 6 fields deleted ..."
awk '$0="line"NR": "NF' lifetime-2W-count.tmp > lifetime-2W-count1
echo "lifetime-2W-count1 generated ..."
awk '{print $2}' lifetime-2W-count1 > lifetime-2W-count
echo "lifetime-2W-count generated ..." 
rm lifetime-2W-count.tmp
rm lifetime-2W-count1
awk -F, '{if($1==1)print > "lifetime-2W-count-1s";else if($1==2)print > "lifetime-2W-count-2s";else if($1==3)print > "lifetime-2W-count-3s";else if($1==4)print > "lifetime-2W-count-4s";else if($1==5)print > "lifetime-2W-count-5s";else print > "lifetime-2W-count-GrEq6s"}' lifetime-2W-count
echo "lifetime-2W-count-1s 2s 3s 4s 5s and lifetime-2W-count-GrEq6s generated ..."
echo "Done ..."
