#!/bin/sh

for i in {1..1000}
do
  echo "Editing $i ..."
  awk '/ 1 path/' graph.${i}geocard > graph.${i}geocard-2W
  echo "Editing $i  1-paths done..."
  awk '/ 2 path/' graph.${i}geocard > graph.${i}geocard-3W
  echo "Editing $i  2-paths done..."
  awk '/ 3 path/' graph.${i}geocard > graph.${i}geocard-4W
  echo "Editing $i  3-paths done..."
  awk '/ 4 path/' graph.${i}geocard > graph.${i}geocard-5W
  echo "Editing $i  4-paths done..."
  awk '/ 5 path/' graph.${i}geocard > graph.${i}geocard-6W
  echo "Editing $i  5-paths done..."
done
