      PROGRAM cluster
!*********************************************************************
!     
!    -program retrieves coordinates (without breaking a molecule) along 
!     specific distance range from an atom in an xyz file, wraps coordinates
!     around the specific atom/ion of interest
!    -only works for water right now, assumes OHH molecule order
!     
!     created by auclark may 2013
!     
!*********************************************************************
      integer, parameter :: mxfile = 2000000
      integer, parameter :: mxatms = 200000
      character*9, DIMENSION(mxatms) ::  atmnam, atmnamB
      real*8, DIMENSION(mxatms) :: xxx,yyy,zzz, xxxB, yyyB, zzzB
      real*8 :: xdist, ydist, zdist, dist, xs, ys, zs
      real*8, DIMENSION(9) :: cell
      integer :: natmssolv, natmssolB, fileno, atarg
      
!*********************************************************************
! Open input file 

      open(unit=7,file='water1.xyz',status='old',form='formatted')
      open(unit=8,file='solB1.xyz',status='old',form='formatted')
      open(unit=2, file='newcoordh2o.xyz',form='formatted')
      read(7,*) natmssolv
      read(7,*)
      read(8,*) natmssolB
      read(8,*)

            j = 0
             do while (j < natmssolv)
                j = j + 1
             read(7,*) atmnam(j), xxx(j), yyy(j), zzz(j) 
             enddo

            j = 0
             do while (j < natmssolB)
                j = j + 1
             read(8,*) atmnamB(j), xxxB(j), yyyB(j), zzzB(j) 
             enddo

!  center about the solB COM - set approximate COM atom index
        atarg=2
        xs = xxxB(atarg)
        ys = yyyB(atarg)
        zs = zzzB(atarg)

! set the cell dimensions
        cell(1)=18.873
        cell(5)=18.873
        cell(9)=18.873

!      i = 0
!       do while (i < natmssolv) !changes the solvent coordinates to be centered about the solB
!         i = i + 1
!             xxx(i) = xxx(i) - xs
!             xxx(i) = xxx(i) - cell(1)*nint(xxx(i)/cell(1))
!             yyy(i) = yyy(i) - ys
!             yyy(i) = yyy(i) - cell(5)*nint(yyy(i)/cell(5))
!             zzz(i) = zzz(i) - zs
!             zzz(i) = zzz(i) - cell(9)*nint(zzz(i)/cell(9))
!       enddo


            j = 0
             do while (j < natmssolv)  !now calculate the distances of the solvent from the solB COM
                j = j + 1
               if(atmnam(j) == 'Ow ')then !may  need to change O-atom atom name
!                 xdist=(xxx(j)-xxxB(atarg))**2
                 xdist=(xxx(j)-xs)**2
!                 ydist=(yyy(j)-yyyB(atarg))**2
                 ydist=(yyy(j)-ys)**2
!                 zdist=(zzz(j)-zzzB(atarg))**2
                 zdist=(zzz(j)-zs)**2
                 dist=sqrt(xdist+ydist+zdist)
                 if(dist.lt.6.1d0) then !set your distance cutoff - this is for the whole OHH
                  write(2,'(a2,2x,3e14.6)')atmnam(j),      &
                   xxx(j),yyy(j),zzz(j)
                  write(2,'(a2,2x,3e14.6)')atmnam(j+1),      &
                  xxx(j+1),yyy(j+1),zzz(j+1)
                  write(2,'(a2,2x,3e14.6)')atmnam(j+2),      &
                  xxx(j+2),yyy(j+2),zzz(j+2)
                 j=j+2 
                 endif
               endif
!               if(atmnam(j) == 'Cl')then
!                 xdist=(xxx(j)-xxx(atarg))**2
!                 ydist=(yyy(j)-yyy(atarg))**2
!                 zdist=(zzz(j)-zzz(atarg))**2
!                 dist=sqrt(xdist+ydist+zdist)
!                 if(dist.lt.6.5) then
!                  write(2,'(a2,2x,3e14.6)')atmnam(j),      &
!                   xxx(j),yyy(j),zzz(j)
!                 endif
!               endif
!               if(atmnam(j) == 'OU')then
!                  write(2,'(a2,2x,3e14.6)')atmnam(j),      &
!                   xxx(j),yyy(j),zzz(j)
!               endif
             enddo

            j = 0
             do while (j < natmssolB)
                j = j + 1
                  write(2,'(a2,2x,3e14.6)')atmnamB(j),      &
                  xxxB(j),yyyB(j),zzzB(j)
             enddo
          
         close(7)
         close(8)
         close(2)

      end PROGRAM cluster
