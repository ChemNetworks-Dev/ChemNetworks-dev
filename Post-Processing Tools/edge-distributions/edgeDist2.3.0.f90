      PROGRAM edgeDist

!**********************************************************************
!   Program to:
!      count the number of hydrogen bonds (edge distribution) -> edout 
!      average the the number of conections (edge frequency) -> efout
!      count the number of path lengths (geodesic analysis) -> egout
!      
!   input(s): *.graph
!             *.geocard
!   
!   This code was re-written from edgeDist1.3 code to work with 
!      scripts.
!   As of Dec 2015 the egout file does not have adjustable bin sizes
!   Fixed a bug with count1 in ver 2.1.1
!   As of ver 2.3.0, a default bin will be used that uses integer
!      bin sizes 
!              
!    last modified: April 2016 by L. Edens
!**********************************************************************
      IMPLICIT NONE
      integer, parameter :: mxNodes = 300
      integer, parameter :: mxLine = 3000
      integer, parameter :: mxPath = 10
      integer, parameter :: filelen = 100
      integer, parameter :: shFilLen = 20
      integer, parameter :: extlen = 15
      integer, dimension(mxNodes) :: count1, track, path
      integer, dimension(mxNodes,2) :: count2
      real, dimension(mxPath,3) :: dist
      integer :: vert, nVert, mxVert, er, ex, found, ctest
      integer :: nbin, mxFreq, mnFreq, gmxFreq, totFreq, i, j, k
      integer :: cfile, efout, edout, egout
      integer :: bin = 1, fnum, mainLoop
      integer :: node1, node2, nPath, pthlen, totnPath, totPath
      real ::  tdist
      character*(20), parameter :: FM = '(I3,a,I6,a,F5.2,a)'
      character*(50), parameter :: FM2 = '(I3,a,I8,a,F8.4,a,F8.4 &
                                    &a,F8.4,a,F5.2)'
      character*(filelen) :: infile, filename
      character*(shFilLen) :: pfile1, pfile2, pfile3, pfile4, pfile5
      character*(filelen+extlen) :: outfile1, outfile2, outfile3
      character*(filelen+extlen) :: readfile
      character*(extlen) :: fext1 = '.edout', fext2 = '.efout'
      character*(extlen) :: fext3 = '.egout'
      character*(extlen) :: inext1 = '.Graph', inext2 = '.geocard'
      character*(10) :: chnum
      character*1 :: tab
      logical :: debug
!**********************************************************************
!Turn on/off debug print statements
      debug =  .False. 
!**********************************************************************
!Initialize

      do i=1,mxNodes
         count2(i,1) = 0
         count2(i,2) = 0
         track(i) = 0
         path(i) = 0
         dist(i,1) = 0
         dist(i,2) = 0
         dist(i,3) = 0
      enddo
      cfile =0
      nVert = 0
      mxVert = 1
      mxFreq = 1
      mnFreq = 0
      gmxFreq = 0
      totFreq = 0
      er = 0
      bin = 0
      tab = char(9)
      nPath = 0
      totnPath = 0
      totPath = 0
      tdist = 0.0
!**********************************************************************
!input parameters

      filename = 'Output.Graph' !output filename
         if(debug)write(6,*) 'Filename ', filename       

      edout = 0
         if(debug)write(6,*) 'edout', edout      

      efout = 0
         if(debug)write(6,*) 'efout', efout       

      egout = 1
         if(debug)write(6,*) 'egout', egout       

      mnFreq = 0 !minimum frequency number
         if(debug)write(6,*) 'mnFreq', mnFreq       

      bin = 1 !bin size
         if(debug)write(6,*) 'bin', bin       

      !****the nummber of infiles *******
      mainLoop = 100
         
      !input files must be in one of these formats (# = number):
      !pfile1#.input.pfile2#.xyz.pfile4#.xyz.Graph
      !pfile1#.input.pfile2#.xyz.pfile4#.xyz.GraphGeod
      !pfile5#.xyz.geocard
      !pfile(s) denote the file input name that can be adjusted by the
      !user. The code is currently using water.input.water.xyz.water.xyz.Graph 
      !and water.xyz.geocard as markers. This can be changed in the bash
      !script.Please refer to the Readme file for additional details. 

      pfile1 = 'water'
      pfile2 = '.pfile2'
      pfile3 = '.xyz'
      pfile4 = '.pfile4'
      pfile5 = 'water'
      fnum = 1

!**********************************************************************
!as of 11/23/15 
!     file ID   file type
!       10        infile (if applicable)
!       11        readfile for .Graph files
!       12        edout (edge number per node)
!       13        efout (edge frequency)
!       14        readfile for .geocard files
!       15        egout (geodesic frequency)
!**********************************************************************


!**********************************************************************
!Generate output filename

      j = 1
      k = 1
      write(chnum, '(I10)')fnum
      filename = trim(filename)//adjustl(chnum)
      !add extension to the user inputted filename
       !this is done by stepping through the filename until a the 
       !first ' ' is found. The extension is then added.
       !The code is currently setup for only 2 extensions
       !trim() is used to to eliminate the ' ' at the end of the file 
      do i=1,(filelen+extlen)
         if(filename(j:j) /= ' ' .AND. j /= filelen) then
            outfile1(i:i) = filename(j:j)
            outfile2(i:i) = filename(j:j)
            outfile3(i:i) = filename(j:j)
            j = j+1
         else if(k < extlen+1) then
            !found first ' ', add extension
            outfile1(i:i) = fext1(k:k)
            outfile2(i:i) = fext2(k:k)
            outfile3(i:i) = fext3(k:k)
            k = k+1
         else
            outfile1(i:i) = ' '
            outfile2(i:i) = ' '
            outfile3(i:i) = ' '
         endif
      enddo
      if(debug)write(6,*)'outfile1 = ', trim(outfile1)


!**********************************************************************
!main loop
          
      do cfile=1,mainLoop

         write(chnum, '(I10)')fnum
         if(edout == 1 .OR. efout ==  1) then     

            !zero count1 array for readin
            do i=1,mxNodes
               count1(i) = 0
            enddo

            !build .Graph readfile name
             !pfile1: 'water.input'
             !pfile2: '.water'
             !pfile3: '.xyz'
             !pfile4: '.water'
             !pfile(s) are defined according to user in the
             !edgeDist-le.bash     
            readfile = trim(pfile1)//pfile2
            readfile = trim(readfile)//adjustl(chnum)
            readfile = trim(readfile)//pfile3
            readfile = trim(readfile)//pfile4
            readfile = trim(readfile)//adjustl(chnum)
            readfile = trim(readfile)//pfile3
            if(debug)write(*,*) 'readfile ',&
               trim(readfile)//trim(inext1)    
            open(11,file=(trim(readfile)//trim(inext1)), &
                  action='read',iostat=ex)
            if(er/=0) then
               write(6, '(a)')'Could not open readfile. Exiting.'
               stop
            endif

            !******
            !populate count array
            !loop for the max lines of read in file
            iloop: do i=1,mxLine  
            read(11,*,iostat=ex)vert
               if(ex>0) then
                  write(6,'(a)')'Error with input file'
                  stop
               else if(ex<0) then    !exit loop at end of file
                  if(debug)write(6,'(a)')'End of file'
                  exit iloop
               else      
                  !loop for max array size           
                  do j=1,mxNodes
                     !This method cycles through the count array until 
                      !the array index equals vert. The element at that
                      !index is then incremented by 1
                     if(vert == j) then
                        !add counter to correct node
                        count1(j)=count1(j)+1  
                        !record the number of unique vertices read in
                         !this number will be used as the max loop 
                         !size below            
                        if(count1(j)==1 .AND. vert>mxVert) mxVert=vert
                        if(count1(j)> mxFreq) then
                           !find the overall maximum edge number 
                           !if(debug)then 
                              !write(6,*)'count1(',j,')=',count1(j) 
                           !endif
                           mxFreq = count1(j)
                        endif
                     endif
                  enddo !mxNodes loop
               endif
               nVert = nVert+1
            enddo iloop
            close(11)
         endif

         !******
         !output edge number per node array 
         if(edout==1) then
            if (cfile ==1 ) open(12,file=trim(outfile1), status='new')
            !write output file as a large matrix where
            !each row is a snapshot
            !Format is (where t= snapshot, vert = verticies):
            ! vert_num_for_node1(t1) vert_num_for_node2(t1) ...
            ! vert_num_for_node1(t2) vert_num_for_node2(t2) ...
            ! ...
            ! vert_num_for_node1(tn) vert_num_for_node2(tn) ...
            write(12,*)(count1(i),' ',i=1,mxVert)
         endif

         !*****
         !find the edge distribution frequency
         if(efout==1) then

            !NOTE: nbin will cause an error if not the correct size

            if(bin.LE.0) then
               write(6,*)'bin must be greater than 0'
               efout = 0
               goto 20
            endif         

            !fill in bin counts
 
            do i=1,mxVert
               track(count1(i)+1) = track(count1(i)+1) + 1
               totFreq = totFreq + 1
            if(gmxFreq < mxFreq) gmxFreq = mxFreq+1

 10         enddo
 20      endif

         if(egout == 1) then
            !build .geocard readin file
             !build readfile name
             !pfile1: 'water.input'
             !pfile2: '.water'
             !pfile3: '.xyz'
             !pfile4: '.water'
             !pfile5: 'water'
             !pfile(s) are defined according to user in the
             !edgeDist-le.bash
            readfile = trim(pfile5)//adjustl(chnum)
            readfile = trim(readfile)//pfile3
            if(debug) write(6,*) 'readfile ',&
               trim(readfile)//trim(inext2)    
            open(14,file=(trim(readfile)//trim(inext2)), &
               action='read',iostat=ex)
            if(er/=0) then
               write(6, '(a)')'Could not open readfile. Exiting.'
               stop
            endif

            jloop: do j=1,mxLine

               !node1 node2 pthlen path:: # # .... distance: dist              
               read(14,*,iostat=ex) node1, node2, &
                  pthlen, (chnum,i=1,pthlen+4)
               if(ex>0) then
                  write(6,'(a)')'Error with input file'
                  stop
               else if(ex<0) then    !exit loop at end of file
                  if(debug)write(6,'(a)')'End of file'
                  exit jloop
               else
                  read(chnum, '(F10.6)')tdist

                  !increment path array and add distance to dist array
                  path(pthlen) = path(pthlen) + 1.0
                  dist(pthlen,1) = dist(pthlen,1) + tdist
                  totPath = totPath + 1

                  !determine min and max distance
                  if(tdist /=  0.0) then
                     !min
                     if(dist(pthlen,2) == 0) then
                        dist(pthlen,2) = tdist
                     else if (dist(pthlen,2) > tdist) then
                         dist(pthlen,2) = tdist
                     endif
                     !max
                     if(dist(pthlen,3) < tdist) then
                        dist(pthlen,3) = tdist
                     endif
                  endif

                  !count number of unique pathlengths
                  if(path(pthlen)==1)nPath = nPath + 1
               endif 
            enddo jloop     
            if(totnPath < nPath) totnPath = nPath   
            nPath = 0.0
            close(14)
         endif
    
         if(debug)write(6,*)'totFreq= ',totFreq

         fnum = fnum + 1
      enddo !mainLoop

      if(edout == 1) close(12)
      
!***********************************************************************
!outputs

      !frequency of edge numbers array
      if(efout==1) then
         open(13,file=trim(outfile2), status='new')        
          do i=1,gmxFreq
            write(13,FM)i-1,' ',track(i),' ', &
                 (real(track(i))/real(totFreq))*100.0
         enddo
!        do i=1,nbin
!            write(13,FM)count2(i,1),' ',count2(i,2),' ', &
!                 (real(count2(i,2))/real(totFreq))*100.0
!         enddo
         close(13)
      endif

      !geopaths
      if(egout==1) then
         open(15,file=trim(outfile3), status='new')
!     Print headings
      write(15,'(A4, T6, A8, T17, A8, T27, A8, T37, & 
          A8, T47, A8, T57 / 54("="))') "Path", "Freq.", &
         "Avg.Dist.", "MinDist", "MaxDist", "Percent" 
       
         do i=1,totnPath
            write(15,FM2)i,' ',path(i),'  <',dist(i,1)/(path(i)*1.0), &
               '> ',dist(i,2),'  ',dist(i,3), '   ',&
               (real(path(i))/real(totPath))*100.0
          enddo
         close(15)
      endif
      write(6, *)'Number of data files: ',cfile-1

      END PROGRAM edgeDist
