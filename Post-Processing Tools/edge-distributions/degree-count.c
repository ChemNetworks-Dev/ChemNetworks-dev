/****************************************
 *                                      *
 * Author: Abdullah Ozkanlar            *
 *         abdullah.ozkanlar@wsu.edu    *
 *                                      *
 * Washington State University          *
 * Chemistry Department                 *
 * A. Clark Research Group              *
 * Pullman, WA 99164                    *
 *                                      *
 ****************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "nrutil.h"
#define NRANSI
#define FLN 256

double *vector(int nl, int nh);
double **matrix(int nrl, int nrh, int ncl, int nch);
void free_vector(double *v, int nl, int nh);
void free_matrix(double **m, int nrl, int nrh, int ncl, int nch);
int findf(FILE *fd, int n, ...);

int main(int argc, char *argv[]){

       FILE *inputf1;
       FILE *outputf1;

       char finput[FLN];      /* intput file name */
       FILE *fd;              /* file pointer */  

       char foutput[FLN]; 


      int N;    

      int nwaters, nsnaps;
      double total, psifir, pbir, piki, puc, pdort, pbes, palti;
      int i,j,k,m,test,atm;  
      double sifir, bir, iki, uc, dort, bes, alti;
      double *a,**c;
      double first,second;
      char format[] = "%.8E ";  /* format of output */



/**************************************************/
  if(argc != 2){
    printf("Usuage: %s inputfile\n"
           "inputfile is assumed to have extension .new\n", argv[0]);

    exit(-1);
  }


sprintf(finput, "%s", argv[1]);

  if((fd=fopen(finput,"r"))==NULL){
    printf("Cannot open file %s\n", finput);
    exit(-1);
  }

/**************************************************/
   
printf("Enter number of waters: \n");
scanf("%d",&nwaters);
printf("Enter number of snapshots: \n");
scanf("%d",&nsnaps); 

         a = vector( 1, nwaters*nsnaps );
         c = matrix(1, nsnaps, 1, nwaters);

/**************************************************/

    i=1;

  while(fscanf(fd,"%lf",&a[i])!=EOF)
   {
             i=i+1;
    }
 fclose(fd);


k=1;
for(m=1;m<=nsnaps;m++)
    for(i=1;i<=nwaters;i++)
       { c[m][i]=a[k]; k=k+1;}

/**************************************************/

sifir = 0; bir = 0; iki = 0; uc = 0; dort = 0; bes = 0; alti = 0;
   
sprintf(foutput, "%s.degree.count", argv[1]);
outputf1=fopen(foutput,"w");

fprintf(outputf1,"Degrees: \n");

for(m=1;m<=nsnaps;m++)
   {
       
       for(atm=1;atm<=nwaters;atm=atm+1)
       {   
               if(c[m][atm] == 0) sifir = sifir +1;
               if(c[m][atm] == 1) bir = bir +1;
               if(c[m][atm] == 2) iki = iki +1;
               if(c[m][atm] == 3) uc = uc +1;
               if(c[m][atm] == 4) dort = dort +1;
               if(c[m][atm] == 5) bes = bes +1;
               if(c[m][atm] == 6) alti = alti +1;
       } 
       
       
       
   }   /* Closing big FOR */
   

/* Percentages */

total = sifir + bir + iki + uc + dort + bes + alti;

psifir = (sifir / total) * 100;
pbir = (bir / total) * 100;
piki = (iki / total) * 100;
puc = (uc / total) * 100;
pdort = (dort / total) * 100;
pbes = (bes / total) * 100;
palti = (alti / total) * 100;

/* Print counts and corresponding percentages */
   fprintf(outputf1,"Total = %.0lf\n",total);
   fprintf(outputf1,"Degree 0 : count = %.0lf  Percentage = %.1lf \n",sifir,psifir);
   fprintf(outputf1,"Degree 1 : count = %.0lf  Percentage = %.1lf \n",bir,pbir);
   fprintf(outputf1,"Degree 2 : count = %.0lf  Percentage = %.1lf \n",iki,piki);
   fprintf(outputf1,"Degree 3 : count = %.0lf  Percentage = %.1lf \n",uc,puc);
   fprintf(outputf1,"Degree 4 : count = %.0lf  Percentage = %.1lf \n",dort,pdort);
   fprintf(outputf1,"Degree 5 : count = %.0lf  Percentage = %.1lf \n",bes,pbes);
   fprintf(outputf1,"Degree 6 : count = %.0lf  Percentage = %.1lf \n",alti,palti); 

 fclose(outputf1);
 

 /* free memory */

  free_vector( a, 1, nwaters*nsnaps );
  free_matrix( c, 1, nsnaps, 1, nwaters );

  return 0;
}

int findf(FILE *fd, int n, ...){

  int  s;
  int  ret;
  char buf[1024];
  va_list args;
  char *tem[n];
 
  va_start(args,n);
  for(s=0;s<n;s++)
     tem[s]=va_arg(args,char*);
  va_end(args);
 
  for(s = 0; (ret=fscanf(fd, "%s", buf)) == 1; ){
 
    if( strncmp(buf,tem[s], 1024) == 0 ||
        strcmp("*",tem[s]) == 0) s++; else s = 0;
 
    if( s == n ) break;
  }
 
  return ret;

}


double *vector(int nl, int nh)
{
        double *v;

        v = (double *) malloc( (unsigned)(nh-nl+1) * sizeof(double) );
        if (!v)
                nrerror("\nallocation failure in vector()");
        return v-nl;
}
double **matrix(int nrl, int nrh, int ncl, int nch)
{
        int i;
        double **m;

        m = (double **) malloc( (unsigned)(nrh - nrl + 1) * sizeof(double *) );
        if (!m)
                nrerror("allocation failure 1 in matrix()");
        m -= nrl;

        for (i = nrl; i <= nrh; i++)
        {
                m[i] = (double *) malloc( (unsigned)(nch - ncl + 1)*sizeof(double) );
                if (!m[i])
                        nrerror("allocation failure 2 in matrix()");
                m[i] -= ncl;
        }
        return m;
}
void free_vector(double *v, int nl, int nh)
{
        free( (char *)(v + nl) );
}

void free_matrix(double **m, int nrl, int nrh, int ncl, int nch)
{
        int i;

        for (i = nrh; i >= nrl; i--)
                free( (char *)(m[i] + ncl) );
        free( (char *)(m + nrl) );
}
#undef NRANSI

void nrerror(char *error_text)
{
        fprintf(stderr, "Numerical Recipes run-time error...\n");
        fprintf(stderr, "%s\n", error_text);
        fprintf(stderr, "...now exiting to system...\n");
        exit(1);
}
