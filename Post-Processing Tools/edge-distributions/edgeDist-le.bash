#!/bin/bash

#control script for the Edge Distribution Fortran program
#made by L.Edens

date
echo "start"

#**********
#Default inputs: 
#Optional command line inputs follow the same order

# number of files
n=1

#erase previous output files (No=0/Yes=1)
erase=0

# turn off/on (0/1) the edge distribution count histogram output
efout=0

# turn off/on (0/1) th"e vertex count per node matrix output
edout=0

# turn off/on (0/1) the geodesic path and distance output
egout=1

# edge distribution program name
eDfile="edgeDist2.3.0.f90"

inputname1="water"
inputname2="pfile2"
inputname4="pfile4"
inputname5="water"

#reset input vlaues if entered on the command line
if [ $# -gt 0 ] && [ $# -lt 7 ]
then
   if [ $# -ge 1 ]; then
      n=$1
   fi
   if [ $# -ge 2 ]; then
      erase=$2
   fi
   if [ $# -ge 3 ]; then
      efout=$3
   fi
   if [ $# -ge 4 ]; then
      edout=$4
   fi
   if [ $# -ge 5 ]; then
      egout=$5
   fi
   if [ $# == 6 ]; then
      eDfile=$6
   fi
fi

if [ $# -ge 7 ]; then
   echo "Too many arguments. Using defaut inputs."
fi

#erase previous outputs
if [ $erase == 1 ]
then
   if [ $efout == 1 ]; then
      #find extension name 
      fext=$(grep -Eo "fext2 = '\.[0-Z]{1,10}" $eDfile | awk '{print $3}' | sed -r 's/^.{1}//')
      rm *$fext
   fi
   if [ $edout == 1 ]; then
      fext=$(grep -Eo "fext1 = '\.[0-Z]{1,10}" $eDfile | awk '{print $3}' | sed -r 's/^.{1}//')
      rm *$fext
   fi
   if [ $egout == 1 ]; then
      fext=$(grep -Eo "fext3 = '\.[0-Z]{1,10}" $eDfile | awk '{print $3}' | sed -r 's/^.{1}//')
      rm *$fext
   fi
fi


#find current file number (grep is required since number size is unknown)
oldn=$(grep -Eo 'mainLoop = [0-9]{1,10}' $eDfile | awk '{print $3}')

#update with new file number
sed -i "0,/mainLoop = $oldn/s//mainLoop = $n/" $eDfile

#update with new output control numbers 
sed -i "0,/efout = [0-9]/s//efout = $efout/" $eDfile
sed -i "0,/edout = [0-9]/s//edout = $edout/" $eDfile
sed -i "0,/egout = [0-9]/s//egout = $egout/" $eDfile

#find current file name (pfile1 cannot be greater than 20 characters) 
oldstr1=$(grep -Eo "pfile1 = '[0-Z]{1,20}.[0-Z]{1,20}" $eDfile | awk '{print $3}')
	
#update with new file name
sed -i "0,/pfile1 = $oldstr1/s//pfile1 = '$inputname1/" $eDfile

#find current file name (pfile2 cannot be greater than 20 characters) 
oldstr2=$(grep -Eo "pfile2 = '.[0-Z]{1,20}" $eDfile | awk '{print $3}')

#update with new file name
sed -i "0,/pfile2 = $oldstr2/s//pfile2 = '.$inputname2/" $eDfile

#find current file name (pfile4 cannot be greater than 20 characters) 
oldstr4=$(grep -Eo "pfile4 = '.[0-Z]{1,20}" $eDfile | awk '{print $3}')

#update with new file name
sed -i "0,/pfile4 = $oldstr4/s//pfile4 = '.$inputname4/" $eDfile

#find current file name (pfile5 cannot be greater than 20 characters) 
oldstr5=$(grep -Eo "pfile5 = '[0-Z]{1,20}" $eDfile | awk '{print $3}')

#update with new file name
sed -i "0,/pfile5 = $oldstr5/s//pfile5 = '$inputname5/" $eDfile

#recompile and run
gfortran $eDfile -o edge.exe
./edge.exe
rm edge.exe

date
echo "end"


