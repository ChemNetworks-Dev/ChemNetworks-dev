      PROGRAM isoGDhist
!*********************************************************************
!     
!      program to read isolated.gd and determine the histogram for you
!     
!*********************************************************************
      integer, parameter :: mxline = 60000000
      character*40 :: fname 
      character*10 :: dummy
      integer :: mxlen, icount, nline
      integer :: dum2, dum3
      integer, allocatable :: mxgd(:)
!
       open(1,file='isolated.gd')
       open(2,file='histogram')
       allocate(mxgd(mxline))
!
!***************************************************************
! Open relevant files for particular snapshot
! Will need to rename the geocard files to #.geocard

          mxlen=0
          do j=1, mxline
             read(1,*,end=200) dummy, mxgd(j)  !figure out how many lines in file
             if(mxgd(j).gt.mxlen) mxlen=mxgd(j) !figure out max gd length in whole set
          enddo
200       nline=j-1

          rewind(1)

          write(2,*)'max length of all gd=', mxlen
          do j=1,mxlen
            icount=0
            do i=1,nline
              if(mxgd(i).eq.j) icount=icount+1
            enddo
           write(2,*)'gd length = ', j, 'counts = ', icount
          enddo
 
       close (1)
       close (2)
      end PROGRAM isoGDhist
