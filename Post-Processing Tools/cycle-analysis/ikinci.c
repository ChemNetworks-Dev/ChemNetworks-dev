/****************************************
 *                                      *
 * Author: Abdullah Ozkanlar            *
 *         abdullah.ozkanlar@wsu.edu    *
 *                                      *
 * Washington State University          *
 * Chemistry Department                 *
 * A. Clark Research Group              *
 * Pullman, WA 99164                    *
 *                                      *
 ****************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "nrutil.h"
#define MAX 100000000
#define NRANSI
#define FLN 256

double *vector(int nl, int nh);
double **matrix(int nrl, int nrh, int ncl, int nch);
void free_vector(double *v, int nl, int nh);
void free_matrix(double **m, int nrl, int nrh, int ncl, int nch);


int main(int argc, char *argv[]){

       FILE *inputf;
       FILE *outputf;  

       char finput[FLN];      /* intput file name */
       FILE *fd;              /* file pointer */  

       char foutput[FLN];    

      int i,j,k,counter,t,cnt; 
      int z,r,s,znew; 
      double *a,*test,**m,**m2;
      char format[] = "%.8E ";  /* format of output */

    a = vector( 1, MAX );
// test = vector( 1, MAX );
    m = matrix(1, MAX/2, 1, 2);
   m2 = matrix(1, MAX/2, 1, 2);

/**************************************************/
  if(argc != 2){
    printf("Usuage: %s inputfile\n"
           "inputfile is assumed to have extension .input\n", argv[0]);

    exit(-1);
  }

/* sprintf(finput, "%s.input", argv[1]); */
sprintf(finput, "%s", argv[1]);

  if((fd=fopen(finput,"r"))==NULL){
    printf("Cannot open file %s\n", finput);
    exit(-1);
  }

/**************************************************/

    i=1;

  while(fscanf(fd,"%lf",&a[i])!=EOF)
   {
             i=i+1;
    }
 fclose(fd);

/******************************************************/

s=1;
for(z=1;z<=(i-1)/2;z++)
    for(r=1;r<=2;r++)
       { m[z][r]=a[s]; s=s+1;}


sprintf(foutput, "%s.output.ikinci", argv[1]);
outputf=fopen(foutput,"w");

znew = 1;
for(z=1;z<=(i-1)/2;z++)
{   cnt = 0;
     for(t=1;t<z;t++){
        if(m[t][1] == m[z][1] && m[t][2] == m[z][2]){cnt = cnt + 1;} 
     }

     if(cnt == 0) {m2[znew][1]=m[z][1]; m2[znew][2]=m[z][2]; znew = znew + 1;}  
   
}


for(z=1;z<znew;z++)
       fprintf(outputf,"%.0f %.0f\n",m2[z][1],m2[z][2]);
/*
for(z=1;z<=(i-1)/2;z++)
       fprintf(outputf,"%.0f %.0f\n",m2[z][1],m2[z][2]);
*/


fclose(outputf);

 /* free memory */

free_vector( a, 1, MAX );
// free_vector( test, 1, MAX );
free_matrix(m, 1, MAX/2, 1, 2);
free_matrix(m2, 1, MAX/2, 1, 2);

  return 0;
}


double *vector(int nl, int nh)
{
        double *v;

        v = (double *) malloc( (unsigned)(nh-nl+1) * sizeof(double) );
        if (!v)
                nrerror("\nallocation failure in vector()");
        return v-nl;
}
double **matrix(int nrl, int nrh, int ncl, int nch)
{
        int i;
        double **m;

        m = (double **) malloc( (unsigned)(nrh - nrl + 1) * sizeof(double *) );
        if (!m)
                nrerror("allocation failure 1 in matrix()");
        m -= nrl;

        for (i = nrl; i <= nrh; i++)
        {
                m[i] = (double *) malloc( (unsigned)(nch - ncl + 1)*sizeof(double) );
                if (!m[i])
                        nrerror("allocation failure 2 in matrix()");
                m[i] -= ncl;
        }
        return m;
}
void free_vector(double *v, int nl, int nh)
{
        free( (char *)(v + nl) );
}

void free_matrix(double **m, int nrl, int nrh, int ncl, int nch)
{
        int i;

        for (i = nrh; i >= nrl; i--)
                free( (char *)(m[i] + ncl) );
        free( (char *)(m + nrl) );
}

#undef NRANSI

void nrerror(char *error_text)
{
        fprintf(stderr, "Numerical Recipes run-time error...\n");
        fprintf(stderr, "%s\n", error_text);
        fprintf(stderr, "...now exiting to system...\n");
        exit(1);
}
