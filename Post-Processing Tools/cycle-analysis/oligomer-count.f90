      

!*********************************************************************
!     
!     program to read abdullah's water oligomer output files
!
!     aec
!     2012/07/18
!     
!*********************************************************************
      
      integer, parameter :: ntem = 31
      integer, parameter :: ntem2 = 32
      integer, parameter :: ntem3 = 33
      integer, parameter :: ntem4 = 34
      integer, parameter :: ntem5 = 35
      integer, parameter :: ntem6 = 36
      integer, parameter :: ntem8 = 38
      integer, parameter :: nf = 40000 !nf is total number of frames
      integer, parameter :: no = 600  !no is total number of oligomers per frames that can be counted
      integer, parameter :: nwat = 216  !this is the number of waters in the box
      character*60 :: fname
      character*17:: dummy
      character*25:: dummy2
      real*8, DIMENSION(nwat,nf) :: water
      integer, DIMENSION(nf,no,2) :: dimer
      integer, DIMENSION(nf,no,2) :: isodimer
      integer, DIMENSION(nf,no,3) :: trimer
      integer, DIMENSION(nf,no,3) :: isotrimer
      integer, DIMENSION(nf,no,4) :: tetramer
      integer, DIMENSION(nf,no,4) :: isotet
      integer, DIMENSION(nf,no,5) :: pentamer
      integer, DIMENSION(nf,no,5) :: isopent
      integer, DIMENSION(nf,no,6) :: bag
      integer, DIMENSION(nf,no,6) :: isobag
      integer, DIMENSION(nf,no,6) :: boat
      integer, DIMENSION(nf,no,6) :: isoboat
      integer, DIMENSION(nf,no,6) :: book
      integer, DIMENSION(nf,no,6) :: isobook
      integer, DIMENSION(nf,no,6) :: cage
      integer, DIMENSION(nf,no,6) :: isocage
      integer, DIMENSION(nf,no,6) :: chair
      integer, DIMENSION(nf,no,6) :: isochair
      integer, DIMENSION(nf,no,6) :: prism
      integer, DIMENSION(nf,no,6) :: isoprism
      integer, DIMENSION(nf,no,6) :: prismbook
      integer, DIMENSION(nf,no,6) :: isoprismbook
      integer, DIMENSION(nf,no,6) :: ring
      integer, DIMENSION(nf,no,6) :: isoring
      integer :: iconf, iter, ftype, icount, iolig,icount2, ihex,a
      real*8 :: percent,counts,frames,avg,hex,wat
  
! change ftype from 0 to 1 if you want to read isolated structure files
       ftype=0
!set entries for all oligomer matrices to zero
       do i=1,nf
          do k=1,no
            do j=1, 2
              dimer(i,k,j)=0
              isodimer(i,k,j)=0
            enddo
            do j=1,3
               trimer(i,k,j)=0
               isotrimer(i,k,j)=0
            enddo
            do j=1,4
               tetramer(i,k,j)=0
               isotet(i,k,j)=0
            enddo
            do j=1,5
               pentamer(i,k,j)=0
               isopent(i,k,j)=0
            enddo
            do j=1,6
               bag(i,k,j)=0
               isobag(i,k,j)=0
               book(i,k,j)=0
               isobook(i,k,j)=0
               boat(i,k,j)=0
               isoboat(i,k,j)=0
               cage(i,k,j)=0
               isocage(i,k,j)=0
               chair(i,k,j)=0
               isochair(i,k,j)=0
               prism(i,k,j)=0
               isoprism(i,k,j)=0
               prismbook(i,k,j)=0
               isoprismbook(i,k,j)=0
               ring(i,k,j)=0
               isoring(i,k,j)=0
            enddo
         enddo
      enddo

!*********************************************************************
! OPEN HEXAMER FILES
       open(7,file='HEXAMER.bags.total',status='old')
       open(8,file='HEXAMER.boats.total',status='old')
       open(9,file='HEXAMER.books.total',status='old')
       open(10,file='HEXAMER.cages.total',status='old')
       open(11,file='HEXAMER.chairs.total',status='old')
       open(12,file='HEXAMER.prisms.total',status='old')
       open(13,file='HEXAMER.prismbooks.total',status='old')
       open(14,file='HEXAMER.rings.total',status='old')
       open(15,file='PENTAMER.total',status='old')
       open(16,file='TETRAMER.total',status='old')
       open(17,file='TRIMER.total',status='old')
       open(30,file='DIMER.total',status='old')
! IF ISOLATED FILES EXIST, OPEN THEM
       if(ftype.eq.1) then
       open(18,file='HEXAMER.bags.isolated.total',status='old')
       open(19,file='HEXAMER.boats.isolated.total',status='old')
       open(20,file='HEXAMER.books.isolated.total',status='old')
       open(21,file='HEXAMER.cages.isolated.total',status='old')
       open(22,file='HEXAMER.chairs.isolated.total',status='old')
       open(23,file='HEXAMER.prisms.isolated.total',status='old')
       open(24,file='HEXAMER.prismbooks.isolated.total',status='old')
       open(25,file='HEXAMER.rings.isolated.total',status='old')
       open(26,file='PENTAMER.isolated.total',status='old')
       open(27,file='TETRAMER.isolated.total',status='old')
       open(28,file='TRIMER.isolated.total',status='old')
       open(29,file='DIMER.isolated.total',status='old')
       endif
       open(ntem,file='oligomer.count',recl=600)  
       open(ntem2,file='oligomer.stats',recl=600)
       open(ntem3,file='hexamer.lifetimes',recl=600)
       open(ntem4,file='pentamer.lifetimes',recl=600)
       open(ntem5,file='tetramer.lifetimes',recl=600)
       open(ntem6,file='trimer.lifetimes',recl=600)
       open(ntem8,file='water.tracking',recl=600)

!----------------------------BAGS-------------------------------------
       write(ntem,*) 'HEXAMER BAGS'
       write(ntem2,*) 'HEXAMER BAGS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do 
100         read(7,'(a17)',end=110) dummy  
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (7)
              iolig=1
              read(7,*,end=110) bag(iconf,iolig,1:6)  
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                bag(iconf,iolig,1:6)
              do i=1,no
                 read(7,'(a17)',end=110) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 100
                 backspace (7)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(7,*,end=110) bag(iconf,iolig,1:6)  
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 bag(iconf,iolig,1:6)
              enddo
            endif
           enddo
110    close (7)
            write(ntem2,*)'Number of frames with bags = ', icount
            write(ntem2,*)'Total hexamer bags=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with hexamers in them
            if(icount.eq.0) goto 120
              counts=icount  !number of frames with bags
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with bags', percent
! Calculate average # of hexamers observed               
              hex=ihex   !number of bags total
              avg=hex/counts        
              write(ntem2,*)'When a bag is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER BAGS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(bag(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
120       continue

!----------------------------ISOLATED BAGS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER BAGS'
         write(ntem2,*) 'ISOLATED HEXAMER BAGS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
200         read(18,'(a17)',end=210) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (18)
              iolig=1
              read(18,*,end=210) (isobag(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isobag(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(18,'(a17)',end=210) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 200
                 backspace (18)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(18,*,end=210) (isobag(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isobag(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
210    close (18)
            write(ntem2,*)'Number of frames with isolated                &
                 bags= ', icount
            write(ntem2,*)'Total # isolated bags =', ihex
            write(ntem2,*)'configurations counted = ', iconf
!*********************************************************************
            if(icount.eq.0) goto 220
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated 
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated bags', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of bags total
              avg=hex/counts
              write(ntem2,*)'When an isolated bag is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOBAGS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isobag(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
220          continue
         endif

!----------------------------BOATS-------------------------------------
       write(ntem,*) 'HEXAMER BOATS'
       write(ntem2,*) 'HEXAMER BOATS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
300         read(8,'(a17)',end=310) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (8)
              iolig=1
              read(8,*,end=310) (boat(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (boat(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(8,'(a17)',end=310) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 300
                 backspace (8)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(8,*,end=310) (boat(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (boat(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
310    close (8)
            write(ntem2,*)'Number of frames with boats = ', icount
            write(ntem2,*)'Total hexamer boats=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 320
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with boats
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with boats', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of boats total
              avg=hex/counts
              write(ntem2,*)'When a boat is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER BOATS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(boat(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
           enddo
320      continue

!----------------------------ISOLATED BOATS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER BOATS'
         write(ntem2,*) 'ISOLATED HEXAMER BOATS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
400         read(19,'(a17)',end=410) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (19)
              iolig=1
              read(19,*,end=410) isoboat(iconf,iolig,1:6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                isoboat(iconf,iolig,1:6)
              do i=1,no
                 read(19,'(a17)',end=410) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 400
                 backspace (19)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(19,*,end=410) isoboat(iconf,iolig,1:6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isoboat(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
410    close (19)
            write(ntem2,*)'Number of frames with isolated                &
                 boats= ', icount
            write(ntem2,*)'Total # isolated boats =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 420
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated boats', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of boats total
              avg=hex/counts
              write(ntem2,*)'When an isolated boat is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOBOATS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isoboat(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
           enddo
420      continue
         endif

!----------------------------BOOKS-------------------------------------
       write(ntem,*) 'HEXAMER BOOKS'
       write(ntem2,*) 'HEXAMER BOOKS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
500         read(9,'(a17)',end=510) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (9)
              iolig=1
              read(9,*,end=510) (book(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (book(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(9,'(a17)',end=510) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 500
                 backspace (9)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(9,*,end=510) (book(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (book(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
510    close (9)
            write(ntem2,*)'Number of frames with books = ', icount
            write(ntem2,*)'Total hexamer books=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 520
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with books
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with books', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of books total
              avg=hex/counts
              write(ntem2,*)'When a book is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER BOOKS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(book(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
           enddo
520        continue

!----------------------------ISOLATED BOOKS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER BOOKS'
         write(ntem2,*) 'ISOLATED HEXAMER BOOKS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
600         read(20,'(a17)',end=610) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (20)
              iolig=1
              read(20,*,end=610) (isobook(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isobook(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(20,'(a17)',end=610) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 600
                 backspace (20)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(20,*,end=610) (isobook(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isobook(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
610    close (20)
            write(ntem2,*)'Number of frames with isolated                &
                 books= ', icount
            write(ntem2,*)'Total # isolated books =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 620
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated books', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of books total
              avg=hex/counts
              write(ntem2,*)'When an isolated book is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOBOOKS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isobook(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
         enddo
620      continue
         endif

!----------------------------CAGES-------------------------------------
       write(ntem,*) 'HEXAMER CAGES'
       write(ntem2,*) 'HEXAMER CAGES'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
700         read(10,'(a17)',end=710) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (10)
              iolig=1
              read(10,*,end=710) (cage(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (cage(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(10,'(a17)',end=710) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 700
                 backspace (10)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(10,*,end=710) (cage(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (cage(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
710    close (10)
            write(ntem2,*)'Number of frames with cages = ', icount
            write(ntem2,*)'Total hexamer cages=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with hexamers in them
              if(icount.eq.0) goto 720
              counts=icount  !number of frames with cages
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with cages', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of cages total
              avg=hex/counts
              write(ntem2,*)'When a cage is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER CAGES'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(cage(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
         enddo
720      continue

!----------------------------ISOLATED CAGES-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER CAGES'
         write(ntem2,*) 'ISOLATED HEXAMER CAGES'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
800         read(21,'(a17)',end=810) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (21)
              iolig=1
              read(21,*,end=810) (isocage(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isocage(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(21,'(a17)',end=810) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 800
                 backspace (21)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(21,*,end=810) (isocage(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isocage(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
810    close (21)
            write(ntem2,*)'Number of frames with isolated                &
                 cages= ', icount
            write(ntem2,*)'Total # isolated cages =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 820
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated cages', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of cages total
              avg=hex/counts
              write(ntem2,*)'When an isolated cage is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOCAGES'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isocage(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
         enddo
820      continue
         endif

!----------------------------CHAIR-------------------------------------
       write(ntem,*) 'HEXAMER CHAIR'
       write(ntem2,*) 'HEXAMER CHAIR'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
900         read(11,'(a17)',end=910) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (11)
              iolig=1
              read(11,*,end=910) (chair(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (chair(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(11,'(a17)',end=910) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 900
                 backspace (11)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(11,*,end=910) (chair(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (chair(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
910    close (11)
            write(ntem2,*)'Number of frames with chairs = ', icount
            write(ntem2,*)'Total hexamer chairs=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 920
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with chairs
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with chairs', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of chairs total
              avg=hex/counts
              write(ntem2,*)'When a chair is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER CHAIR'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(chair(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
         enddo
920      continue

!----------------------------ISOLATED CHAIRS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER CHAIRS'
         write(ntem2,*) 'ISOLATED HEXAMER CHAIRS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1000         read(22,'(a17)',end=1010) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (22)
              iolig=1
              read(22,*,end=1010) (isochair(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isochair(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(22,'(a17)',end=1010) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1000
                 backspace (22)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(22,*,end=1010) (isochair(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isochair(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1010    close (22)
            write(ntem2,*)'Number of frames with isolated                &
                 chairs= ', icount
            write(ntem2,*)'Total # isolated chairs =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1020
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated chairs', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of chairs total
              avg=hex/counts
              write(ntem2,*)'When an isolated chair is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOCHAIR'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isochair(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1020      continue
          endif

!----------------------------PRISM-------------------------------------
       write(ntem,*) 'HEXAMER PRISM'
       write(ntem2,*) 'HEXAMER PRISM'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1100         read(12,'(a17)',end=1110) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (12)
              iolig=1
              read(12,*,end=1110) (prism(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (prism(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(12,'(a17)',end=1110) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1100
                 backspace (12)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(12,*,end=1110) (prism(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (prism(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1110    close (12)
            write(ntem2,*)'Number of frames with prisms = ', icount
            write(ntem2,*)'Total hexamer prisms=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1120
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with prisms
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with prisms', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of prisms total
              avg=hex/counts
              write(ntem2,*)'When a prism is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER PRISM'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(prism(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1120      continue

!----------------------------ISOLATED PRISMBOOKS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER PRISMBOOKS'
         write(ntem2,*) 'ISOLATED HEXAMER PRISMBOOKS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1200         read(23,'(a17)',end=1210) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (23)
              iolig=1
              read(23,*,end=1210) (isoprism(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isoprism(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(23,'(a17)',end=1210) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1200
                 backspace (23)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(23,*,end=1210) (isoprism(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isoprism(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1210    close (23)
            write(ntem2,*)'Number of frames with isolated                &
                 prisms= ', icount
            write(ntem2,*)'Total # isolated prisms =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1220
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated prisms', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of prisms total
              avg=hex/counts
           write(ntem2,*)'When an isolated prism is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOPRISM'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isoprism(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1220      continue
          endif

!----------------------------PRISMBOOK-------------------------------------
       write(ntem,*) 'HEXAMER PRISMBOOK'
       write(ntem2,*) 'HEXAMER PRISMBOOK'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1300         read(13,'(a17)',end=1310) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (13)
              iolig=1
              read(13,*,end=1310) (prismbook(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (prismbook(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(13,'(a17)',end=1310) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1300
                 backspace (13)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(13,*,end=1310) (prismbook(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (prismbook(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1310    close (13)
            write(ntem2,*)'Number of frames with prismbooks = ', icount
            write(ntem2,*)'Total hexamer prismbooks=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1320
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with prismbooks
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with prismbooks', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of prismbooks total
              avg=hex/counts
              write(ntem2,*)'When a prismbook is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER PRISMBOOK'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(prismbook(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1320      continue

!----------------------------ISOLATED PRISMBOOKS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER PRISMBOOKS'
         write(ntem2,*) 'ISOLATED HEXAMER PRISMBOOKS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1400         read(24,'(a17)',end=1410) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (24)
              iolig=1
            read(24,*,end=1410) (isoprismbook(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isoprismbook(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(24,'(a17)',end=1410) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1400
                 backspace (24)
                 iolig=iolig+1
                 ihex=ihex+1
              read(24,*,end=1410) (isoprismbook(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isoprismbook(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1410    close (24)
            write(ntem2,*)'Number of frames with isolated                &
                 prismbooks= ', icount
            write(ntem2,*)'Total # isolated prismbooks =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1420
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated prismbooks', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of prismbooks total
              avg=hex/counts
              write(ntem2,*)'When an isolated prismbook is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISOPRISMBOOK'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isoprismbook(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1420      continue
          endif

!----------------------------RINGS-------------------------------------
       write(ntem,*) 'HEXAMER RINGS'
       write(ntem2,*) 'HEXAMER RINGS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1500         read(14,'(a17)',end=1510) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (14)
              iolig=1
              read(14,*,end=1510) (ring(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (ring(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(14,'(a17)',end=1510) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1500
                 backspace (14)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(14,*,end=1510) (ring(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (ring(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1510    close (14)
            write(ntem2,*)'Number of frames with rings = ', icount
            write(ntem2,*)'Total hexamer rings=', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1520
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with rings
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with rings', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of rings total
              avg=hex/counts
              write(ntem2,*)'When a ring is present, what is the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER RINGS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(ring(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1520      continue

!----------------------------ISOLATED RINGS-------------------------------------
         if(ftype.eq.1) then
         write(ntem,*) 'ISOLATED HEXAMER RINGS'
         write(ntem2,*) 'ISOLATED HEXAMER RINGS'
         iconf = 0
         icount = 0 !number of frames with hexamers in them
         ihex = 0   !total number of hexamers counted
         iolig = 0
           do
1600         read(25,'(a17)',end=1610) dummy
            if(dummy == 'Snapshot hexamers')  then
               iconf = iconf + 1
            else
              backspace (25)
              iolig=1
            read(25,*,end=1610) (isoring(iconf,iolig,k), k=1,6)
              icount = icount + 1
              ihex=ihex+1
!*********************************************************************
              write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                (isoring(iconf,iolig,i),i=1,6)
              do i=1,no
                 read(25,'(a17)',end=1610) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot hexamers')  iconf=iconf+1
                 if(dummy == 'Snapshot hexamers') goto 1600
                 backspace (25)
                 iolig=iolig+1
                 ihex=ihex+1
              read(25,*,end=1610) (isoring(iconf,iolig,k), k=1,6)
                 write(ntem,*)'Frame #', iconf,'W1-6=',                  &
                 (isoring(iconf,iolig,k),k=1,6)
              enddo
            endif
           enddo
1610    close (25)
            write(ntem2,*)'Number of frames with isolated                &
                 rings= ', icount
            write(ntem2,*)'Total # isolated rings =', ihex
            write(ntem2,*)'configurations counted = ', iconf
            if(icount.eq.0) goto 1620
!*********************************************************************
! Calculated % frames with hexamers in them
              counts=icount  !number of frames with isolated
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isolated rings', percent
! Calculate average # of hexamers observed
              hex=ihex   !number of rings total
              avg=hex/counts
              write(ntem2,*)'When an isolated ring is present, what is        &
                 the average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem3,*) 'HEXAMER ISORINGS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isoring(i,j,1:6))
             if(j.eq.1) write(ntem3,*) 'Frame #', i, 'j=', j,          &
                   'Hexamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem3,*) 'Frame #', i, 'j=', j,             &
                       'Hexamer #', a
                  endif
                endif
             enddo
          enddo
1620      continue
          endif

!----------------------------PENTAMERS-------------------------------------
       write(ntem,*) 'PENTAMERS'
       write(ntem2,*) 'PENTAMERS'
         iconf = 0
         icount = 0 !number of frames with pentamers in them
         ihex = 0   !total number of pentamers counted
         iolig = 0
           do
1700         read(15,'(a17)',end=1710) dummy
            if(dummy == 'Snapshot pentamer')  then
               iconf = iconf + 1
            else
              backspace (15)
              iolig=1
              read(15,*,end=1710) (pentamer(iconf,iolig,k), k=1,5)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-5=',                  &
                (pentamer(iconf,iolig,k),k=1,5)
              do i=1,no
                 read(15,'(a17)',end=1710) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot pentamer')  iconf=iconf+1
                 if(dummy == 'Snapshot pentamer') goto 1700
                 backspace (15)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(15,*,end=1710) (pentamer(iconf,iolig,k), k=1,5)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-5=',                  &
                 (pentamer(iconf,iolig,k),k=1,5)
              enddo
            endif
           enddo
1710    close (15)
          write(ntem2,*)'Number of frames with pentamers = ', icount
            write(ntem2,*)'Total hexamer pentamers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with pentamers in them
            if(icount.eq.0) goto 1720
              counts=icount  !number of frames with pentamers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with pentamers', percent
! Calculate average # of pentamers observed
!*********************************************************************
              hex=ihex   !number of pentamers total
              avg=hex/counts
       write(ntem2,*)'When a pentamer is present, what is the average   &
               # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem4,*) 'PENTAMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(pentamer(i,j,1:5))
             if(j.eq.1) write(ntem4,*) 'Frame #', i, 'j=', j,          &
                   'Pentamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem4,*) 'Frame #', i, 'j=', j,             &
                       'Pentamer #', a
                  endif
                endif
             enddo
           enddo
1720       continue

!----------------------------ISOLATED PENTAMERS-------------------------------------
       if(ftype.eq.1) then
       write(ntem,*) 'ISOLATED PENTAMERS'
       write(ntem2,*) 'ISOLATED PENTAMERS'
         iconf = 0
         icount = 0 !number of frames with pentamers in them
         ihex = 0   !total number of pentamers counted
         iolig = 0
           do
1800         read(26,'(a17)',end=1810) dummy
            if(dummy == 'Snapshot pentamer')  then
               iconf = iconf + 1
            else
              backspace (26)
              iolig=1
              read(26,*,end=1810) (isopent(iconf,iolig,k), k=1,5)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-5=',                  &
                (isopent(iconf,iolig,k),k=1,5)
              do i=1,no
                 read(26,'(a17)',end=1810) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot pentamer')  iconf=iconf+1
                 if(dummy == 'Snapshot pentamer') goto 1800
                 backspace (26)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(26,*,end=1810) (isopent(iconf,iolig,k), k=1,5)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-5=',                  &
                 (isopent(iconf,iolig,k),k=1,5)
              enddo
            endif
           enddo
1810    close (26)
        write(ntem2,*)'Number of frames with isopentamers = ', icount
            write(ntem2,*)'Total isopentamers counted=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with pentamers in them
            if(icount.eq.0) goto 1820
              counts=icount  !number of frames with pentamers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isopentamers', percent
! Calculate average # of pentamers observed
!*********************************************************************
           hex=ihex   !number of isopentamers total
           avg=hex/counts
    write(ntem2,*)'When a isopentamer is present, what is the average   &
             # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem4,*) 'PENTAMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isopent(i,j,1:5))
             if(j.eq.1) write(ntem4,*) 'Frame #', i, 'j=', j,          &
                   'Pentamer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem4,*) 'Frame #', i, 'j=', j,             &
                       'Pentamer #', a
                  endif
                endif
             enddo
           enddo
1820       continue
           endif

!----------------------------TETRAMERS-------------------------------------
       write(ntem,*) 'TETRAMERS'
       write(ntem2,*) 'TETRAMERS'
         iconf = 0
         icount = 0 !number of frames with tetramers in them
         ihex = 0   !total number of tetramers counted
         iolig = 0
           do
1900         read(16,'(a17)',end=1910) dummy
            if(dummy == 'Snapshot tetramer')  then
               iconf = iconf + 1
            else
              backspace (16)
              iolig=1
              read(16,*,end=1910) (tetramer(iconf,iolig,k), k=1,4)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-4=',                  &
                (tetramer(iconf,iolig,k),k=1,4)
              do i=1,no
                 read(16,'(a17)',end=1910) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot tetramer')  iconf=iconf+1
                 if(dummy == 'Snapshot tetramer') goto 1900
                 backspace (16)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(16,*,end=1910) (tetramer(iconf,iolig,k), k=1,4)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-4=',                  &
                 (tetramer(iconf,iolig,k),k=1,4)
              enddo
            endif
           enddo
1910    close (16)
          write(ntem2,*)'Number of frames with tetramers = ', icount
            write(ntem2,*)'Total count tetramers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with tetramers in them
            if(icount.eq.0) goto 1920
              counts=icount  !number of frames with tetramers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with tetramers', percent
! Calculate average # of tetramers observed
!*********************************************************************
              hex=ihex   !number of tetramers total
              avg=hex/counts
           write(ntem2,*)'When a tetramer is present, what is the       &
                average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem5,*) 'TETRAMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(tetramer(i,j,1:4))
             if(j.eq.1) write(ntem5,*) 'Frame #', i, 'j=', j,          &
                   'Tetramer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem5,*) 'Frame #', i, 'j=', j,             &
                       'Tetramer #', a
                  endif
                endif
             enddo
           enddo
1920       continue

!----------------------------ISOLATED TETRAMERS-------------------------------------
       if(ftype.eq.1) then
       write(ntem,*) 'ISOLATED TETRAMERS'
       write(ntem2,*) 'ISOLATED TETRAMERS'
         iconf = 0
         icount = 0 !number of frames with tetramers in them
         ihex = 0   !total number of tetramers counted
         iolig = 0
           do
2000         read(27,'(a17)',end=2010) dummy
            if(dummy == 'Snapshot tetramer')  then
               iconf = iconf + 1
            else
              backspace (27)
              iolig=1
              read(27,*,end=2010) (isotet(iconf,iolig,k), k=1,4)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-4=',                  &
                (isotet(iconf,iolig,k),k=1,4)
              do i=1,no
                 read(27,'(a17)',end=2010) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot tetramer')  iconf=iconf+1
                 if(dummy == 'Snapshot tetramer') goto 2000
                 backspace (27)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(27,*,end=2010) (isotet(iconf,iolig,k), k=1,4)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-4=',                  &
                 (isotet(iconf,iolig,k),k=1,4)
              enddo
            endif
           enddo
2010    close (27)
          write(ntem2,*)'Number of frames with isolated tetramers        &
              = ', icount
            write(ntem2,*)'Total count isolated tetramers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with tetramers in them
            if(icount.eq.0) goto 2020
              counts=icount  !number of frames with tetramers
              frames=iconf
              percent=(counts/frames)*100
          write(ntem2,*) '% Frames with isolated tetramers', percent
! Calculate average # of tetramers observed
!*********************************************************************
              hex=ihex   !number of tetramers total
              avg=hex/counts
           write(ntem2,*)'When a tetramer is present, what is the       &
                average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem5,*) 'ISOTETRAMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isotet(i,j,1:4))
             if(j.eq.1) write(ntem5,*) 'Frame #', i, 'j=', j,          &
                   'Tetramer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem5,*) 'Frame #', i, 'j=', j,             &
                       'Tetramer #', a
                  endif
                endif
             enddo
           enddo
2020       continue
           endif
 
!----------------------------TRIMERS-------------------------------------
       write(ntem,*) 'TRIMERS'
       write(ntem2,*) 'TRIMERS'
         iconf = 0
         icount = 0 !number of frames with trimers in them
         ihex = 0   !total number of trimers counted
         iolig = 0
           do
2100         read(17,'(a15)',end=2110) dummy
            if(dummy == 'Snapshot trimer')  then
               iconf = iconf + 1
            else
              backspace (17)
              iolig=1
              read(17,*,end=2110) (trimer(iconf,iolig,k), k=1,3)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-3=',                  &
                (trimer(iconf,iolig,k),k=1,3)
              do i=1,no
                 read(17,'(a15)',end=2110) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot trimer')  iconf=iconf+1
                 if(dummy == 'Snapshot trimer') goto 2100
                 backspace (17)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(17,*,end=2110) (trimer(iconf,iolig,k), k=1,3)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-3=',                  &
                 (trimer(iconf,iolig,k),k=1,3)
              enddo
            endif
           enddo
2110    close (17)
          write(ntem2,*)'Number of frames with trimers = ', icount
            write(ntem2,*)'Total count trimers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with trimers in them
            if(icount.eq.0) goto 2120
              counts=icount  !number of frames with trimers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with trimers', percent
! Calculate average # of trimers observed
!*********************************************************************
              hex=ihex   !number of trimers total
              avg=hex/counts
           write(ntem2,*)'When a trimer is present, what is the       &
                average   &
                # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem6,*) 'TRIMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(trimer(i,j,1:3))
             if(j.eq.1) write(ntem6,*) 'Frame #', i, 'j=', j,          &
                   'Trimer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem6,*) 'Frame #', i, 'j=', j,             &
                       'Trimer #', a
                  endif
                endif
             enddo
           enddo
2120       continue

       if(ftype.eq.1) then
!----------------------------ISOLATED TRIMERS-------------------------------------
       write(ntem,*) 'ISOLATED TRIMERS'
       write(ntem2,*) 'ISOLATED TRIMERS'
         iconf = 0
         icount = 0 !number of frames with isotrimers in them
         ihex = 0   !total number of trimers counted
         iolig = 0
           do
2200         read(28,'(a15)',end=2210) dummy
            if(dummy == 'Snapshot trimer')  then
               iconf = iconf + 1
            else
              backspace (28)
              iolig=1
              read(28,*,end=2210) (isotrimer(iconf,iolig,k), k=1,3)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-3=',                  &
                (isotrimer(iconf,iolig,k),k=1,3)
              do i=1,no
                 read(28,'(a15)',end=2210) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot trimer')  iconf=iconf+1
                 if(dummy == 'Snapshot trimer') goto 2200
                 backspace (28)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(28,*,end=2210) (isotrimer(iconf,iolig,k), k=1,3)
!*********************************************************************
                 write(ntem,*)'Frame #', iconf,'W1-3=',                  &
                 (isotrimer(iconf,iolig,k),k=1,3)
              enddo
            endif
           enddo
2210    close (28)
        write(ntem2,*)'Number of frames with isotrimers = ', icount
            write(ntem2,*)'Total count isotrimers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with trimers in them
            if(icount.eq.0) goto 2220
              counts=icount  !number of frames with trimers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isotrimers', percent
! Calculate average # of trimers observed
!*********************************************************************
              hex=ihex   !number of trimers total
              avg=hex/counts
           write(ntem2,*)'When a isotrimer is present, what is the       &
               average   &
               # observed in a frame?', avg
!*********************************************************************
! Write file hexamer.lifetime for abdullah to calulate lifetimes
       write(ntem6,*) 'ISOTRIMERS'
          do i=1,iconf
             do j=1,no !max number of hexamer bags in a box
                  a=sum(isotrimer(i,j,1:3))
             if(j.eq.1) write(ntem6,*) 'Frame #', i, 'j=', j,          &
                   'Trimer #', a
                if(j.ne.1) then
                  if(a.ne.0) then 
                     write(ntem6,*) 'Frame #', i, 'j=', j,             &
                       'Trimer #', a
                  endif
                endif
             enddo
           enddo
2220       continue
           endif

!----------------------------DIMERS-------------------------------------
       write(ntem,*) 'DIMERS'
       write(ntem2,*) 'DIMERS'
         iconf = 0
         icount = 0 !number of frames with dimers in them
         ihex = 0   !total number of dimers counted
         iolig = 0
           do
2300         read(30,'(a14)',end=2310) dummy
            if(dummy == 'Snapshot graph')  then
               iconf = iconf + 1
            else
              backspace (30)
              iolig=1
              read(30,*,end=2310) (dimer(iconf,iolig,k), k=1,2)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-2=',                  &
                (dimer(iconf,iolig,k),k=1,2)
              do i=1,no
                 read(30,'(a14)',end=2310) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot graph')  iconf=iconf+1
                 if(dummy == 'Snapshot graph') goto 2300
                 backspace (30)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(30,*,end=2310) (dimer(iconf,iolig,k), k=1,2)
!*********************************************************************
                   write(ntem,*)'Frame #', iconf,'W1-2=',                  &
                   (dimer(iconf,iolig,k),k=1,2)
              enddo
            endif
           enddo
2310    close (30)
        write(ntem2,*)'Number of frames with dimers = ', icount
            write(ntem2,*)'Total count dimers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with dimers in them
            if(icount.eq.0) goto 2320
              counts=icount  !number of frames with dimers
              frames=iconf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with dimers', percent
! Calculate average # of dimers observed
!*********************************************************************
              hex=ihex   !number of dimers total
              avg=hex/counts
           write(ntem2,*)'When a dimer is present, what is the       &
               average   &
               # observed in a frame?', avg

2320     continue

!----------------------------ISOLATED DIMERS-------------------------------------
         if(ftype.eq.1) then
       write(ntem,*) 'ISOLATED DIMERS'
       write(ntem2,*) 'ISOLATED DIMERS'
         iconf = 0
         icount = 0 !number of frames with isodimers in them
         ihex = 0   !total number of dimers counted
         iolig = 0
           do
2400         read(29,'(a14)',end=2410) dummy
            if(dummy == 'Snapshot graph')  then
               iconf = iconf + 1
            else
              backspace (29)
              iolig=1
              read(29,*,end=2410) (isodimer(iconf,iolig,k), k=1,2)
              icount = icount + 1
              ihex=ihex+1
              write(ntem,*)'Frame #', iconf,'W1-2=',                  &
                (isodimer(iconf,iolig,k),k=1,2)
              do i=1,no
                 read(29,'(a14)',end=2410) dummy      !ignores the isomorph of the first hexamer
                 if(dummy == 'Snapshot graph')  iconf=iconf+1
                 if(dummy == 'Snapshot graph') goto 2400
                 backspace (29)
                 iolig=iolig+1
                 ihex=ihex+1
                 read(29,*,end=2410) (isodimer(iconf,iolig,k), k=1,2)
!*********************************************************************
                   write(ntem,*)'Frame #', iconf,'W1-2=',                  &
                   (isodimer(iconf,iolig,k),k=1,2)
              enddo
            endif
           enddo
2410    close (29)
        write(ntem2,*)'Number of frames with isodimers = ', icount
            write(ntem2,*)'Total count isodimers=', ihex
            write(ntem2,*)'configurations counted = ', iconf
! Calculated % frames with dimers in them
            if(icount.eq.0) goto 2420
              counts=icount  !number of frames with dimers
              frames=nf
              percent=(counts/frames)*100
              write(ntem2,*) '% Frames with isodimers', percent
! Calculate average # of dimers observed
!*********************************************************************
              hex=ihex   !number of dimers total
              avg=hex/counts
           write(ntem2,*)'When a isodimer is present, what is the       &
               average   &
               # observed in a frame?', avg
           endif

2420       continue
!*********NOW DETERMINE WHAT HAPPENS FOR EVERY WATER IN THE BOX*****

          do i=1,nwat
             do j=1,iconf
                water(i,j)=0
             enddo
          enddo

!*********************************************************************

          do i=1, nwat
             do j=1,iconf
               do k=1,no
               if(dimer(j,k,1).eq.i) water(i,j)=2.0d0
               if(dimer(j,k,2).eq.i) water(i,j)=2.0d0
               if(isodimer(j,k,1).eq.i) water(i,j)=2.5d0
               if(isodimer(j,k,2).eq.i) water(i,j)=2.5d0
               enddo
               do k=1,no
               if(trimer(j,k,1).eq.i) water(i,j)=3.0d0
               if(trimer(j,k,2).eq.i) water(i,j)=3.0d0
               if(trimer(j,k,3).eq.i) water(i,j)=3.0d0
               if(isotrimer(j,k,1).eq.i) water(i,j)=3.5d0
               if(isotrimer(j,k,2).eq.i) water(i,j)=3.5d0
               if(isotrimer(j,k,3).eq.i) water(i,j)=3.5d0
               enddo
               do k=1,no
               if(tetramer(j,k,1).eq.i) water(i,j)=4.0d0
               if(tetramer(j,k,2).eq.i) water(i,j)=4.0d0
               if(tetramer(j,k,3).eq.i) water(i,j)=4.0d0
               if(tetramer(j,k,4).eq.i) water(i,j)=4.0d0
               if(isotet(j,k,1).eq.i) water(i,j)=4.5d0
               if(isotet(j,k,2).eq.i) water(i,j)=4.5d0
               if(isotet(j,k,3).eq.i) water(i,j)=4.5d0
               if(isotet(j,k,4).eq.i) water(i,j)=4.5d0
               enddo
               do k=1,no 
               if(pentamer(j,k,1).eq.i) water(i,j)=5.0d0
               if(pentamer(j,k,2).eq.i) water(i,j)=5.0d0
               if(pentamer(j,k,3).eq.i) water(i,j)=5.0d0
               if(pentamer(j,k,4).eq.i) water(i,j)=5.0d0
               if(pentamer(j,k,5).eq.i) water(i,j)=5.0d0
               if(isopent(j,k,1).eq.i) water(i,j)=5.5d0
               if(isopent(j,k,2).eq.i) water(i,j)=5.5d0
               if(isopent(j,k,3).eq.i) water(i,j)=5.5d0
               if(isopent(j,k,4).eq.i) water(i,j)=5.5d0
               if(isopent(j,k,5).eq.i) water(i,j)=5.5d0
               enddo
               do k=1,no
               if(bag(j,1,1).eq.i) water(i,j)=6.0d0
               if(bag(j,1,2).eq.i) water(i,j)=6.0d0
               if(bag(j,1,3).eq.i) water(i,j)=6.0d0
               if(bag(j,1,4).eq.i) water(i,j)=6.0d0
               if(bag(j,1,5).eq.i) water(i,j)=6.0d0
               if(bag(j,1,6).eq.i) water(i,j)=6.0d0
               if(isobag(j,1,1).eq.i) water(i,j)=6.5d0
               if(isobag(j,1,2).eq.i) water(i,j)=6.5d0
               if(isobag(j,1,3).eq.i) water(i,j)=6.5d0
               if(isobag(j,1,4).eq.i) water(i,j)=6.5d0
               if(isobag(j,1,5).eq.i) water(i,j)=6.5d0
               if(isobag(j,1,6).eq.i) water(i,j)=6.5d0
               if(boat(j,1,1).eq.i) water(i,j)=6.0d0
               if(boat(j,1,2).eq.i) water(i,j)=6.0d0
               if(boat(j,1,3).eq.i) water(i,j)=6.0d0
               if(boat(j,1,4).eq.i) water(i,j)=6.0d0
               if(boat(j,1,5).eq.i) water(i,j)=6.0d0
               if(boat(j,1,6).eq.i) water(i,j)=6.0d0
               if(isoboat(j,1,1).eq.i) water(i,j)=6.5d0
               if(isoboat(j,1,2).eq.i) water(i,j)=6.5d0
               if(isoboat(j,1,3).eq.i) water(i,j)=6.5d0
               if(isoboat(j,1,4).eq.i) water(i,j)=6.5d0
               if(isoboat(j,1,5).eq.i) water(i,j)=6.5d0
               if(isoboat(j,1,6).eq.i) water(i,j)=6.5d0
               if(book(j,1,1).eq.i) water(i,j)=6.0d0
               if(book(j,1,2).eq.i) water(i,j)=6.0d0
               if(book(j,1,3).eq.i) water(i,j)=6.0d0
               if(book(j,1,4).eq.i) water(i,j)=6.0d0
               if(book(j,1,5).eq.i) water(i,j)=6.0d0
               if(book(j,1,6).eq.i) water(i,j)=6.0d0
               if(isobook(j,1,1).eq.i) water(i,j)=6.5d0
               if(isobook(j,1,2).eq.i) water(i,j)=6.5d0
               if(isobook(j,1,3).eq.i) water(i,j)=6.5d0
               if(isobook(j,1,4).eq.i) water(i,j)=6.5d0
               if(isobook(j,1,5).eq.i) water(i,j)=6.5d0
               if(isobook(j,1,6).eq.i) water(i,j)=6.5d0
               if(cage(j,1,1).eq.i) water(i,j)=6.0d0
               if(cage(j,1,2).eq.i) water(i,j)=6.0d0
               if(cage(j,1,3).eq.i) water(i,j)=6.0d0
               if(cage(j,1,4).eq.i) water(i,j)=6.0d0
               if(cage(j,1,5).eq.i) water(i,j)=6.0d0
               if(cage(j,1,6).eq.i) water(i,j)=6.0d0
               if(isocage(j,1,1).eq.i) water(i,j)=6.50d0
               if(isocage(j,1,2).eq.i) water(i,j)=6.50d0
               if(isocage(j,1,3).eq.i) water(i,j)=6.50d0
               if(isocage(j,1,4).eq.i) water(i,j)=6.50d0
               if(isocage(j,1,5).eq.i) water(i,j)=6.50d0
               if(isocage(j,1,6).eq.i) water(i,j)=6.50d0
               if(chair(j,1,1).eq.i) water(i,j)=6.0d0
               if(chair(j,1,2).eq.i) water(i,j)=6.0d0
               if(chair(j,1,3).eq.i) water(i,j)=6.0d0
               if(chair(j,1,4).eq.i) water(i,j)=6.0d0
               if(chair(j,1,5).eq.i) water(i,j)=6.0d0
               if(chair(j,1,6).eq.i) water(i,j)=6.0d0
               if(isochair(j,1,1).eq.i) water(i,j)=6.5d0
               if(isochair(j,1,2).eq.i) water(i,j)=6.5d0
               if(isochair(j,1,3).eq.i) water(i,j)=6.5d0
               if(isochair(j,1,4).eq.i) water(i,j)=6.5d0
               if(isochair(j,1,5).eq.i) water(i,j)=6.5d0
               if(isochair(j,1,6).eq.i) water(i,j)=6.5d0
               if(prism(j,1,1).eq.i) water(i,j)=6.0d0
               if(prism(j,1,2).eq.i) water(i,j)=6.0d0
               if(prism(j,1,3).eq.i) water(i,j)=6.0d0
               if(prism(j,1,4).eq.i) water(i,j)=6.0d0
               if(prism(j,1,5).eq.i) water(i,j)=6.0d0
               if(prism(j,1,6).eq.i) water(i,j)=6.0d0
               if(isoprism(j,1,1).eq.i) water(i,j)=6.5d0
               if(isoprism(j,1,2).eq.i) water(i,j)=6.5d0
               if(isoprism(j,1,3).eq.i) water(i,j)=6.5d0
               if(isoprism(j,1,4).eq.i) water(i,j)=6.5d0
               if(isoprism(j,1,5).eq.i) water(i,j)=6.5d0
               if(isoprism(j,1,6).eq.i) water(i,j)=6.5d0
               if(prismbook(j,1,1).eq.i) water(i,j)=6.0d0
               if(prismbook(j,1,2).eq.i) water(i,j)=6.0d0
               if(prismbook(j,1,3).eq.i) water(i,j)=6.0d0
               if(prismbook(j,1,4).eq.i) water(i,j)=6.0d0
               if(prismbook(j,1,5).eq.i) water(i,j)=6.0d0
               if(prismbook(j,1,6).eq.i) water(i,j)=6.0d0
               if(isoprismbook(j,1,1).eq.i) water(i,j)=6.5d0
               if(isoprismbook(j,1,2).eq.i) water(i,j)=6.5d0
               if(isoprismbook(j,1,3).eq.i) water(i,j)=6.5d0
               if(isoprismbook(j,1,4).eq.i) water(i,j)=6.5d0
               if(isoprismbook(j,1,5).eq.i) water(i,j)=6.5d0
               if(isoprismbook(j,1,6).eq.i) water(i,j)=6.5d0
               if(ring(j,1,1).eq.i) water(i,j)=6.0d0
               if(ring(j,1,2).eq.i) water(i,j)=6.0d0
               if(ring(j,1,3).eq.i) water(i,j)=6.0d0
               if(ring(j,1,4).eq.i) water(i,j)=6.0d0
               if(ring(j,1,5).eq.i) water(i,j)=6.0d0
               if(ring(j,1,6).eq.i) water(i,j)=6.0d0
               if(isoring(j,1,1).eq.i) water(i,j)=6.5d0
               if(isoring(j,1,2).eq.i) water(i,j)=6.5d0
               if(isoring(j,1,3).eq.i) water(i,j)=6.5d0
               if(isoring(j,1,4).eq.i) water(i,j)=6.5d0
               if(isoring(j,1,5).eq.i) water(i,j)=6.5d0
               if(isoring(j,1,6).eq.i) water(i,j)=6.5d0
               enddo
            enddo
          enddo

          do i=1,iconf
           write(ntem8,*) 'Water ID#', 3, 'Frame #', i,                &
              'Oligomer type', water(3,i)
          enddo

! What is the percent chance that any individual water is part of a recognized oligomer?
         
         icount=0
         do i=1,nwat
            do j=1,iconf
               if(water(i,j).ne.0) icount=icount+1
            enddo
         enddo
         counts=icount
         frames=iconf
         wat=nwat
         avg=((counts/frames)/wat)*100
         write(ntem2,*)'% Chance any H2O will be part of an           &
              oligomer =', avg

!*********************************************************************
      end
