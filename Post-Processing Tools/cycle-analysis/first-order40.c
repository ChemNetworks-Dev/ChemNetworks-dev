/****************************************
 *                                      *
 * Author: Abdullah Ozkanlar            *
 *         abdullah.ozkanlar@wsu.edu    *
 *                                      *
 * Washington State University          *
 * Chemistry Department                 *
 * A. Clark Research Group              *
 * Pullman, WA 99164                    *
 *                                      *
 ****************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>
#include "nrutil.h"
#define MAX 500000000
#define NRANSI
#define FLN 256

double *vector(int nl, int nh);
double **matrix(int nrl, int nrh, int ncl, int nch);
void free_vector(double *v, int nl, int nh);
void free_matrix(double **m, int nrl, int nrh, int ncl, int nch);


int main(int argc, char *argv[]){

       FILE *inputf;
       FILE *outputf;  

       char finput[FLN];      /* intput file name */
       FILE *fd;              /* file pointer */  

       char foutput[FLN];    

      int i,j,k,k1,k2,k3,k4,counter,t,cnt;
      int k5,k6,k7,k8,k9,k10,k11,k12,k13,k14,k15,k16,k17,k18,k19,k20,k21;
      int k22,k23,k24,k25,k26,k27,k28,k29,k30,k31,k32,k33,k34,k35,k36,k37,k38,k39,k40; 
      int k41;
      int z,r,s,x,y,w; 
      double *a,*test,**m;
      char format[] = "%.8E ";  /* format of output */

    a = vector( 1, MAX );
// test = vector( 1, MAX );
    m = matrix(1, MAX/2, 1, 2);

/**************************************************/
  if(argc != 2){
    printf("Usuage: %s inputfile\n"
           "inputfile is assumed to have extension .input\n", argv[0]);

    exit(-1);
  }


sprintf(finput, "%s", argv[1]);

  if((fd=fopen(finput,"r"))==NULL){
    printf("Cannot open file %s\n", finput);
    exit(-1);
  }

/**************************************************/

    i=1;

  while(fscanf(fd,"%lf",&a[i])!=EOF)
   {
             i=i+1;
    }
 fclose(fd);

/******************************************************/

s=1;
for(z=1;z<=(i-1)/2;z++)
    for(r=1;r<=2;r++)
       { m[z][r]=a[s]; s=s+1;}


sprintf(foutput, "%s.output.first", argv[1]);
outputf=fopen(foutput,"w");

j = 1;
k1 = 0; k2 = 0; k3 = 0; k4 = 0; k5 = 0; k6 = 0; k7 = 0; k8 = 0; k9 = 0; k10 = 0;
k11 = 0; k12 = 0; k13 = 0; k14 = 0; k15 = 0; k16 = 0; k17 = 0; k18 = 0; k19 = 0; k20 = 0;
k21 = 0; k22 = 0; k23 = 0; k24 = 0; k25 = 0; k26 = 0; k27 = 0; k28 = 0; k29 = 0; k30 = 0;
k31 = 0; k32 = 0; k33 = 0; k34 = 0; k35 = 0; k36 = 0; k37 = 0; k38 = 0; k39 = 0; k40 = 0;
k41 = 0; 

while( j <= ((i-1)/2))
{  cnt = 1;
   for(k=j+1;k<=((i-1)/2);k++)
   {
      if(m[j][2] == m[k][2])
      { 
        if(m[k][1] == (m[j][1] + 1))
        { cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[j][1],m[j][2],m[k][1],m[k][2]);
          k1 = k;  
        } 

      }
       
     
   }

   if(k1 != 0){
   for(k=k1+1;k<=((i-1)/2);k++)
   { 
      if(m[k1][2] == m[k][2])
      {
        if(m[k][1] == (m[k1][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k1][1],m[k1][2],m[k][1],m[k][2]);
          k2 = k;                     
        }

      }
      
     
   }
   k1 = 0;
   }

   if(k2 != 0){
   for(k=k2+1;k<=((i-1)/2);k++)
   {
      if(m[k2][2] == m[k][2])
      {
        if(m[k][1] == (m[k2][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k2][1],m[k2][2],m[k][1],m[k][2]);
          k3 = k;
        }

      }


   }
   k2 = 0;
   }

   if(k3 != 0){
   for(k=k3+1;k<=((i-1)/2);k++)
   {
      if(m[k3][2] == m[k][2])
      {
        if(m[k][1] == (m[k3][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k3][1],m[k3][2],m[k][1],m[k][2]);
          k4 = k;
        }

      }


   }
   k3 = 0;
   }

   if(k4 != 0){
   for(k=k4+1;k<=((i-1)/2);k++)
   {
      if(m[k4][2] == m[k][2])
      {
        if(m[k][1] == (m[k4][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k4][1],m[k4][2],m[k][1],m[k][2]);
          k5 = k;
        }

      }


   }
   k4 = 0;
   }

   if(k5 != 0){
   for(k=k5+1;k<=((i-1)/2);k++)
   {
      if(m[k5][2] == m[k][2])
      {
        if(m[k][1] == (m[k5][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k5][1],m[k5][2],m[k][1],m[k][2]);
          k6 = k;
        }

      }


   }
   k5 = 0;
   }

   if(k6 != 0){
   for(k=k6+1;k<=((i-1)/2);k++)
   {
      if(m[k6][2] == m[k][2])
      {
        if(m[k][1] == (m[k6][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k6][1],m[k6][2],m[k][1],m[k][2]);
          k7 = k;
        }
 
      }


   }
   k6 = 0;
   }

   if(k7 != 0){
   for(k=k7+1;k<=((i-1)/2);k++)
   {
      if(m[k7][2] == m[k][2])
      {
        if(m[k][1] == (m[k7][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k7][1],m[k7][2],m[k][1],m[k][2]);
          k8 = k;
        }
 
      }


   }
   k7 = 0;
   }

   if(k8 != 0){
   for(k=k8+1;k<=((i-1)/2);k++)
   {
      if(m[k8][2] == m[k][2])
      {
        if(m[k][1] == (m[k8][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k8][1],m[k8][2],m[k][1],m[k][2]);
          k9 = k;
        }
 
      }

 
   }
   k8 = 0;
   }

   if(k9 != 0){
   for(k=k9+1;k<=((i-1)/2);k++)
   {
      if(m[k9][2] == m[k][2])
      {
        if(m[k][1] == (m[k9][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k9][1],m[k9][2],m[k][1],m[k][2]);
          k10 = k;
        }
 
      }

 
   }
   k9 = 0;
   }

   if(k10 != 0){
   for(k=k10+1;k<=((i-1)/2);k++)
   {
      if(m[k10][2] == m[k][2])
      {
        if(m[k][1] == (m[k10][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k10][1],m[k10][2],m[k][1],m[k][2]);
          k11 = k;
        }
 
      }
 
 
   }
   k10 = 0;
   }

/***************/

      if(k11 != 0){
   for(k=k11+1;k<=((i-1)/2);k++)
   { 
      if(m[k11][2] == m[k][2])
      {
        if(m[k][1] == (m[k11][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k11][1],m[k11][2],m[k][1],m[k][2]);
          k12 = k;                     
        }

      }
      
     
   }
   k11 = 0;
   }

   if(k12 != 0){
   for(k=k12+1;k<=((i-1)/2);k++)
   {
      if(m[k12][2] == m[k][2])
      {
        if(m[k][1] == (m[k12][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k12][1],m[k12][2],m[k][1],m[k][2]);
          k13 = k;
        }

      }


   }
   k12 = 0;
   }

   if(k13 != 0){
   for(k=k13+1;k<=((i-1)/2);k++)
   {
      if(m[k13][2] == m[k][2])
      {
        if(m[k][1] == (m[k13][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k13][1],m[k13][2],m[k][1],m[k][2]);
          k14 = k;
        }

      }


   }
   k13 = 0;
   }

   if(k14 != 0){
   for(k=k14+1;k<=((i-1)/2);k++)
   {
      if(m[k14][2] == m[k][2])
      {
        if(m[k][1] == (m[k14][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k14][1],m[k14][2],m[k][1],m[k][2]);
          k15 = k;
        }

      }


   }
   k14 = 0;
   }

   if(k15 != 0){
   for(k=k15+1;k<=((i-1)/2);k++)
   {
      if(m[k15][2] == m[k][2])
      {
        if(m[k][1] == (m[k15][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k15][1],m[k15][2],m[k][1],m[k][2]);
          k16 = k;
        }

      }


   }
   k15 = 0;
   }

   if(k16 != 0){
   for(k=k16+1;k<=((i-1)/2);k++)
   {
      if(m[k16][2] == m[k][2])
      {
        if(m[k][1] == (m[k16][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k16][1],m[k16][2],m[k][1],m[k][2]);
          k17 = k;
        }
 
      }


   }
   k16 = 0;
   }

   if(k17 != 0){
   for(k=k17+1;k<=((i-1)/2);k++)
   {
      if(m[k17][2] == m[k][2])
      {
        if(m[k][1] == (m[k17][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k17][1],m[k17][2],m[k][1],m[k][2]);
          k18 = k;
        }
 
      }


   }
   k17 = 0;
   }

   if(k18 != 0){
   for(k=k18+1;k<=((i-1)/2);k++)
   {
      if(m[k18][2] == m[k][2])
      {
        if(m[k][1] == (m[k18][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k18][1],m[k18][2],m[k][1],m[k][2]);
          k19 = k;
        }
 
      }

 
   }
   k18 = 0;
   }

   if(k19 != 0){
   for(k=k19+1;k<=((i-1)/2);k++)
   {
      if(m[k19][2] == m[k][2])
      {
        if(m[k][1] == (m[k19][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k19][1],m[k19][2],m[k][1],m[k][2]);
          k20 = k;
        }
 
      }

 
   }
   k19 = 0;
   }

   if(k20 != 0){
   for(k=k20+1;k<=((i-1)/2);k++)
   {
      if(m[k20][2] == m[k][2])
      {
        if(m[k][1] == (m[k20][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k20][1],m[k20][2],m[k][1],m[k][2]);
          k21 = k;
        }
 
      }
 
 
   }
   k20 = 0;
   }

/********************************/

/***************/

      if(k21 != 0){
   for(k=k21+1;k<=((i-1)/2);k++)
   { 
      if(m[k21][2] == m[k][2])
      {
        if(m[k][1] == (m[k21][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k21][1],m[k21][2],m[k][1],m[k][2]);
          k22 = k;                     
        }

      }
      
     
   }
   k21 = 0;
   }

   if(k22 != 0){
   for(k=k22+1;k<=((i-1)/2);k++)
   {
      if(m[k22][2] == m[k][2])
      {
        if(m[k][1] == (m[k22][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k22][1],m[k22][2],m[k][1],m[k][2]);
          k23 = k;
        }

      }


   }
   k22 = 0;
   }

   if(k23 != 0){
   for(k=k23+1;k<=((i-1)/2);k++)
   {
      if(m[k23][2] == m[k][2])
      {
        if(m[k][1] == (m[k23][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k23][1],m[k23][2],m[k][1],m[k][2]);
          k24 = k;
        }

      }


   }
   k23 = 0;
   }

   if(k24 != 0){
   for(k=k24+1;k<=((i-1)/2);k++)
   {
      if(m[k24][2] == m[k][2])
      {
        if(m[k][1] == (m[k24][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k24][1],m[k24][2],m[k][1],m[k][2]);
          k25 = k;
        }

      }


   }
   k24 = 0;
   }

   if(k25 != 0){
   for(k=k25+1;k<=((i-1)/2);k++)
   {
      if(m[k25][2] == m[k][2])
      {
        if(m[k][1] == (m[k25][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k25][1],m[k25][2],m[k][1],m[k][2]);
          k26 = k;
        }

      }


   }
   k25 = 0;
   }

   if(k26 != 0){
   for(k=k26+1;k<=((i-1)/2);k++)
   {
      if(m[k26][2] == m[k][2])
      {
        if(m[k][1] == (m[k26][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k26][1],m[k26][2],m[k][1],m[k][2]);
          k27 = k;
        }
 
      }


   }
   k26 = 0;
   }

   if(k27 != 0){
   for(k=k27+1;k<=((i-1)/2);k++)
   {
      if(m[k27][2] == m[k][2])
      {
        if(m[k][1] == (m[k27][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k27][1],m[k27][2],m[k][1],m[k][2]);
          k28 = k;
        }
 
      }


   }
   k27 = 0;
   }

   if(k28 != 0){
   for(k=k28+1;k<=((i-1)/2);k++)
   {
      if(m[k28][2] == m[k][2])
      {
        if(m[k][1] == (m[k28][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k28][1],m[k28][2],m[k][1],m[k][2]);
          k29 = k;
        }
 
      }

 
   }
   k28 = 0;
   }

   if(k29 != 0){
   for(k=k29+1;k<=((i-1)/2);k++)
   {
      if(m[k29][2] == m[k][2])
      {
        if(m[k][1] == (m[k29][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k29][1],m[k29][2],m[k][1],m[k][2]);
          k30 = k;
        }
 
      }

 
   }
   k29 = 0;
   }

   if(k30 != 0){
   for(k=k30+1;k<=((i-1)/2);k++)
   {
      if(m[k30][2] == m[k][2])
      {
        if(m[k][1] == (m[k30][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k30][1],m[k30][2],m[k][1],m[k][2]);
          k31 = k;
        }
 
      }
 
 
   }
   k30 = 0;
   }

/***************/

      if(k31 != 0){
   for(k=k31+1;k<=((i-1)/2);k++)
   { 
      if(m[k31][2] == m[k][2])
      {
        if(m[k][1] == (m[k31][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k31][1],m[k31][2],m[k][1],m[k][2]);
          k32 = k;                     
        }

      }
      
     
   }
   k31 = 0;
   }

   if(k32 != 0){
   for(k=k32+1;k<=((i-1)/2);k++)
   {
      if(m[k32][2] == m[k][2])
      {
        if(m[k][1] == (m[k32][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k32][1],m[k32][2],m[k][1],m[k][2]);
          k33 = k;
        }

      }


   }
   k32 = 0;
   }

   if(k33 != 0){
   for(k=k33+1;k<=((i-1)/2);k++)
   {
      if(m[k33][2] == m[k][2])
      {
        if(m[k][1] == (m[k33][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k33][1],m[k33][2],m[k][1],m[k][2]);
          k34 = k;
        }

      }


   }
   k33 = 0;
   }

   if(k34 != 0){
   for(k=k34+1;k<=((i-1)/2);k++)
   {
      if(m[k34][2] == m[k][2])
      {
        if(m[k][1] == (m[k34][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k34][1],m[k34][2],m[k][1],m[k][2]);
          k35 = k;
        }

      }


   }
   k34 = 0;
   }

   if(k35 != 0){
   for(k=k35+1;k<=((i-1)/2);k++)
   {
      if(m[k35][2] == m[k][2])
      {
        if(m[k][1] == (m[k35][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k35][1],m[k35][2],m[k][1],m[k][2]);
          k36 = k;
        }

      }


   }
   k35 = 0;
   }

   if(k36 != 0){
   for(k=k36+1;k<=((i-1)/2);k++)
   {
      if(m[k36][2] == m[k][2])
      {
        if(m[k][1] == (m[k36][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k36][1],m[k36][2],m[k][1],m[k][2]);
          k37 = k;
        }
 
      }


   }
   k36 = 0;
   }

   if(k37 != 0){
   for(k=k37+1;k<=((i-1)/2);k++)
   {
      if(m[k37][2] == m[k][2])
      {
        if(m[k][1] == (m[k37][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k37][1],m[k37][2],m[k][1],m[k][2]);
          k38 = k;
        }
 
      }


   }
   k37 = 0;
   }

   if(k38 != 0){
   for(k=k38+1;k<=((i-1)/2);k++)
   {
      if(m[k38][2] == m[k][2])
      {
        if(m[k][1] == (m[k38][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k38][1],m[k38][2],m[k][1],m[k][2]);
          k39 = k;
        }
 
      }

 
   }
   k38 = 0;
   }

   if(k39 != 0){
   for(k=k39+1;k<=((i-1)/2);k++)
   {
      if(m[k39][2] == m[k][2])
      {
        if(m[k][1] == (m[k39][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k39][1],m[k39][2],m[k][1],m[k][2]);
          k40 = k;
        }
 
      }

 
   }
   k39 = 0;
   }

   if(k40 != 0){
   for(k=k40+1;k<=((i-1)/2);k++)
   {
      if(m[k40][2] == m[k][2])
      {
        if(m[k][1] == (m[k40][1] + 1))
        { // cnt = cnt + 1;
          fprintf(outputf,"%.0lf %.0lf\n%.0lf %.0lf\n",m[k40][1],m[k40][2],m[k][1],m[k][2]);
          k41 = k;
        }
 
      }
 
 
   }
   k40 = 0;
   }

/***************/
 
     if(cnt == 1) fprintf(outputf,"%.0lf %.0lf\n",m[j][1],m[j][2]);
     j = j + 1; 
}



 fclose(outputf);

 /* free memory */

free_vector( a, 1, MAX );
// free_vector( test, 1, MAX );
free_matrix(m, 1, MAX/2, 1, 2);

  return 0;
}


double *vector(int nl, int nh)
{
        double *v;

        v = (double *) malloc( (unsigned)(nh-nl+1) * sizeof(double) );
        if (!v)
                nrerror("\nallocation failure in vector()");
        return v-nl;
}
double **matrix(int nrl, int nrh, int ncl, int nch)
{
        int i;
        double **m;

        m = (double **) malloc( (unsigned)(nrh - nrl + 1) * sizeof(double *) );
        if (!m)
                nrerror("allocation failure 1 in matrix()");
        m -= nrl;

        for (i = nrl; i <= nrh; i++)
        {
                m[i] = (double *) malloc( (unsigned)(nch - ncl + 1)*sizeof(double) );
                if (!m[i])
                        nrerror("allocation failure 2 in matrix()");
                m[i] -= ncl;
        }
        return m;
}
void free_vector(double *v, int nl, int nh)
{
        free( (char *)(v + nl) );
}

void free_matrix(double **m, int nrl, int nrh, int ncl, int nch)
{
        int i;

        for (i = nrh; i >= nrl; i--)
                free( (char *)(m[i] + ncl) );
        free( (char *)(m + nrl) );
}

#undef NRANSI

void nrerror(char *error_text)
{
        fprintf(stderr, "Numerical Recipes run-time error...\n");
        fprintf(stderr, "%s\n", error_text);
        fprintf(stderr, "...now exiting to system...\n");
        exit(1);
}
