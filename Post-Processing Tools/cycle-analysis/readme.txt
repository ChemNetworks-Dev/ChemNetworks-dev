=============================
To run the oligomer lifetimes:
=============================
>>> ./oligomer-count
will generate '*.lifetimes' files.
To run lifetimes of oligomers, e.g. "bag hexamers"
Print the "Frame #"  and "Hexamer #" via 
>>> awk '{print $3, $8}'  hexamer.bags.lifetimes > hbags

Then, run the following executables in the given order:

>>> ./first-order40 hbags  (use .pbs file to submit to queue)
>>> ./ikinci hbags.output.first (use .pbs file to submit to queue)
>>> ./second-count hbags.output.first.output.ikinci
>>> ./last-column-second.sh hbags.output.first.output.ikinci.output.second
>>> ./third-frequencies hbags.output.first.output.ikinci.output.second.counts

The last step will generate an output file named "hbags.output.first.output.ikinci.output.second.counts.output.frequencies" in which 
the "# of snaphots the hexamers (bags, in this case) lasted" and the corresponding "frequencies" are printed.

Finally, use excel template 'Lifetime.xlsm' to get the average lifetimes.
