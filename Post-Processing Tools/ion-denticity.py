### Nitesh Kumar, Aurora Clark Group ###
### Denticity of interfacial ions using GraphGeod File ###
### Input files 1. cation.gro 2. cation-anion.GraphGeod ###
### This code can be used to calulate the ion density in bulk, or at the interface. ### 
### This code can be used with ITIM ### 
### Slabs are used to define bulk and the interface ###
### This code can be extended to calculate ion denticity more than \eta2 ###
### Contact : nitesh.kumar@wsu.edu ### 



import sys
import numpy as np
import os, sys
import pandas as pd 

print ("molecule_name, start_snap, end_snap, deltaT")
print ("eg. WATER/OCTA_OH, 0, 10000, 2")


mole_name = sys.argv[1]
mole_name2 = sys.argv[5]
mole_name3 =sys.argv[6]
time_b = int(sys.argv[2])  #0
time_e = int(sys.argv[3])  #10000
delta_t = int(sys.argv[4]) #1
t= time_b
amsum = []

while t <= time_e:
    y = []
    x = []
    with open('ury' + str(t)  + '.gro') as inputfile:
        print(t)
        lines = inputfile.readlines()
        lengthfile = len(lines)
        inputfile.close()
        for i in range(2,lengthfile):
            if str(lines[i].split()[1]) == 'UO' : 
                x.append(i) 
        for i in x:
            if (float(lines[i].split()[-1]) >= 5.57224174 and float(lines[i].split()[-1]) <= 6.57224174 ) or (float(lines[i].split()[-1]) >= 11.3570159 and float(lines[i].split()[-1]) <= 12.3570159):
                m = int(454+i)
                print (m) 
                y.append(m)
            else:
                pass
        #print y             
    with open(mole_name + str(t) + '.input.' + mole_name2 + str(t) + '.xyz.' + mole_name3 + str(t) + '.xyz.' + 'GraphGeod','r') as inputfile1:
        print(t)
        lines1 = inputfile1.readlines()
        lengthfile1 = len(lines1)
        inputfile1.close()
        edge_u1 = []
        edge_u2 = []
        for i in range(0, lengthfile1):
            if 1 <= int(lines1[i].split()[-3]) <= 3 : 
                if int(lines1[i].split()[1]) in y : 
                    print('b')
                #if (lines[i].find('# Fix ') != -1):
                    edge_u1.append(int(lines1[i].split()[0]))
                    edge_u2.append(int(lines1[i].split()[1]))
        df = pd.DataFrame({'one': pd.Series(edge_u1),'two':pd.Series(edge_u2)})    
        amsum.append(df.groupby(df.columns.tolist(),as_index=False).size())
    
    t += delta_t
#print(amsum)

amsum_final = []

for j in range(0,t-1):
    amsum_final.append(amsum[j])

np.savetxt('denticity_amsum_interface.txt',amsum,fmt='%s')

mono =[]
two =[]
three = []
denticity_list_all = []

with open("denticity_amsum_interface.txt") as dent : 
    d_lines = dent.readlines()
    d_lengthfile = len(d_lines)
    dent.close()
    for i in range(0,d_lengthfile-1):
        if (len(d_lines[i].split()) == 3) and (1<=len(d_lines[i].split()[-1])<=2): 
            denticity_list_all.append(int(d_lines[i].split()[2]))

denticity_list_all = np.asarray(denticity_list_all)
for i in denticity_list_all :
    if i == 1 :
        mono.append(1)
    elif i == 2 :
        two.append(2)
    else :
        three.append(3)

print(denticity_list_all)
mono_avg = []
mono_avg.append(float(float(len(mono))/time_e))
two_avg = []
two_avg.append(float(float(len(two))/time_e))
a = []
a.append(np.mean(amsum))
np.savetxt('CIP_monodentate_interface.txt', mono_avg)
np.savetxt('CIP_bidentate_interface.txt', two_avg)

